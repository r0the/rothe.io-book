import math
import pygame

TITLE = "Juliamenge"
WIDTH = 500
HEIGHT = 500
SCALE = 200
background_colour = 255, 255, 255
N = 500
S = 4


def draw_julia_point(x, y, c):
    re = (x - WIDTH / 2) / SCALE
    im = (y - HEIGHT / 2) / SCALE
    z = complex(re, im)
    n = 0
    while n < N and abs(z) < S:
        z = z * z + c
        n = n + 1
    shade = 255 - int(math.sqrt(n) * 255 / math.sqrt(N))
    colour = (shade / 2, shade / 2, shade)
    screen.draw.rect(Rect(x, y, 1, 1), colour)


def draw_julia(c):
    for x in range(0, WIDTH):
        for y in range(0, HEIGHT):
            draw_julia_point(x, y, c)


def draw():
    screen.fill(background_colour)
    draw_julia(-0.1 + 0.651j)
    pygame.image.save(screen.surface, "julia.png")
