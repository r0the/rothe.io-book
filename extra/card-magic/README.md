# Kartentrick
---

## Regeln

- Reihenfolge Farben: Herz, Pik, Karo, Kreuz
- As ist eins.
- fünf Karten werden ausgewählt
- Kartenpaar der gleichen Farbe mit kleinster Differenz wählen
- grössere Karte der beiden wählen als geheime Karte
- kleinere Karte ist erste (links) der Folge
- Position der kleinsten Karte der übrigen drei gibt +1, +2 oder +3 an
- letzte zwei Karten aufsteigend: +3


## Erklärung
Herz-4		Karo-5		Kreuz-3		Herz-As

- die erste der 4 Karten zeigt die Farbe der gesuchten
  -> hier Herz
- die erste der 4 Karten gibt den Startwert vor
  -> hier 4
- Reihenfolge der Werte:  As=1, 2, ..., 10, Bube=11, Dame=12, König=13
- Reihenfolge der Farben: Herz, Pik, Karo, Kreuz
- Reihenfolge absolut:    zuerst Wert, bei gleichem Wert: Farbe
- die Position der kleinsten der verbleibenden 3 Karten gibt einen
  Wert von 1-3
  -> hier Herz-As: Position 3 (von 3)
  -> zum Startwert 4 addieren wir also 3: 7

- nun bleiben also noch 2 Karten anzuschauen
  -> Karo-5 und Kreuz-3
- wenn die höhere der beiden Karten vor der tieferen kommt, addiert man
  nochmals 3, sonst bleibt die Zahl unverändert
  -> hier Karo-5 vor Kreuz-3: 7 + 3 = 10

-> geheime Karte: Herz-10

* [:download: magic.zip](./magic.zip)
