# Lindenmayer-Systeme
---

Aristid Lindenmayer ist ein ungarischer Biologe, welcher von 1925 bis 1989 gelebt hat.
Er hat einen Typ formaler Sprachen entwickelt, mit welchen er das Zellwachstum von Pflanzen modelliert hat.

::: columns 3
![Koch-Kurve mit Tiefe 3](./koch-curve-1.png)
***
![Koch-Kurve mit Tiefe 6](./koch-curve-2.png)
***
![Drachenkurve mit Tiefe 17](./dragon-curve.png)
:::
## Einfaches L-System

Ein L-System (oder Lindenmayer-System) kann als einfache Programmiersprache angesehen werden,
welche einen Zeichenstift steuert. Ein L-System-Programm besteht aus einer Zeichenkette $s$, wobei jedes Zeichen einen Befehl darstellt:

| Zeichen        | Bedeutung                                       |
| -------------- | ----------------------------------------------- |
| Grossbuchstabe | Um die Länge $l$ nach vorne bewegen.            |
| `-`            | Um den Winkel $a$ im Uhrzeigersinn drehen.      |
| `+`            | Um den Winkel $a$ im Gegenuhrzeigersinn drehen. |

Alle anderen Zeichen werden ignoriert. Die Länge $l$ und der Winkel $a$ heissen Parameter und werden für jedes L-System angegeben.

::: example
**Beispiel:** Das L-System mit $s =$ `F+F+F+F` , $l = 1$ und $a = 90\degree$ zeichnet ein Einheitsquadrat.
:::

## Ersetzungsregeln

Zusätzlich werden für ein L-System Ersetzungsregeln definiert. Eine solche Regel beschreibt, durch
welche Zeichenkette ein Grossbuchstaben ersetzt werden soll.

::: example
**Beispiel:** Die Ersetzungsregel `A` → `B+A` beschreibt, dass jedes A durch die Zeichenkette B+A ersetzt werden soll.
:::

Die Ersetzungsregeln werden auf die Zeichenkette $s$ angewendet, bevor die Befehle ausgeführt werden.

::: example
**Beispiel:** Beim L-System $s =$ `AHA` , $l = 1$ , $a = 90\degree$ mit den Ersetzungsregeln `A` → `B+A` und `H` → `+` ergibt sich die Befehlsfolge `B+A+B+A`. Somit wird auch hier ein Einheitsquadrat gezeichnet.
:::

Wichtig ist, dass alle Ersetzungen gleichzeitig stattfinden.

## Wiederholtes Ersetzen

Ein L-System kann in verschiedenen Tiefen dargestellt werden. Die Tiefe gibt an, wie oft nacheinander die Ersetzungsregeln angewendet werden, bevor die resultierende Zeichenkette als Befehlsfolge interpretiert und gezeichnet wird.

Zusätzlich wird bei jedem Ersetzungsschritt die Länge $l$ mit einem konstanten Faktor $f < 1$ multipliziert.

Für das L-System $s =$ `F` , $l = 1$ , $a = 60\degree$ , $f = \frac{1}{3}$ und der Ersetzungsregel `F` → `F+F--F+F` ergeben sich folgende Befehlsfolgen:

| Tiefe | Länge         | Befehlsfolge                           |
|:----- |:------------- |:-------------------------------------- |
| 0     | $1$           | `F`                                    |
| 1     | $\frac{1}{3}$ | `F+F--F+F`                             |
| 2     | $\frac{1}{9}$ | `F+F--F+F+F+F--F+F--F+F--F+F+F+F--F+F` |

## Vorgegebene L-Systeme

Ein paar schöne L-Systeme:

| Name              | Definition                                                               |
| ----------------- | ------------------------------------------------------------------------ |
| Koch-Kurve        | $s=$`A`, $a=60\degree$, $l=0.8$, $f=\frac{1}{3}$, `A` → `A+A--A+A`       |
| Koch-Schneeflocke | $s=$`A--A--A`, $a=60\degree$, $l=0.8$, $f=\frac{1}{3}$, `A` → `A+A--A+A` |
| Mäanderkurve      | $s=$`X`, $a=90\degree$, $l=0.2$, $f=0.8$, `X` → `-FF+FF+F+F-F-FFX`       |
| Drachenkurve      | $s=$`R`, $a=45\degree$, $l=1$, $f=0.7$, `R` → `+R--L+`, `L` → `-R++L-`   |

::: exercise Aufgabe

Schreibe ein Programm, welches L-Systeme zeichnet. Das Programm ein vorgegebenes L-System ein paar Sekunden lang in der Tiefe 0 anzeigen, dann in der Tiefe 1 usw.

Zusätzliche Anregungen:
- Der Benutzer kann ein L-System auswählen.
- Die maximale Tiefe kann eingestellt werden.
- Der Benutzer kann manuell zur vorherigen/nächsten Tiefe wechseln.
- Der Benutzer kann selbst L-Systeme definieren.
:::

## L-Systeme mit Stack

Nun erweitern wir die «Sprache» der L-Systeme um zwei Regeln:

| Zeichen | Bedeutung                                                |
| ------- | -------------------------------------------------------- |
| `(`     | Speichere die aktuellen Koordinaten auf einem Stack      |
| `)`     | Stelle die obersten Koordinaten auf dem Stack wieder her |
