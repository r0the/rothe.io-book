import turtle

def drawCommands(commands, length, angle):
    for cmd in commands:
        if cmd.isalpha():
            turtle.forward(length)
        elif cmd == '+':
            turtle.left(angle)
        elif cmd == '-':
            turtle.right(angle)


drawCommands('F+F+F+F', 100, 90)
