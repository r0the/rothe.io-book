# Spirograph
---

Ein Spirograph ist ein Spielzeug, mit welchem schöne Figuren gezeichnet werden können.

Mathematisch gesehen werden Hypotrochoide gezeichnet. Diese Kurven entstehen, in dem ein Kreis mit Radius $r$ in einem grösseren, fixen Kreis mit Radius $R$ herumrollt und ein Zeichenstift am inneren Kreis im Abstand $d$ vom Mittelpunkt fixiert ist.

![Animation eines Hypotrochoids ©](./hypotrochoid.gif)

Ein Hypotrochoid wird durch folgende Gleichungen beschrieben:

$$x(\alpha) = (R - r)\cdot\cos\alpha + d\cdot\cos\left(\frac{R-r}{r}\cdot\alpha\right)$$

$$y(\alpha) = (R - r)\cdot\sin\alpha - d\cdot\sin\left(\frac{R-r}{r}\cdot\alpha\right)$$


::: exercise Aufgabe

Schreibe mit Pygame Zero in Programm, welches einen Spirographen simuliert.

Verwende folgende Werte:

$$R = 125, \qquad r = 75, \qquad d = 125 $$

$$R = 125, \qquad r = 85, \qquad d = 125 $$

Experimentiere mit weiteren Werten
***

Lösung mit Turtle-Grafik:
``` python ./spirograph_turtle.py
```

Lösung mit Pygame Zero:
``` python ./spirograph_pgzero.py
```
:::
