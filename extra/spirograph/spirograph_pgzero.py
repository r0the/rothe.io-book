import math

SIZE = 200
TITLE = "Spirograph"
WIDTH = 2 * SIZE
HEIGHT = 2 * SIZE

R = 125
r = 75
d = 125
THETA = 0.1

pen = Actor("pen_red")
pen.x = SIZE + R - r + d
pen.y = SIZE
pen.angle = 0


def update():
    pen.angle = pen.angle + THETA
    dr = R - r
    pen.x = SIZE + dr * math.cos(pen.angle) + d * math.cos((dr / r) * pen.angle)
    pen.y = SIZE - dr * math.sin(pen.angle) + d * math.sin((dr / r) * pen.angle)


def draw():
    pen.draw()
