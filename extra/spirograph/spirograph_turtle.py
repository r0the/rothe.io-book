import math
import turtle

turtle.hideturtle()
turtle.speed(10)
turtle.pensize(3)
turtle.color((0.9, 0, 0.9))

R = 125
r = 85
d = 125
theta = 0.2
steps = 8 * int(6*3.14 / theta)

turtle.penup()
turtle.goto(R - r + d, 0)
turtle.pendown()
angle = 0
for t in range(0, steps):
    angle = angle + theta
    x = (R - r) * math.cos(angle) + d * math.cos(((R - r)/r)*angle)
    y = (R - r) * math.sin(angle) - d * math.sin(((R - r)/r)*angle)
    turtle.goto(x, y)
