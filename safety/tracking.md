# Tracking
---


## Tracking im Browser

- Cookies
- Tracking-Scripts
- Fingerprinting

## Tracking beim Handy

- mittels [WLAN-Namen][3].

[3]: https://de.wikipedia.org/wiki/WLAN-basierte_Ortung

- mittels [Audiosignalen][2], die via Mikrofon aufgenommen werden. Diese können von einem Werbeblakat (z.B. [am Bahnhof Zürich][1]), von einem anderen Smartphone, von einer Webseite am PC oder von einem Fernsehgerät ausgesendet werden.

[1]: https://www.20min.ch/schweiz/zuerich/story/Sammelt-mysterioeses-Kaestchen-unsere-Daten--22479099
[2]: https://www.heise.de/newsticker/meldung/Tracking-Forscher-finden-Ultraschall-Spyware-in-234-Android-Apps-3704642.html
