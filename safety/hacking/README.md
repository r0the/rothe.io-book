# Hacking
---


## Herkunft des Begriffs[^1]

US-amerikanische Funkamateure verwendeten Mitte der 1950er Jahre den Begriff «hacking» ursprünglich als Ausdruck für besonders einfallsreiche Anpassungen ihrer Geräte, die dazu dienten, deren Leistung zu verbessern.

Steve Russel, Mitglied des Tech Model Railroad Club of MIT, an einem PDP-1-Computer
In den späten 1950er Jahren wurde «hacking» auch vom Modelleisenbahnclub des MIT (Massachusetts Institute of Technology) verwendet, dem Tech Model Railroad Club (TMRC). Das Wort nahm im TMRC Bezug zur Anpassung ihrer elektronischen und mechanischen Geräte. Es gehört zum Wesen eines Hacks, dass er rasch durchgeführt wird, effektiv ist und unelegant sein kann («quick and dirty»). Er erreicht das gewünschte Ziel, ohne die Systemarchitektur, in die er eingebettet ist, komplett umformen zu müssen, obwohl er oft im Widerspruch zu ihr steht.

Das Wort «Hack» stand am MIT auch im Kontext von technikbasierten Streichen oder entsprach einem Wort für besonders geschickte oder gewagte Taten. Dabei schwang eine Konnotation von nicht destruktiver Harmlosigkeit und kreativem Spaß mit. Hatte ein Student des MIT einen raffinierten Streich ausgeheckt, galt der Übeltäter als «Hacker». Der Gebrauch des Wortes «Hack» verschob sich zur Technik, die benötigt wird, um den Streich auszuführen. Es wurde später für eine schlaue technische Lösung im Allgemeinen verwendet, ohne sich dabei unbedingt auf einen Streich zu beziehen und ohne dies auf den Computer zu beschränken.

Als Mitglieder des Modellbahnklubs damit begannen, mit einem DEC-PDP-1-Computer zu arbeiten, wurde ihr Jargon nun auch auf den Computer übertragen. Die zuerst bekannte Verwendung des Begriffs «Hacker» wurde auf diese Weise von der Ausgabe der Studentenzeitung The Tech vom 20. November 1963 der technischen Fachschule des MIT registriert und bezog sich zunächst auf Personen, die mit der Technik des Telefonnetzes herumspielten (Phreaking).

## Exploit

Ein **Exploit** (englisch für «ausnutzen») ist in der elektronischen Datenverarbeitung eine systematische Möglichkeit, Schwachstellen auszunutzen, die bei der Entwicklung eines Programms entstanden sind. Dabei werden mit Hilfe von Programmcodes Sicherheitslücken und Fehlfunktionen von Programmen (oder ganzen Systemen) ausgenutzt, meist um sich Zugang zu Ressourcen zu verschaffen oder in Computersysteme einzudringen, bzw. diese zu beeinträchtigen.

### Beispie: SQL-Injection-Exploits

Diese Art von Exploits betrifft Web-Anwendungen. Sie nutzen aus, dass manche Web-Anwendungen den Text, welcher ein Benutzer auf der Webseite eingibt, ungeprüft an die Datenbank weiterleitet. Die Datenbank interpretiert dann einen Teil der Benutzereingabe als Befehl. Damit können Daten von anderen Benutzern ausgelesen oder gar gelöscht werden.

![](./xkcd-exploits-of-a-mom.png "Beispiel eines SQL-Injection-Exploits")

Im obenstehenden Beispiel hat eine Mutter ihren Sohn «Robert"; DROP TABLE students; --» genannt. Sie nimmt an, dass das Schulverwaltungssystem den folgenden Befehl verwendet, um einen neuen Schüler hinzuzufügen:

``` sql
INSERT INTO TABLE students (name) values ("Vorname").
```

Dabei wird Vorname durch den Namen des Schülers ersetzt. Wird Bobbys Name eingesetzt, ergibt sich folgender Befehl:

``` sql
INSERT INTO TABLE students (name) values ("Bobby"); DROP TABLE students; --")
```

Durch das Anführungszeichen im Namen denkt die Datenbank, dass der Vorname dort zu Ende ist und interpretiert den daruffolgenden Text als Befehl, sämtliche Schülerdaten zu löschen.


## Links zum Lesen / Schauen

* [:youtube: Hacker - die Guten und die Bösen, SRF][1]
* [:link: Was ist eine Sicherheitslücke?][2]
* [:link: Was ist ein Computervirus?][3]
* [:link: Was ist ein Trojaner?][4]

## Swiss Cyber Storm

**Swiss Cyber Storm** ist eine internationale Konferenz zum Thema Internetsicherheit, welche in der Schweiz stattfindet. Diese Organisation führt auch die **Swiss Hacking Challange** durch, einem Wettbewerb für Jugendliche.

* [:link: Swiss Cyber Storm][11]
* [:link: Swiss Hacking Challange][12]

[^1]: Quelle: [Wikipedia](https://de.wikipedia.org/wiki/Hacker)

[1]: https://www.srf.ch/play/tv/srf-myschool/video/hacker---die-guten-und-die-boesen?id=ae8c581e-2f44-4a59-b8c5-59f07a25db3e
[2]: https://www.securityinfo.ch/artikel/sicherheitsluecke/was-ist-eine-sicherheitsluecke-einfach-erklaert.html
[3]: https://www.securityinfo.ch/artikel/computervirus/was-ist-ein-computervirus-einfach-erklaert.html
[4]: https://www.securityinfo.ch/artikel/trojaner/was-ist-ein-trojaner-einfach-erklaert.html

[11]: https://www.swisscyberstorm.com/
[12]: https://swiss-hacking-challenge.com/
