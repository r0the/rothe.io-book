# Chrome
---

## Werbeblocker

* [:link: uBlock Origin][1]

## Skriptblocker

* [:link: NoScript][2]

[1]: https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm?hl=en
[2]: https://chrome.google.com/webstore/detail/noscript/doojmbjmlfjjnbmnoijecmcbfeoakpjm?hl=en


https://addons.mozilla.org/de/firefox/addon/ublock-origin/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search
