# Sicherheit
---

> Sicherheit bezeichnet einen Zustand, der **frei von unvertretbaren Risiken** ist oder als **gefahrenfrei** angesehen wird. Mit dieser Definition ist Sicherheit sowohl auf ein einzelnes Individuum als auch auf andere Lebewesen, auf unbelebte reale Objekte oder Systeme wie auch auf abstrakte Gegenstände bezogen.
>
> Quelle: [Wikipedia][11]

## Gefahren

- finanzieller Schaden
- Verlust von Daten
- soziale Probleme
- gesundheitliche Probleme

## Apps

::: cards 3
### [:mdi-whatsapp: WhatsApp][1]

***
### [:mdi-instagram: Instagram][2]

***
### [:mdi-snapchat: Snapchat][3]
:::


[1]: ?page=whatsapp
[2]: ?page=instagram
[3]: ?page=snapchat
[11]: https://de.wikipedia.org/wiki/Sicherheit
