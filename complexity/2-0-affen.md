# Kartenpuzzle[^1]

Kennst du das Affenpuzzle?

![](images/affenpuzzle3x3.png)

Das Puzzle besteht (hier) aus neun Affen-Karten, die man passend aneinander legen muss.

::: exercise Aufgabe 1
1. Drucke das Puzzle aus, schneide die Karten aus und versuche, das Puzzle zu lösen.

2. Wenn Du das Puzzle geschafft hast, kannst Du die Variante mit 4x4 Karten probieren.

3. Für Mutige: Das Affenpuzzle gibt es auch in einer 5x5-Version.

4. Zum Experimentieren kannst du auch das folgende Javascript-Puzzle benutzen. Ziehe die Karten an die gewünschte Position und drehe sie, indem du auf sie klickst.
:::

## Ein Algorithmus zur Lösung des Affenpuzzles
Selbst bei recht einfachen Puzzles wie dem 3x3-Affenpuzzle oben kann es eine Weile dauern, bis man die Lösung gefunden hat. Bei größeren Puzzles wie dem 4x4- bzw. 5x5-Puzzle dauert es schon sehr lange bzw. kommt man ans Verzweifeln. Man muss viele Kartenkonfigurationen ausprobieren, um sie dann doch wieder zu verwerfen.

Das Erzeugen und Überprüfen sehr vieler Kartenkonfigurationen ist eine Aufgabe, die man gut an einen Computer übertragen kann.

Ein naiver Algorithmus zur Lösung des Affenpuzzles geht von folgender Idee aus:

Man wählt eine bestimmte Kartenreihenfolge aus (eine sog. Permutation der Ausgangskartenreihe). Zudem wählt man eine bestimmte Orientierung der einzelnen Karten (jede Karte kann ja in 4 unterschiedlichen Ausrichtungen hingelegt werden). Wenn man jetzt die zur Permutation und Orientierung gehörende Kartenreihe in das 3x3-Kartenfeld legt (z.B. von links oben nach rechts unten), dann kann man anschließend überprüfen, ob die Kartenübergänge alle stimmen.

```
ALGORITHMUS affenpuzzle(n):
    erzeuge n*n Karten
    erzeuge die Startpermutation der Karten
    SOLANGE die letzte Permutation noch nicht erreicht und noch keine Lösung gefunden ist:
        erzeuge eine Startorientierung der Karten
        SOLANGE die letzte Orientierung noch nicht erreicht ist:
            erzeuge das Kartenfeld zur Permutation und zur Orientierung
            überprüfe das Kartenfeld
            WENN ok:
                Lösung = Kartenfeld
            erzeuge die nächste Orientierung
        erzeuge die nächste Permutation
    Rückgabe: (Karten, Lösung)
```


::: exercise Aufgabe 2
Du musst nicht alle Details der Implementierung verstehen. Wichtig für die Benutzung ist vor allem die Funktion `affenpuzzle(n)`. Der Parameter `n` steht hier für die Größe des Puzzles. Bei einem 3x3-Puzzle beträgt die Größe `n = 3`.

1. Beim Testen ist die Größe `n = 2` voreingestellt. Führe die Testanweisungen mehrere Male aus und versuche zu verstehen, wie die Karten im Implementierungsprogramm dargestellt werden. Beachte, dass es vorkommen kann, dass die erzeugten Karten keine Puzzle-Lösung haben.

2. Stelle beim Testen die Größe `n = 3` ein. Wenn du jetzt die Ausführung startest, musst du erst einmal sehr lange warten. Wenn du ungeduldig wirst, dann versuche mit folgender Strategie die Wartezeit abzuschätzen:

Schritt 1: Berechne, wie viele Konfigurationen erzeugt werden. Überlege Dir, wie viele Permutationen der Ausgangskartenreihe möglich sind und wie viele Orientierungen jede Permutation zulässt. Zur Kontrolle: 95126814720

Schritt 2: In der Implementierung ist bereits eine Variable `zaehler` vorgesehen, die die erzeugten Konfigurationen mitzählt. Ergänze im Programm eine Ausgabeanweisung, die bei jeweils 1 Million überprüfter Konfigurationen eine Ausgabe auf dem Bildschirm macht. Stoppe dann die Zeit, die für 1 Million Konfigurationsüberprüfungen benötigt wird.

Schritt 3: Mit den Ergebnissen aus Schritt 1 und Schritt 2 kannst du jetzt eine Hochrechnung machen.
:::

## Das Problem und seine Größe

Die Problemgröße lässt sich hier durch die Anzahl der Karten, die eine Seite des Kartenfeldes bilden, beschrieben. Das zur Abbildung oben gehörende Problem hat demnach die Problemgröße n = 3.

## Abschätzung des Berechnungsaufwands
Implementiert man den gezeigten Algorithmus, so zeigen bereits Testaufrufe für kleine Problemgrößen, dass man mit sehr langen Laufzeiten rechnen muss. Bereits für die Problemgröße n = 3 verliert man die Geduld, auf ein Ergebnis zu warten.

In Fällen wie diesem, in denen nicht klar ist, wann mit einem Ergebnis zu rechnen ist, empfiehlt es sich, den Berechnungsaufwand genauer zu analysieren und abzuschätzen.

Der Berechnungsaufwand wird hier wesentlich durch die Anzahl der Kartenkonfigurationen bestimmt, die hier systematisch erzeugt und überprüft werden. Mit der Kostenfunktion K(n) beschreiben wir diesen Berechnungsaufwand: K(n) gibt an, wie viele verschiedene Kartenkonfigurationen es bei einer Problemgröße n gibt.

Die Herleitung einer Formel für K(n) soll hier für den Fall n = 3 durchgespielt werden.

Im Fall n = 3 gibt es insgesamt 3*3 = 9 Karten.

Wenn man jetzt eine Kartenreihenfolge bildet, dann gibt es 9 Möglichkeiten für die erste Karte, 8 Möglichkeiten für die zweite Karte, 7 Möglichkeiten für die dritte Karte, ... 2 Möglichkeiten für die achte Karte und schließlich 1 Möglichkeit für die neunte Karte.

Ingesamt können bei 9 Karten also 9*8*7*6*5*4*3*2*1 verschiedene Kartenreihenfolgen (Permutationen) gebildet werden. Mathematiker schreiben auch 9! (gelesen: 9 Fakultät) für das Produkt 9*8*7*6*5*4*3*2*1.

Wenn eine bestimmte Kartenreihenfolge vorliegt, dann können alle 9 Karten in jeweils 4 verschiedenen Weisen ausgerichtet werden. Zu jeder Kartenreihenfolge gibt es also 4*4*4*4*4*4*4*4*4 verschiedene Orientierungen der Karten. Mathematiker schreiben für das Produkt 4*4*4*4*4*4*4*4*4 kurz 49.

Wenn man beide Ergebnisse zusammenfasst, dann ergeben sich im Fall n = 3 insgesamt 49*9! bzw. 4(3*3)*(3*3)! verschiedene Kartenkonfigurationen.

Wenn man die Überlegungen verallgeinert, so erhält man die Formel:

K(n) = 4(n*n)*(n*n)!

## Praktische Anwendbarkeit des Algorithmus
Wir berechnen zunächst einige Werte der Kostenfunktion K(n) = 4(n*n)*(n*n)!:

T(2) = 6144
T(3) = 95126814720
T(4) = 89862698310039502848000
T(5) = 17464069942802730897824646237782016000000
T(6) = 1756688818283804381631563107501689976914509549544878899200000000
...
Die wenigen Werte zeigen bereits, dass die Funktion K(n) sehr schnell wächst und bereits für kleine n-Werte riesige Funktionswerte liefert.

Wenn man davon ausgeht, dass ein Rechner zur Erzeugung und Überprüfung von 1 Million Konfigurationen 1 Sekunde benötigt, dann besagt der Wert T(3) = 95126814720, dass für ein 3x3-Puzzle im ungünstigsten Fall mehr als 95126 Sekunden (das ist mehr als ein Tag) benögt werden.

::: exercise Aufgabe 1
1. Schätze ab, wie lange ein Rechner im ungünstigsten Fall zur Überprüfung eines 4x4-Puzzles (5x5-Puzzles) benötigt. Gehe hier zunächst auch davon aus, dass ein Rechner zur Erzeugung und Überprüfung von 1 Million Konfigurationen 1 Sekunde benötigt.

2. Die Erfahrung lehrt, dass künftige Rechnergenerationen viel schneller sind. Wie wirkt es sich aus, wenn Rechner 1000mal bzw. 1000000mal schneller sind als in der Hochrechnung oben angenommen wurde?

3. Warum kann man sagen, dass der Algorithmus praktisch nicht anwendbar ist?

4. Mache Vorschläge, wie man den naiven Algorithmus verbessern könnte.
:::

## Verbesserte Algorithmen
Der oben gezeigte naive Algorithmus zur Bestimmung der Lösung eines Affenpuzzles ist in der Praxis nicht anwendbar. Nur für sehr kleine Problemgrößen erhält man in akzeptabler Zeit ein Ergebnis.

Diese Eigenschaft spiegelt sich im Wachstumsverhalten der Kostenfunktion wider. Die Kostenfunktion K(n) = 4(n*n)*(n*n)! wächst schneller als jede Exponentialfunktion.

Man kann jetzt versuchen, durch Überlegungen die Anzahl der zu überprüfenden Kartenkonfigurationen zu reduzieren (vgl. auch Affenpuzzle_GeroScholz.pdf).

Verbesserte Algorithmen zeigen dann ein durchaus akzeptables Laufzeitverhalten für Problemgrößen bis n = 5. Mit wachsender Problemgröße zeigen sich aber dieselben Schwierigkeiten wie beim naiven Algorithmus: Die Kostenfunktion wächst so schnell, dass eine geringe Erhöhung der Problemgröße das Laufzeitverhalten explodieren lässt.

Algorithmen mit einer exponentiellen oder noch ungünstigeren (Zeit-) Komplexität gelten als praktisch nicht anwendbar, da nur für kleine Problemgrößen akzeptable Laufzeiten - auch bei künftigen Rechnergenerationen - zu erwarten sind.

Ob es Algorithmen zur Lösung des Affenpuzzles gibt, deren (Zeit-) Komplexität günstiger als exponentiell ist, ist nicht bekannt.


[^1]: Quelle: Klaus Becker, [Komplexität von Algorithmen und Problemen](https://www.inf-schule.de/grenzen/komplexitaet/affenpuzzle), inf-schule.de, CC BY-SA 4.0
