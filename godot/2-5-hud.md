# HUD
---

The final piece our game needs is a UI: an interface to display things
like score, a "game over" message, and a restart button. Create a new
scene, and add a :ref:`CanvasLayer <class_CanvasLayer>` node named ``HUD``. "HUD" stands for
"heads-up display", an informational display that appears as an
overlay on top of the game view.

The :ref:`CanvasLayer <class_CanvasLayer>` node lets us draw our UI elements on
a layer above the rest of the game, so that the information it displays isn't
covered up by any game elements like the player or mobs.

The HUD displays the following information:

-  Score, changed by ``ScoreTimer``.
-  A message, such as "Game Over" or "Get Ready!"
-  A "Start" button to begin the game.

The basic node for UI elements is :ref:`Control <class_Control>`. To create our UI,
we'll use two types of :ref:`Control <class_Control>` nodes: :ref:`Label <class_Label>`
and :ref:`Button <class_Button>`.

Create the following as children of the ``HUD`` node:

-  :ref:`Label <class_Label>` named ``ScoreLabel``.
-  :ref:`Label <class_Label>` named ``MessageLabel``.
-  :ref:`Button <class_Button>` named ``StartButton``.
-  :ref:`Timer <class_Timer>` named ``MessageTimer``.

Click on the ``ScoreLabel`` and type a number into the *Text* field in the
Inspector. The default font for ``Control`` nodes is small and doesn't scale
well. There is a font file included in the game assets called
"Xolonium-Regular.ttf". To use this font, do the following for each of
the three ``Control`` nodes:

1. Under "Custom Fonts", choose "New DynamicFont"

![](images/custom_font1.png)

2. Click on the "DynamicFont" you added, and under "Font/Font Data",
   choose "Load" and select the "Xolonium-Regular.ttf" file. You must
   also set the font's ``Size``. A setting of ``64`` works well.

![](images/custom_font2.png)

.. note:: **Anchors and Margins:** ``Control`` nodes have a position and size,
          but they also have anchors and margins. Anchors define the
          origin - the reference point for the edges of the node. Margins
          update automatically when you move or resize a control node. They
          represent the distance from the control node's edges to its anchor.
          See :ref:`doc_design_interfaces_with_the_control_nodes` for more details.

Arrange the nodes as shown below. Click the "Layout" button to
set a Control node's layout:

![](images/ui_anchor.png)

You can drag the nodes to place them manually, or for more precise
placement, use the following settings:

## ScoreLabel

-  *Text* : ``0``
-  *Layout* : "Top Wide"
-  *Align* : "Center"

## MessageLabel

-  *Text* : ``Dodge the Creeps!``
-  *Layout* : "HCenter Wide"
-  *Align* : "Center"
-  *Autowrap* : "On"

## StartButton

-  *Text* : ``Start``
-  *Layout* : "Center Bottom"
-  *Margin* :

   -  Top: ``-200``
   -  Bottom: ``-100``

Now add this script to ``HUD``:

``` gdscript
extends CanvasLayer

signal start_game
```

The ``start_game`` signal tells the ``Main`` node that the button
has been pressed.

``` gdscript
func show_message(text):
    $MessageLabel.text = text
    $MessageLabel.show()
    $MessageTimer.start()
```

This function is called when we want to display a message
temporarily, such as "Get Ready". On the ``MessageTimer``, set the
``Wait Time`` to ``2`` and set the ``One Shot`` property to "On".

``` gdscript
func show_game_over():
    show_message("Game Over")

    yield($MessageTimer, "timeout")

    $MessageLabel.text = "Dodge the\nCreeps!"
    $MessageLabel.show()

    yield(get_tree().create_timer(1), "timeout")

    $StartButton.show()
```

This function is called when the player loses. It will show "Game
Over" for 2 seconds, then return to the title screen and, after a brief pause,
show the "Start" button.

.. note:: When you need to pause for a brief time, an alternative to using a
          Timer node is to use the SceneTree's ``create_timer()`` function. This
          can be very useful to delay, such as in the above code, where we want
          to wait a little bit of time before showing the "Start" button.

``` gdscript
func update_score(score):
    $ScoreLabel.text = str(score)
```

This function is called by ``Main`` whenever the score changes.

Connect the ``timeout()`` signal of ``MessageTimer`` and the
``pressed()`` signal of ``StartButton``.

``` gdscript
func _on_StartButton_pressed():
    $StartButton.hide()
    emit_signal("start_game")

func _on_MessageTimer_timeout():
    $MessageLabel.hide()
```

## Connecting HUD to Main

Now that we're done creating the ``HUD`` scene, save it and go back to ``Main``.
Instance the ``HUD`` scene in ``Main`` like you did the ``Player`` scene, and
place it at the bottom of the tree. The full tree should look like this,
so make sure you didn't miss anything:

![](images/completed_main_scene.png)

Now we need to connect the ``HUD`` functionality to our ``Main`` script.
This requires a few additions to the ``Main`` scene:

In the Node tab, connect the HUD's ``start_game`` signal to the
``new_game()`` function of the Main node.

In ``new_game()``, update the score display and show the "Get Ready"
message:

``` gdscript
    $HUD.update_score(score)
    $HUD.show_message("Get Ready")
```

In ``game_over()`` we need to call the corresponding ``HUD`` function:

``` gdscript
$HUD.show_game_over()
```

Finally, add this to ``_on_ScoreTimer_timeout()`` to keep the display in
sync with the changing score:

``` gdscript
$HUD.update_score(score)
```

Now you're ready to play! Click the "Play the Project" button. You will
be asked to select a main scene, so choose ``Main.tscn``.

## Removing old creeps

If you play until "Game Over" and then start a new game the creeps from the
previous game are still on screen. It would be better if they all disappeared
at the start of a new game.

We'll use the ``start_game`` signal that's already being emitted by the ``HUD``
node to remove the remaining creeps. We can't use the editor to connect the
signal to the mobs in the way we need because there are no ``Mob`` nodes in the
``Main`` scene tree until we run the game. Instead we'll use code.

Start by adding a new function to ``Mob.gd``. ``queue_free()`` will delete the
current node at the end of the current frame.

``` gdscript
func _on_start_game():
    queue_free()
```

Then in ``Main.gd`` add a new line inside the ``_on_MobTimer_timeout()`` function,
at the end.

``` gdscript
$HUD.connect("start_game", mob, "_on_start_game")
```

This line tells the new Mob node (referenced by the ``mob`` variable) to respond
to any ``start_game`` signal emitted by the ``HUD`` node by running its
``_on_start_game()`` function.
