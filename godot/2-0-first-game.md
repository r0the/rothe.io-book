# Erstes Spiel
---

Dieses Tutorial hilft dir, dein erstes Godot-Projekt zu erstellen. Du lernst, wie der Godot-Editor funktioniert, wie ein Projekt strukturiert wird und wie ein 2D-Spiel aufgebaut wird.

Das Spiel heiss «Dodge the Creeps!». Die Spielfigur muss sich bewegen und den Feinden so lange wie möglich ausweichen. So sieht das fertige Spiel aus:

![](images/dodge_preview.gif)

**Wieso 2D?** 3D-Spiele sind viel komplexer als 2D-Spiele. Du solltest erst 2D-Spiele entwickeln, bis du der Spielentwicklungsprozess gut verstehst.

## Projektdateien

Hier findest du eine Musterlösung für dieses Projekt:

* [:link: Godot Dodge][1]

---
Quelle: [Godot Docs][2], von Juan Linietsky, Ariel Manzur

[1]: https://github.com/kidscancode/Godot3_dodge/releases
[2]: https://docs.godotengine.org/en/3.1/getting_started/step_by_step/your_first_game.html#finishing-up
