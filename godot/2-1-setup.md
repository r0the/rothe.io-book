# Projekt erstellen
---

Starte Godot und erstelle ein neues Projekt. Lade anschliessend den folgenden ZIP-Datei herunter:

* [:download: dodge_assets.zip](resources/dodge_assets.zip)

Die ZIP-Datei enthält alle Bilder und Soundeffekte, welche du für das Spiel benötigst.

Entpacke die Dateien in deinen Projekt-Ordner.

::: box info
Für dieses Tutorial gehen wir davon aus, dass du den Editor kennst. Ansonsten solltest du zuerst [Szenen und Knoten]() lesen, dort findest du eine Erklärung, wie ein Projekt eingerichtet und der Editor benutzt wird.
:::

Dieses Spiel wird im Porträt-Modus laufen, also müssen wir die Grösse des Spielfensters anpassen. Klicke dazu auf _Project ‣ Project Settings ‣ Display ‣ Window_ und setze _Width_ auf den Wert _480_ und _Height_ auf den Wert _720_.

## Projekt organisieren

In diesem Projekte werden wir drei unabhängige Szenen erstellen: **Player**, **Mob** und **HUD**. Diese werden in der Hauptszene des Spiels **Main** zusammengefasst. In einem grösseren Projekt kann es sinnvoll sein, eigene Ordner für die verschiedenen Szenen zu erstellen, aber für dieses Spiel kannst du alle Szenen und Skripts im Hauptordner _res://_ des Projekts speichern. Du siehst die Dateien und Ordner des Projekts im _FileSystem_-Bereich in der unteren linken Ecke:

![](images/filesystem_dock.png)
