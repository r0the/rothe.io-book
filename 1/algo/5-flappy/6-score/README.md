# Punke zählen
---

## Punkte zählen

Die Punkte zählen wir mit einer neue Variable `score` im Aktor `bird`. Im Unterprogramm `reset_bird` wird der Punktestand auf Null gesetzt:

``` python
def reset_bird():
    # ...
    bird.score = 0
```

Jedes Mal, wenn der Vogel erfolgreich durch ein Hindernis geflogen ist, wird der Punktestand um eins erhöht:

``` python
def move_pipes():
    # ...
    if pipe_top.right < 0:
        reset_pipes()
        bird.score = bird.score + 1
```

## Punktestand anzeigen

Der Punktestand kann mit `screen.draw.text` auf dem Fenster ausgegeben werden:

``` python
def draw():
    # ...
    screen.draw.text(str(bird.score), left=20, top=20, fontsize=60)
```

Da die Ausgabe nur für Text vorgesehen ist, der Punktestand aber eine Zahl ist, muss sie mit `str` erst in einen Text umgewandelt werden. Mit `left=` und `top=` kann der ausgegebene Text positioniert werden. Mit `fontsize=` wird die Schriftgrösse gewählt.

::: exercise Aufgabe 7
1. Erweitere dein Programm so, dass die Punkte gezählt werden.
2. Erweitere dein Programm so, dass die maximal erreichte Punktzahl angezeigt wird.
***
``` python ./flappy_bird_7.py
```
:::

::: exercise Aufgabe 8
Erweitere dein Programm um eine Highscore-Liste.
- Die Liste wird in einer Datei gespeichert.
- Wenn das Programm gestartet wird, wird die Liste angezeigt.
- Nach einem Tastendruck beginnt das Spiel.
- Nach Ende des Spiels kann der Name eingetippt werden.
- Der Neue Score wird in der Liste gespeichert.

***

``` python ./flappy_bird_8.py
```

:::
