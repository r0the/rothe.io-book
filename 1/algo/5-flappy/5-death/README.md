# Kollision und Tod
---

~~~ python
actor_a.colliderect(actor_b)
~~~
überprüft, ob `actor_a` mit `actor_b` kollidiert. Dies kann als Bedingung für eine Bedingte Ausführung verwendet werden:
``` python
if bird.colliderect(pipe_top) or bird.colliderect(pipe_bottom):
    bird.image = "bird_dead"
```
Um festzuhalten, dass der Vogel gestorben ist, wird das Bild geändert. Wenn der Vogel gestorben ist, dürfen folgende Anweisungen nicht mehr ausfeführt werden:
- Bewegen auf Tastendruck
- Setzten des «normalen» Bilds, wenn der Vogel sich nach unten bewegt

Dies wird mit einer bedingten Anweisung mit der Bedingung `bird.image != "bird_dead"` programmiert, beispielsweise:

``` python
def on_key_down():
    if bird.image != "bird_dead":
        bird.vy = -FLAP_STRENGTH
        bird.image = "bird_flap"
```

::: exercise Aufgabe 6
Erweitere dein Programm so, dass der Vogel...
1. grau (Bild `"bird_dead"`) wird, sobald er ein Hindernis trifft,
2. nicht mehr fliegen kann, sobald er gestorben ist,
3. im Unterprogramm `reset_bird` wieder lebendig wird.
***
``` python ./flappy_bird_6.py
```
:::
