import pgzrun
import random

TITLE = "Flappy Bird"
WIDTH = 360
HEIGHT = 640

GAP = 160
SPEED = 3
GRAVITY = 0.3
FLAP_STRENGTH = 6.5

background = Actor("background")

pipe_top = Actor("pipe_top")
pipe_bottom = Actor("pipe_bottom")

bird = Actor("bird")
bird.x = 75
bird.max_score = 0


def reset_pipes():
    pipe_gap_y = random.randint(GAP, HEIGHT - GAP)
    pipe_top.bottom = pipe_gap_y - GAP / 2
    pipe_bottom.top = pipe_gap_y + GAP / 2
    pipe_top.left = WIDTH
    pipe_bottom.left = WIDTH


def move_pipes():
    pipe_top.left = pipe_top.left - SPEED
    pipe_bottom.left = pipe_bottom.left - SPEED
    if pipe_top.right < 0:
        reset_pipes()
        bird.score = bird.score + 1
        bird.max_score = max(bird.score, bird.max_score)


def reset_bird():
    bird.y = 200
    bird.vy = 0
    bird.image = "bird"
    bird.score = 0


def move_bird():
    bird.vy = bird.vy + GRAVITY
    if bird.vy > 0 and bird.image != "bird_dead":
        bird.image = "bird"
    bird.y = bird.y + bird.vy
    if bird.top > HEIGHT:
        reset_bird()
        reset_pipes()


def update():
    move_pipes()
    move_bird()
    if bird.colliderect(pipe_top) or bird.colliderect(pipe_bottom):
        bird.image = "bird_dead"


def on_key_down():
    if bird.image != "bird_dead":
        bird.vy = -FLAP_STRENGTH
        bird.image = "bird_flap"


def draw():
    background.draw()
    pipe_top.draw()
    pipe_bottom.draw()
    bird.draw()
    screen.draw.text(str(bird.score), left=20, top=20, fontsize=60)
    screen.draw.text(str(bird.max_score), left=20, top=70, fontsize=60)


reset_bird()
reset_pipes()

pgzrun.go()
