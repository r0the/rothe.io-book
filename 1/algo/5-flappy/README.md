# :extra: Tutorial «Flappy Bird»
---

::: warning
**Achtung** Dieses Tutorial ist für fortgeschrittene Schüler*innen gedacht, welche sich mit Python und Pygame Zero relativ sicher fühlen.
:::

Für dieses Tutorial kannst du einen vorbereiteten ZIP-Ordner herunterladen:

::: exercise Aufgabe 1 – Vorbereitung

1. Lade den folgenden ZIP-Ordner herunter:

* [:zip: flappy_bird.zip](./flappy_bird.zip)

2. Klicke die ZIP-Datei mit der **rechten** Maustaste an und wähle den Menüpunkt _Alle extrahieren…_:

    ![](./zip-extract-1.png)

3. Wähle mit _Durchsuchen_ den gewünschten Ordner und klicke auf _Extrahieren_:

    ![](./zip-extract-2.png)

4. Starte den Mu-Editor.

5. Klicke auf _Load_ und öffne die Datei _flappy_bird.py_.
:::
