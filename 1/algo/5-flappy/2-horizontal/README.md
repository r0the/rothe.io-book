# Horizontale Bewegung
---

Flappy Bird ist ein Side-Scroller. Das heisst, dass sich in horizontaler Richtung nicht die Spielfigur, sondern die «fixen» Objekte bewegen. Hier sind dies die Hindernisse.

Auch die horizontale Geschwindigkeit beeinflusst die Schwierigkeit des Spiels, also definieren wir folgende Variable:
``` python
SPEED = 3
```
Die Bewegung der Hindernisse definieren wir in einem eigenen Unterpgoramm, welches wir `move_pipes` nennen. Das Unterprogramm muss aus dem Unterprogramm `update` aufgerufen werden.

Wenn die Hindernisse am linken Rand verschwunden sind, sollen sie zurückgesetzt werden, indem das in Aufgabe 2 definierte Unterprogramm `reset_pipes` aufgerufen wird:

![Bewegung der Hindernisse](./flappy-bird-flowchart-1.svg)

::: exercise Aufgabe 3
1. Erstelle ein Unterprogramm `move_pipes`, welches den obenstehenden Ablauf hat.
2. Erstelle ein Unterprogramm `update`, welches das Unterprogramm `move_pipes` aufruft.
2. Erweitere das Unterprogramm `reset_pipes` so, dass die Hindernisse ganz nach rechts gesetzt werden, so dass der linke Rand der Hindernisse auf dem rechten Rand des Fenster liegt.
***
``` python ./flappy_bird_3.py
```
:::
