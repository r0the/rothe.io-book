# Fliegen
---

Der Spieler löst durch das Drücken einer beliebigen Taste einen Flügelschlag aus. Dies soll einerseits durch eine andere Grafik dargestellt werden. Andererseits soll der Volge dadurch nach oben fliegen, also eine negative vertikale Geschwindigkeit erhalten. Auch diese beeinflusst die Schwierigkeit des Spiels und wird so definiert:
``` python
FLAP_STRENGTH = 6.5
```
Beim Drücken einer Taste wird das Unterprogramm `on_key_down` aufgerufen. Also wird dieses im Programm definiert. Dort wird die Geschwindigkeit des Vogels `.vy` gesetzt. Ausserdem wird die Grafik des Vogels geändert, indem die Variable `.image` entsprechend gesetzt wird:
``` python
def on_key_down():
    bird.vy = -FLAP_STRENGTH
    bird.image = "bird_flap"
```

::: exercise Aufgabe 5
1. Setze `FLAP_STRENGTH` auf den Wert `6.5`.
2. Füge das obenstehende Unterprogramm in dein Programm ein.
3. Erweitere das Unterprogramm `move_bird` so, dass der Vogel wieder die Grafik `"bird"` erhält, wenn er sich nach unten bewegt, also wenn die Geschwindigkeit `.vy` grösser als 0 wird.
***
``` python ./flappy_bird_5.py
```
:::
