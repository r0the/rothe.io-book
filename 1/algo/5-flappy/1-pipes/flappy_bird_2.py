import pgzrun
import random

TITLE = "Flappy Bird"
WIDTH = 360
HEIGHT = 640

GAP = 160

background = Actor("background")

pipe_top = Actor("pipe_top")
pipe_bottom = Actor("pipe_bottom")


def reset_pipes():
    pipe_gap_y = random.randint(GAP, HEIGHT - GAP)
    pipe_top.bottom = pipe_gap_y - GAP / 2
    pipe_bottom.top = pipe_gap_y + GAP / 2


def draw():
    background.draw()
    pipe_top.draw()
    pipe_bottom.draw()


reset_pipes()
pgzrun.go()
