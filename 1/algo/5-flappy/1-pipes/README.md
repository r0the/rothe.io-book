# Hindernisse
---

Die Hindernisse werden durch zwei Aktoren `pipe_top` und `pipe_bottom` dargestellt:
``` python
pipe_top = Actor("pipe_top")
pipe_bottom = Actor("pipe_bottom")
```
Die Höhe der Lücke zwischen den Hindernissen ist ein wichtiger Faktor, welches die Schwierigkeit des Spiels beeinflusst. Deshalb definieren wir eine Variable für diesen Wert:
``` python
GAP = 160
```

![](./flappy-bird.svg)

Um das Spiel interessant zu machen, soll die Position der Lücke zufällig sein. Dazu benötigen wir das Zusatzmodul `random` von Python. Es wird mit der Anweisung
``` python
import random
```
in das Programm eingebunden. Nun kann mit der Anweisung
``` python
pipe_gap_y = random.randrange(a, b)
```
der Variable `pipe_gap_y` eine zufällige Zahl zwischen `a` und `b` zugewiesen werden. Die Lücke soll mindestens um ihre halbe Höhe vom Rand entfernt sein, also wählen wir einen zufälligen Wert zwischen `GAP` und `HEIGHT - GAP`.

Da die Hindernisse immer wieder neu positioniert werden müssen, werden die entsprechenden Anweisungen in einem Unterprogramm zusammengefasst. Das Unterprogramm soll `reset_pipes` heissen:

``` python
import random

def reset_pipes():
    pipe_gap_y = random.randrange(GAP, HEIGHT - GAP)
    # Hier Position der Hinternisse setzen

reset_pipes()
```

Am Ende unseres Programms muss das Unterprogramm mit `reset_pipes()` aufgerufen werden, damit die Hindernisse zufällig positioniert werden.

::: exercise Aufgabe 2
Integriere den obenstehenden Code in das Programm `flappy_bird`. Ergänze das Programm so, dass die zwei Hindernisse zufällig positioniert und dargestellt werden.
***
``` python ./flappy_bird_2.py
```
:::
