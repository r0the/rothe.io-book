# Vogel und Schwerkraft
---

Nun wollen wir endlich den Vogel ins Spiel bringen. Zuerst wird ein Aktor angelegt und horizontal positioniert:
``` python
bird = Actor("bird_1")
bird.x = 75
```
Der Vogel unterliegt der Schwerkraft. Deren Stärke ist ein weiterer Einflussfakor auf die Schwierigkeit des Spiels. Wir definieren:
``` python
GRAVITY = 0.3
```
Beim freien Fall des Vogels ändert sich seine Geschwindigkeit. Diese wird in der Variable `.vy` des Vogel-Aktors gespeichert. Bei der Bewegung wird erst die Geschwindigkeit um `GRAVITY` erhöht, anschliessend wird die vertikale Position `.y` um `.vy` erhöht.

::: info Mathematische Herleitung
***
Für die Simulation des freien Falls helfen folgende Überlegungen. Als Ausgangspunkt dienen die Newtonschen Bewegungsgleichungen:

$$ a = \frac{\Delta v}{\Delta t} \qquad\qquad v = \frac{\Delta y}{\Delta t} $$

In der Simulation interessiert die jeweilige Änderung der Geschwindigkeit und der Position, also werden die Gleichungen entsprechend umgeformt:

$$ \Delta v = a \cdot \Delta t \qquad\qquad \Delta y = v \cdot \Delta t $$

In einem Simulationsschritt kann also die neue Geschwindigkeit und Position so bestimmt werden:

$$ \begin{aligned}
  v_{n} &= v_{n-1} + \Delta v = v_{n-1} + a \cdot \Delta t \\
  y_{n} &= y_{n-1} + \Delta y = y_{n-1} + v_n \cdot \Delta t
\end{aligned} $$

Vereinfachend kann $\Delta t = 1$ gewählt werden. Somit ergibt sich:

$$ \begin{aligned}
  v_{n} &= v_{n-1} + a \\
  y_{n} &= y_{n-1} + v_n
\end{aligned} $$
:::

![Bewegung des Vogels](./flappy-bird-flowchart-2.svg)


::: exercise Aufgabe 4
1. Erzeuge im Programm einen neuen Aktor für den Vogel.
2. Erweitere das Unterprogramm `draw` so, dass der Vogel gezeichnet wird.
3. Erstelle ein Unterprogramm `reset_bird`, welches die vertikale Position auf 200 und die vertikale Geschwindigkeit des Vogels auf 0 sezt.
4. Erstelle ein Unterprogramm `move_bird`, welches den freien Fall des Vogels gemäss obenstehendem Flussdiagramm modelliert.
***
``` python ./flappy_bird_4.py
```
:::
