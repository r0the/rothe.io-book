import pgzrun
import random

TITLE = "Flappy Bird"
WIDTH = 360
HEIGHT = 640

GAP = 160
SPEED = 3
GRAVITY = 0.3

background = Actor("background")

pipe_top = Actor("pipe_top")
pipe_bottom = Actor("pipe_bottom")

bird = Actor("bird")
bird.x = 75


def reset_pipes():
    pipe_gap_y = random.randint(GAP, HEIGHT - GAP)
    pipe_top.bottom = pipe_gap_y - GAP / 2
    pipe_bottom.top = pipe_gap_y + GAP / 2
    pipe_top.left = WIDTH
    pipe_bottom.left = WIDTH


def move_pipes():
    pipe_top.left = pipe_top.left - SPEED
    pipe_bottom.left = pipe_bottom.left - SPEED
    if pipe_top.right < 0:
        reset_pipes()


def reset_bird():
    bird.y = 200
    bird.vy = 0


def move_bird():
    bird.vy = bird.vy + GRAVITY
    bird.y = bird.y + bird.vy
    if bird.top > HEIGHT:
        reset_bird()
        reset_pipes()


def update():
    move_pipes()
    move_bird()


def draw():
    background.draw()
    pipe_top.draw()
    pipe_bottom.draw()
    bird.draw()


reset_bird()
reset_pipes()
pgzrun.go()
