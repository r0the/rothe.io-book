# Algorithmen
---

In diesem Kapitel wird erklärt, was Algorithmen sind. Im ersten Abschnitt werden die wichtigsten Begriffe eingeführt. Es folgen Beispiele von Algorithmen, an welchen die Begriffe illustriert werden.

Um sich in das Thema einzuarbeiten, empfiehlt es sich, zuerst anhand der Gruppenarbeit die Beispiele kennenzulernen und erst dann die Theorie zu lesen.

::: goal Lernziele
- Du kannst erklären, was ein Algorithmus ist.
- Du kannst Flussdiagramme verstehen und erstellen.
:::
