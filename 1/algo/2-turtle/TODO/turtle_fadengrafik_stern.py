import turtle

doni = turtle.Turtle()
doni.hideturtle()
doni.speed(9)
for i in range(20):
    doni.penup()
    doni.goto(0, -200 + 10 * i)
    doni.pendown()
    doni.goto(-10 * i, 0)

for i in range(20):
    doni.penup()
    doni.goto(-200 + 10 * i, 0)
    doni.pendown()
    doni.goto(0, 10 * i)

for i in range(20):
    doni.penup()
    doni.goto(0, 200 - 10 * i)
    doni.pendown()
    doni.goto(10 * i, 0)

for i in range(20):
    doni.penup()
    doni.goto(200 - 10 * i, 0)
    doni.pendown()
    doni.goto(0, -10 * i)
