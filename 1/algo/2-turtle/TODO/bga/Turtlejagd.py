"""
Programm:   Turtlejagd.py
Datum:      19.02.2020
Autor:      Andreas Bürge
Zweck:      Mehrere Turtles jagen einander
"""


# Das Turtle Modul einbinden und das Fenster festlegen
import turtle
import math
drawing_area = turtle.Screen()
drawing_area.bgcolor('#444444')
drawing_area.setup(width=800, height=800) # legt die Groesse des Zeichenfensters fest
drawing_area.setworldcoordinates(-10,-10,10,10) # legt die user-Koordinaten von left bottom bis right top fest

# Turtles namens seira, alice, bob, clive anlegen
seira = turtle.Turtle()
alice = turtle.Turtle()
bob = turtle.Turtle()
clive = turtle.Turtle()

v = 50      # Geschwindigkeit der Turtles
size = 3    # Zeichenstiftdicke der Turtles

s_c = 1     # Faktor, mit dem jede Turtle ihre Schrittweite multipliziert, um die naechste zu jagen
a_c = 1
b_c = 1
c_c = 1

seira.color("green")
alice.color("red")
bob.color("yellow")
clive.color("blue")

seira.speed(v)
alice.speed(v)
bob.speed(v)
clive.speed(v)

seira.pensize(size)
alice.pensize(size)
bob.pensize(size)
clive.pensize(size)

def main():

    seira.shape("turtle")   # der Turtle die Form einer Turtle geben
    alice.shape("turtle")
    bob.shape("turtle")
    clive.shape("turtle")

    seira.penup()
    seira.setposition(9, 9)
    seira.pendown()

    alice.penup()
    alice.setposition(-9, 9)
    alice.pendown()

    bob.penup()
    bob.setposition(-9, -9)
    bob.pendown()

    clive.penup()
    clive.setposition(9, -9)
    clive.pendown()

    seira.setheading(seira.towards(alice.position()))
    alice.setheading(alice.towards(bob.position()))
    bob.setheading(bob.towards(clive.position()))
    clive.setheading(clive.towards(seira.position()))
    step = 0.2
    k = 0
    while k < 40:
        seira.forward(s_c*step)
        alice.forward(a_c*step)
        bob.forward(b_c*step)
        clive.forward(c_c*step)
        seira.setheading(seira.towards(alice.position()))
        alice.setheading(alice.towards(bob.position()))
        bob.setheading(bob.towards(clive.position()))
        clive.setheading(clive.towards(seira.position()))
        k = k + step
    # end while k


# Ende main


main()  # Aufruf von main

# --- Ende Programm