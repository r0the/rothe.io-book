"""
Programm:   Fadengrafik_Loesung.py
Datum:      14.02.2020
Autor:      Mein Name
Zweck:      Mit der Turtle eine Fadengrafik zeichnen
"""


# Das Turtle Modul einbinden und das Fenster festlegen
import turtle
import math
drawing_area = turtle.Screen()
drawing_area.setup(width=800, height=800) # legt die Groesse des Zeichenfensters fest

links = 0
unten = 0
rechts = 10
oben = 10
drawing_area.setworldcoordinates(links,unten,rechts,oben)

# Eine Turtle namens seira anlegen
seira = turtle.Turtle()

seira.color("black")
seira.pensize(2)

def main():

    # von unten nach links
    seira.shape("turtle")   # Turtle namens seira anlegen mit der Form einer Turtle
    k = 0
    while (k<=10):
        seira.penup()
        seira.setposition(10-k,0)
        seira.pendown()
        seira.setheading(seira.towards(0,k))
        distance = math.sqrt((10-k-0)**2 + (0-k)**2)
        if (k%2==0):
            seira.color("black")
        else:
            seira.color("yellow")
        seira.fd(distance)
        k = k+1
    # Ende while k

    # von links nach oben
    seira.shape("turtle")   # Turtle namens seira anlegen mit der Form einer Turtle
    k = 0
    while (k<=10):
        seira.penup()
        seira.setposition(0,k)
        seira.pendown()
        seira.setheading(seira.towards(k,10))
        distance = math.sqrt((0-k)**2 + (k-10)**2)
        seira.fd(distance)
        k = k+1
    # Ende while k

    # von oben nach rechts
    seira.shape("turtle")   # Turtle namens seira anlegen mit der Form einer Turtle
    k = 0
    while (k<=10):
        seira.penup()
        seira.setposition(k,10)
        seira.pendown()
        seira.setheading(seira.towards(10,10-k))
        distance = math.sqrt((k-10)**2 + (10-(10-k))**2)
        seira.fd(distance)
        k = k+1
    # Ende while k

    # von rechts nach unten
    seira.shape("turtle")   # Turtle namens seira anlegen mit der Form einer Turtle
    k = 0
    while (k<=10):
        seira.penup()
        seira.setposition(10,10-k)
        seira.pendown()
        seira.setheading(seira.towards(10-k,0))
        distance = math.sqrt((10-(10-k))**2 + (10-k-0)**2)
        seira.fd(distance)
        k = k+1
    # Ende while k

# Ende main

main()  # Aufruf von main

# --- Ende Programm
