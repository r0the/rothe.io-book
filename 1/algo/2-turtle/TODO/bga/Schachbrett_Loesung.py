"""
Programm:   Schachbrett.py
Datum:      19.02.2020
Autor:      Andreas Bürge
Zweck:      Mit der Turtle ein Schachbrett zeichnen
"""


# Das Turtle Modul einbinden und das Fenster festlegen
import turtle
import math
drawing_area = turtle.Screen()
drawing_area.setup(width=800, height=800) # legt die Groesse des Zeichenfensters fest
drawing_area.setworldcoordinates(-1,-1,9,9)

# Eine Turtle namens seira anlegen
seira = turtle.Turtle()

seira.color("black")
#seira.pensize(2)
seira.speed(100)

def main():

    seira.shape("turtle")   # Turtle namens seira anlegen mit der Form einer Turtle

    # rechts nach Mitte oben
    k = 0
    # Schleife k ueber die Zeilen
    while k < 8:
        # Schleife j ueber die Spalten
        j = 0
        while j < 8:
            seira.penup()
            seira.setposition(j, k)
            seira.pendown()
            seira.setheading(90)
            if ((j+k)%2)==0:
                seira.color("black")
            else:
                seira.color("white")
            seira.begin_fill()
            for m in range(4):
                seira.forward(1)
                seira.right(90)
            seira.end_fill()
            j = j + 1
        # end while j
        k = k + 1
    # end while k

    seira.ht()
    seira.penup()
    seira.setposition(0, 0)
    seira.pendown()
    seira.setheading(90)
    seira.pensize(3)
    seira.color("blue")
    for m in range(4):
        seira.forward(8)
        seira.right(90)

# Ende main


main()  # Aufruf von main

# --- Ende Programm