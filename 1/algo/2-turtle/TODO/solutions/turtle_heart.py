import turtle

raphael = turtle.Turtle()

raphael.shape("turtle")
raphael.fillcolor("red")
raphael.begin_fill()
raphael.left(45)
raphael.forward(100)
raphael.circle(50, 180)
raphael.right(90)
raphael.circle(50, 180)
raphael.forward(100)
raphael.end_fill()
