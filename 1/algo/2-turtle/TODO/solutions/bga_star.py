"""
Programm:   Star.py
Datum:      20.01.2020
Autor:      Andreas Bürge
Zweck:      Mit der Turtle eine Fadengrafik zeichnen
"""


# Das Turtle Modul einbinden und das Fenster festlegen
import turtle
import math
drawing_area = turtle.Screen()
drawing_area.setup(width=800, height=800) # legt die Groesse des Zeichenfensters fest
drawing_area.setworldcoordinates(-10,-10,10,10)

# Eine Turtle namens seira anlegen
seira = turtle.Turtle()

seira.color("black")
seira.pensize(2)
seira.speed(100)

n = 25
step = 10/n

def main():

    seira.shape("turtle")   # Turtle namens seira anlegen mit der Form einer Turtle
    seira.ht()
    # rechts nach Mitte oben
    k = 0
    while k <= 10:
        seira.penup()
        seira.setposition(10-k, 0)
        seira.pendown()
        seira.setheading(seira.towards(0,k))
        distance = math.sqrt(k**2+(10-k)**2)
        seira.fd(distance)
        k = k + step
    # end while k

    # Mitte oben nach links
    k = 0
    while k <= 10:
        seira.penup()
        seira.setposition(0,10-k)
        seira.pendown()
        seira.setheading(seira.towards(-k,0))
        distance = math.sqrt(k**2+(10-k)**2)
        seira.fd(distance)
        k = k + step
    # end while k

    # links nach Mitte unten
    k = 0
    while k <= 10:
        seira.penup()
        seira.setposition(-10+k,0)
        seira.pendown()
        seira.setheading(seira.towards(0,-k))
        distance = math.sqrt(k**2+(10-k)**2)
        seira.fd(distance)
        k = k + step
    # end while k

    # Mitte unten nach rechts
    k = 0
    while k <= 10:
        seira.penup()
        seira.setposition(0,-10+k)
        seira.pendown()
        seira.setheading(seira.towards(k,0))
        distance = math.sqrt(k**2+(10-k)**2)
        seira.fd(distance)
        k = k + step
    # end while k

# Ende main


main()  # Aufruf von main

# --- Ende Programm