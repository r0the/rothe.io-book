import turtle

raphael = turtle.Turtle()


def quadrat(laenge):
    raphael.pendown()
    raphael.begin_fill()
    for j in range(4):
        raphael.forward(laenge)
        raphael.left(90)
    raphael.end_fill()
    raphael.penup()


def teppich(laenge, i):
    laenge = laenge / 3
    quadrat(laenge)
    if i > 0:
        i = i - 1
        raphael.backward(2 / 3 * laenge)
        raphael.left(90)
        raphael.forward(4 / 3 * laenge)
        raphael.right(90)
        teppich(laenge, i)
        raphael.forward(laenge)
        teppich(laenge, i)
        raphael.forward(laenge)
        teppich(laenge, i)

        raphael.backward(2 * laenge)
        raphael.right(90)
        raphael.forward(laenge)
        raphael.left(90)
        teppich(laenge, i)
        raphael.forward(2 * laenge)
        teppich(laenge, i)

        raphael.backward(2 * laenge)
        raphael.right(90)
        raphael.forward(laenge)
        raphael.left(90)
        teppich(laenge, i)
        raphael.forward(laenge)
        teppich(laenge, i)
        raphael.forward(laenge)
        teppich(laenge, i)

        raphael.backward(4 / 3 * laenge)
        raphael.left(90)
        raphael.forward(2 / 3 * laenge)
        raphael.right(90)


raphael.speed(0)
raphael.penup()
teppich(300, 3)
raphael.hideturtle()
