import turtle

L = 300
I = 4
raphael = turtle.Turtle()


def seite(laenge, i):
    if i == 0:
        raphael.forward(laenge)
    else:
        laenge = laenge / 3
        i = i - 1
        seite(laenge, i)
        raphael.left(60)
        seite(laenge, i)
        raphael.right(120)
        seite(laenge, i)
        raphael.left(60)
        seite(laenge, i)


raphael.speed(0)
raphael.penup()
raphael.backward(L / 2)
raphael.left(90)
raphael.forward(L * 0.3)
raphael.right(90)
raphael.pendown()

for i in range(3):
    seite(L, I)
    raphael.right(120)

raphael.hideturtle()
