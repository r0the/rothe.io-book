import turtle

turtle.colormode(255)

def winkelfarbe(h):
    c = 255
    f = 4.25
    if 0 <= h < 60:
        return c, int(f * h), 0
    elif 60 <= h < 120:
        return int(f * (120 - h)), c, 0
    elif 120 <= h < 180:
        return 0, c, int(f * (h - 120))
    elif 180 <= h < 240:
        return 0, int(f * (240 - h)), c
    elif 240 <= h < 300:
        return int(f * (h - 240)), 0, c
    elif 300 <= h < 360:
        return c, 0, int(f * (360 - h))
    else:
        return 0, 0, 0

leonardo = turtle.Turtle()
leonardo.speed("fastest")
leonardo.hideturtle()

r1 = 150
r2 = 100

def sektor(winkel, farbe):
    leonardo.pencolor(farbe)
    leonardo.fillcolor(farbe)
    leonardo.begin_fill()
    leonardo.forward(r1)
    leonardo.left(90)
    leonardo.circle(r1, winkel)
    leonardo.left(90)
    leonardo.forward(r1)
    leonardo.left(180)
    leonardo.end_fill()

for i in range(360 // 4):
    sektor(4, winkelfarbe(i * 4))

leonardo.penup()
leonardo.right(90)
leonardo.forward(r2)
leonardo.left(90)
leonardo.pendown()

leonardo.color("white")
leonardo.fillcolor("white")
leonardo.begin_fill()
leonardo.circle(r2)
leonardo.end_fill()
