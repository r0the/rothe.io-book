# Unterprogramme (Turtle)
---

Beim Zeichnen gibt es Figuren (z.B. ein Quadrat), welche immer wieder verwendet werden. Es ist mühsam und aufwändig, den Programmcode für diese Figuren immer wieder zu schreiben. Programmiersprachen bieten deshalb die Möglichkeit, Befehle zu einem **Unterprogramm** zusammenzufassen. Diese Befehle können dann beliebig oft ausgeführt werden, indem das Unterprogramm **aufgerufen** wird.

## Unterprogramm definieren

Um ein Unterprogramm zu **definieren** schreibt man `def` gefolgt vom Namen, welchen man dem Unterprogramm geben will. Anschliessend folgt ein Klammerpaar `()` und ein Doppelpunkt `:`

Nach dieser Zeile folgen eine oder mehrere Zeilen, welche um vier Leerzeichen eingerückt sind. Hier stehen die Befehle, die zum Unterprogramm gehören. Sie werden **nicht** ausgeführt.

~~~ python
def befehl():
    Anweisung A
    Anweisung B
~~~

Im folgenden Beispiel wird ein Unterprogramm mit dem Namen `stufe` definiert, welches eine Stufe zeichnet.

``` python
import turtle

raphael = turtle.Turtle()

def stufe():
    raphael.forward(50)
    raphael.left(90)
    raphael.forward(50)
    raphael.right(90)
```

## Unterprogramm aufrufen

Die in einem Unterprogramm definierten Befehle werden erst ausgeführt, wenn das Unterprogramm **aufgerufen** wird. Dazu wird der Name des Unterprogramms gefolgt von einem Klammerpaar geschrieben:

~~~ python
befehl()
~~~

Im folgenden Beispiel wird das Unterprogramm `stufe` definiert und anschliessend zwei Mal aufgerufen:

``` python
import turtle

raphael = turtle.Turtle()

def stufe():
    raphael.forward(50)
    raphael.left(90)
    raphael.forward(50)
    raphael.right(90)

stufe()
stufe()
```

::: exercise Aufgabe Unterprogramm 1

1. Starte den Mu Editor und Erstelle ein neues Programm.
2. Speichere es unter dem Namen **turtle_sub.py**.
3. Kopiere das obenstehende Programm, welches Stufen zeichnet in den Mu Editor.
4. Führe das Programm aus.
5. Benenne das Unterprogramm um, so dass es `zacke` heisst.
6. Ändere das Programm so ab, dass ein Stern gezeichnet wird.

![](./turtle-star.png)
***
``` python solutions/turtle_star_sub.py
```
:::

::: exercise Zusatzaufgabe Unterprogramm 2

Schreibe ein Programm, welches eine Blume zeichnet. Definiere dabei ein Unterprogramm `blatt`, welches ein einzelnes Blütenblatt zeichnet.

![](./turtle-flower.png)
:::

## Parameter

Oft wollen wir Befehle ausführen, die bis auf einen Wert identisch sind. Um beispielsweise zwei unterschiedliche grosse Stufen zu zeichnen, schreiben wird:

``` python
# kleine Stufe
raphael.forward(50)
raphael.left(90)
raphael.forward(50)
raphael.right(90)
# grosse Stufe
raphael.forward(100)
raphael.left(90)
raphael.forward(100)
raphael.right(90)
```

Auch hier besteht die Möglichkeit, diese Befehle in einem Unterprogramm zusammenzufassen. Für den Wert, welcher unterschiedlich ist, wählen wir den Namen `groesse` und schreiben ihn in die Klammer auf der `def`-Zeile. Anstelle des konkreten Wert wird nun im `forward`-Befehl einfach der Name `groesse` geschrieben. `groesse` wird als **Parameter** des Unterprogramms `stufe` bezeichnet.

``` python
import turtle

raphael = turtle.Turtle()

def stufe(groesse):
    raphael.forward(groesse)
    raphael.left(90)
    raphael.forward(groesse)
    raphael.right(90)

stufe(50)
stufe(100)
```

Auf den zwei letzten Zeilen des Programms wird das Unterprogramm `stufe` mit zwei unterschiedlichen Werten für `groesse` aufgerufen.

::: exercise Aufgabe Unterprogramm 3
Erweitere dein Unterprogramm, **turtle_sub.py** so, dass mit dem Unterprogramm `zacke` Zacken unterschiedlicher Grösse gezeichnet werden können.
:::
