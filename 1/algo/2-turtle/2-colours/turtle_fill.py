import turtle

raphael = turtle.Turtle()

raphael.shape("turtle")
raphael.pencolor("navy")
raphael.fillcolor("sky blue")

raphael.begin_fill()
raphael.forward(100)
raphael.left(120)
raphael.forward(100)
raphael.left(120)
raphael.forward(100)
raphael.end_fill()
