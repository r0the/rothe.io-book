# Farben
---

## Farben und Füllung

Nun erweitern wir unser Repertoire von Turtle-Befehlen:

~~~ python
.penup()
~~~
hebt den Stift an. Wenn sich die Turtle nun bewegt, zeichnet sie nicht mehr.

~~~ python
.pendown()
~~~
senkt den Stift. Wenn sich die Turtle nun bewegt, zeichnet sie wieder.

~~~ python
.pencolor(<mark>farbe</mark>)
~~~
legt die Zeichenfarbe für den Stift fest. Die Farbe muss in Anführungszeichen geschrieben werden.

~~~ python
.fillcolor(<mark>farbe</mark>)
~~~
legt die Farbe für das Füllen von Figuren fest. Die Farbe muss in Anführungszeichen geschrieben werden.

~~~ python
.begin_fill()
~~~
beginnt damit, eine gefüllte Figur zu zeichnen.

~~~ python
.end_fill()
~~~
beendet die Figur und füllt sie aus.

Im folgenden Beispiel zeichnet die Turtle ein ausgefülltes Dreieck. Der Rand wird mit der Farbe `navy` gezeichnet, gefüllt wird mit der Farbe `sky blue`:

``` python ./turtle_fill.py
```

![](./turtle-fill.png)

## Farben festlegen
Es gibt zwei Möglichkeiten, eine Farbe für die Python-Turtle festzulegen. Entweder wird ein vordefinierter Name für eine Farbe verwendet oder es wird ein RGB-Wert angegeben.

### RGB-Werte

Um eine Turtle-Farbe als RGB-Wert festlegen zu können, muss zunächst im Turtle-Modul der richtige Farbmodus eingeschaltet werden:

``` python
turtle.colormode(255)
```

Nun kann der RGB-Wert der Farbe bestimmt werden, z.B. mit dem folgenden Tool:

<VueColourPicker output="tuple"/>

Das Farbtupel kann nun in den Turtle-Befehlen `pencolor` oder `fillcolor` verwendet werden:

``` python
raphael.fillcolor(255, 220, 150)
```

### Farbnamen

Um eine Farbe über einen Namen festzulegen, muss der Name der Farbe in doppelten Anführungszeichen angegeben werden:

``` python
raphael.fillcolor("light sky blue")
```

Die folgende Tabelle zeigt eine Auswahl von möglichen Namen für Farben:

<VueColourTable/>
