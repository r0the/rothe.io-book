# Umgang mit Fehlern
---

## Fehler erkennen

Da Python-Programme schrittweise ausgeführt werden, kann es sein, dass das Programm plötzlich abbricht, weil Python auf einer Zeile ein Fehler entdeckt hat. In diesem Fall wird in der __Shell__ von Thonny eine Fehlermeldung ausgegeben:

![Python zeigt in der Shell von Thonny einen Fehler an](./thonny-error.png)

In der Fehlermeldung lassen sich wichtige Informationen erkennen:

- Auf der letzten Zeile steht die eigentliche Fehlermeldung, im Beispiel oben _NameError: name 'raphael' is not defined_.
- Auf der vorletzten Zeile steht, in welchem Python-Programm und auf welcher Zeile der Fehler aufgetreten ist, im Beispiel oben in _turtle_fehler.py_ auf der Zeile _3_.

Mit einem Klick auf den Link kann man direkt zur Zeile springen, welche den Fehler verursacht hat.

Thonny zeigt zusätzlich im Bereich __Assisstant__ eine Erklärung zum Fehler und mögliche Fehlerursachen an. Der Bereich kann über das Menü _View ‣ Assistant_ eingeblendet werden.

## Häufige Fehler

### NameError

_NameError: name '…' is not defined_

Der angegebene Name ist Python nicht bekannt. Typische Szenarien sind:

- Das Modul `turtle` wird verwendet, aber die `import`-Anweisung fehlt:

  ``` python
  raphael = turtle.Turtle()
  ```

  Korrektur:

  ``` python
  import turtle
  raphael = turtle.Turtle()
  ```

- Der Name der Turtle ist falsch geschrieben worden (`Rahpael` statt `raphael`):

  ``` python
  import turtle
  raphael = turtle.Turtle()
  Rahpael.forward(100)
  ```

  Korrektur:

  ``` python
  import turtle
  raphael = turtle.Turtle()
  raphael.forward(100)
  ```

### AttributeError

_AttributeError: '…' has not attribute '…'_

Auch diese Fehlermeldung erscheint, wenn Python einen Namen nicht zuordnen kann. In diesem Fall handelt es sich um einen Namen, der nach einem Punkt `.` steht. Typische Szenarien sind:

- Der Turtle-Befehl `pencolor` ist in der britischen Schriebweise `pencolour` geschrieben worden:
  ``` python
  import turtle
  raphael = turtle.Turtle()
  raphael.pencolour("red")
  ```

  Korrektur:

  ``` python
  import turtle
  raphael = turtle.Turtle()
  raphael.pencolor("red")
  ```

### IndentationError

_IndentationError: unexpected indent_

Vor einem Befehl steht ein Leerzeichen. Das ist nicht erlaubt, das Einrücken von Befehlen hat eine spezielle Bedeutung in Python. Typische Szenarien sind:

- Die zweite Zeile beginnt mit einem Leerzeichen:

  ``` python
  import turtle
   raphael = turtle.Turtle()
  ```

  Korrektur:

  ``` python
  import turtle
  raphael = turtle.Turtle()
  ```

### SyntaxError

_SyntaxError: invalid syntax_

Normalerweise ein Fehler mit der Interpunktion. Es fehlt ein Zeichen, z.B. ein Gleichheitszeichen `=` oder ein Punkt `.`

- Der Punkt zwischen `Turtle` und `turtle` fehlt:

  ``` python
  import turtle
  raphael = turtle Turtle()
  ```

  Korrektur:

  ``` python
  import turtle
  raphael = turtle.Turtle()
  ```

### TurtleGraphicsError

_turtle.TurtleGraphicsError: bad color sequence: (…, …, …)_

Im Turtle-Befehl `fillcolor` oder `pencolor` wird ein RGB-Farbtupel angegeben, welches von Python nicht verstanden wird. Beispiel:

- Es wird ein RGB-Farbtupel verwendet, aber das Umschalten mit `colormode` fehlt:

  ``` python
  import turtle
  raphael = turtle.Turtle()
  raphael.pencolor(255, 255, 0)
  ```

  Korrektur:

  ``` python
  import turtle
  turtle.colormode(255)
  raphael = turtle.Turtle()
  raphael.pencolor(255, 255, 0)
  ```

_turtle.TurtleGraphicsError: bad color string: …_

Im Turtle-Befehl `fillcolor` oder `pencolor` wird ein Namen für eine Farbe angegeben, welche von Python nicht verstanden wird. Beispiel:

- Ein Namen für eine Farbe wird falsch geschrieben:

  ``` python
  import turtle
  raphael = turtle.Turtle()
  raphael.pencolor("dark tortoise")
  ```

  Korrektur:

  ``` python
  import turtle
  turtle.colormode(255)
  raphael = turtle.Turtle()
  raphael.pencolor("dark turquoise")
  ```


::: exercise Flagge von Laos

1. Kopieren Sie das folgende Python-Programm nach Thonny und speichern Sie es als _laos.py_.
2. Das Programm sollte die Flagge von Laos zeichnen, aber es enthält Fehler. Korrigieren Sie alle Fehler.
3. Obschon das Programm fehlerfrei läuft, wird der weisse Kreis nicht gezeichnet. Wieso? Können Sie den Fehler finden?

``` python ./laos.py
```

:::
