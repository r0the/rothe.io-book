# Erstes Programm
---

::: warning
#### Hintergrund
Ein Python-Programm ist zunächst «nur» ein Text, der Unicode/UTF-8 codiert als Datei gespeichert wird.

Damit das Python-Programm etwas bewirkt, muss es von einem Python-**Interpreter** ausgeführt werden. Ein Interpreter ist ein Programm, welches das Python-Programm Zeile für Zeile liest und in Befehle an das Betriebssystem übersetzt.
:::

## Text-Ausgabe
Häufig brauchen wir eine Rückmeldung des Programms, damit wir ein Ergebnis ausgeben können oder sehen, ob überhaupt etwas geschieht. Dazu bietet Python den `print`-Befehl. Mit ihm können wir Text in der *Konsole* ausgegeben. Der auszugebende Text wird als Argument in runden Klammern und mit Anführungszeichen als Text gekennzeichnet an den `print`-Befehl übergeben:

``` python
print("Hallo Welt")
```

::: exercise Aufgabe 1.1

Starte den Mu-Editor und schreibe das Hallo-Welt-Beispiel ab. Speichere die Datei und starte das Programm.

1. Kannst du einen anderen Text ausgeben?
2. Schaffst du es, mehr als eine Zeile Text auszugeben?
:::

## Kommentare

Kommentare sind Teile eines Programms, welche nicht ausgeführt werden. Sie dienen als zusätzliche Erläuterung des Programms für Menschen. Kommentare können auch genutzt werden, um einzelne Anweisungen kurzzeitig zu deaktivieren.

In Python gibt es zwei Möglichkeiten, Kommentare zu schreiben:

## Einzeiliger Kommentar

Ein einzeiliger Kommentar wird mit einem Hashtag-Zeichen `#` eingeleitet. Alles, was auf der gleichen Zeile folgt, wird als Kommentar interpretiert. So kann zu einer Zeile eine Anmerkung geschrieben werden:

``` python
print("Hallo Welt") # gibt einen Text aus
```

Man kann damit auch Befehle **auskommentieren**, damit sie nicht ausgeführt werden. Das ist hilfreich, wenn man Fehler im Programm suchen will:

``` python
# print("Hallo Welt")
```

::: exercise Aufgabe 1.2
Kommentiere die Befehle deines ersten Programms, indem du hinten an den Befehl mit einen Kommentar anhängst.
:::

## Mehrzeiliger Kommentar

Ein mehrzeiliger Kommentar wird mit drei doppelten Anführungszeichen `"""` eingeleitet. Da er über das Ende der Zeile hinausgeht, muss auch sein Ende gekennzeichnet sein. Dies macht man mit weiteren drei doppelten Anführungszeichen.

Wir verwenden immer den folgenden dreizeiligen Kommentar zu Beginn jeder Python-Datei. So können wir später auf einen Blick erkennen, wer wann was programmiert hat.

``` python
"""
Gibt den Text "Hallo Welt" aus.
Autor: Stefan Rothe (ros)
Datum: 20.11.2019
"""

print("Hallo Welt")
```

::: exercise Aufgabe 1.3

Füge deinem Programm einen standardisierten mehrzeiligen Kommentar (wie im Beispiel) hinzu und passe ihn entsprechend an.
:::
