import turtle
import time

turtle.title("Quadrat")
turtle.shape("turtle")

colors = ["red", "yellow", "green1", "cyan", "blue", "magenta"]

def square():
    turtle.begin_fill()
    for i in range(4):
        turtle.forward(100)
        turtle.left(90)
    turtle.end_fill()


for c in colors:
    turtle.color(c)
    square()
    turtle.left(60)
