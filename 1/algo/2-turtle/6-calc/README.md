# Rechnen
---

In Python kann natürlich auch gerechnet werden. Die Grundrechenoperationen werden in Python folgendermassen geschrieben:

| Operation      | Mathematisch | Python   |
| -------------- | ------------ | -------- |
| Addition       | $a + b$      | `a + b`  |
| Subtraktion    | $a - b$      | `a - b`  |
| Multiplikation | $a \cdot b$  | `a * b`  |
| Division       | $a ÷ b$      | `a / b`  |
| Potenz         | $a^b$        | `a ** b` |

Grundsätzlich kann in Python überall, wo eine Zahl erwartet wird, auch ein Term geschrieben werden. So spielt es keine Rolle, ob man

``` python
raphael.forward(100)
```

oder

``` python
raphael.forward((4 + 5) * 10 + 10)
```

schreibt. Wie man hier sieht, gelten in Python auch die normalen Rechenregeln wie «Punkt vor Strich». Mit Klammern kann angegeben werden, dass eine Strichoperation zuerst berechnet werden soll.

### Rechnen in Schleifen

Der Name `i` in einer Schleife gibt an, wie oft die Schleife schon durchlaufen wurde. Mit `i` kann ebenfalls gerechnet werden. Um zehn Kreise mit einem Radius von 50, 55, 60, … Pixel zu zeichnen, kann man folgendes schreiben:

``` python ./turtle_circles.py
```

![](./turtle-circles.png)

::: exercise Aufgabe 3.1 – Rechtecke

1. Definiere ein Unterprogramm, welches ein Rechteck zeichnet, dass doppelt so breit wie hoch ist.
2. Lasse die Turtle mit Hilfe des Unterprogramms eine Reihe von immer grösseren Rechtecken zeichnen.

![](./turtle-rectangles.png)

***

``` python
def rechteck(seite):
    # Hier Recteck zeichnen

for i in range(20):
    rechteck(50 + 10 * i)
```


***
``` python ./turtle_rectangles.py
```

:::

::: exercise Aufgabe 3.2 – Reguläres Polygon

1. Definiere ein Unterprogramm, welches `n` als Parameter hat und ein reguläres Polygon mit `n` Seiten (also ein n-Eck) zeichnet.
2. Verwende das Unterprogramm, um ein reguläres Dreieck, Viereck, Fünfeck und Sechseck zu zeichnen.

![](./turtle-regular-polygon.png)

***
``` python
def n_eck(n):

for i in range(4):
    n_eck(3 + i)
```
***
``` python ./turtle_regular_polygon.py
```
:::


:::  exercise Aufgabe 3.3 – Farbverlauf
Schreibe ein Turtle-Programm, welches einen Farbverlauf zeichnet:

![](./turtle-gradient.png)

Bei einem Farbverlauf werden die RGB-Werte der Farbe basierend auf den Anzahl Durchläufen der Schleife `i` berechnet.

Um der Turtle eine RGB-Farbe zuweisen zu können muss folgendes beachtet werden:

- Es muss der richtige Farbmodus des `turtle`-Moduls eingeschaltet werden:

  ```
  turtle.colormode(255)
  ```

- Die RGB-Werte müssen ganze Zahlen zwischen 0 und 255 sein.
:::

::: extra Zusatzaufgabe 3.4
Programmiere ein Turtle-Kunstwerk, welches einen Farbverlauf beinhaltet.
:::
