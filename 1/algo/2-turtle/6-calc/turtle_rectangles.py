import turtle

turtle.colormode(255)
raphael = turtle.Turtle()
raphael.shape("turtle")

def rechteck(seite):
    raphael.forward(seite)
    raphael.left(90)
    raphael.forward(seite / 2)
    raphael.left(90)
    raphael.forward(seite)
    raphael.left(90)
    raphael.forward(seite / 2)
    raphael.left(90)

for i in range(20):
    rechteck(50 + 10 * i)
