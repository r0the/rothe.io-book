import turtle

raphael = turtle.Turtle()
raphael.shape("turtle")


def n_eck(n):
    for i in range(n):
        raphael.forward(100)
        raphael.left(360 / n)


for i in range(4):
    n_eck(3 + i)
