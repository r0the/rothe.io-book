import turtle

turtle.colormode(255)
raphael = turtle.Turtle()
raphael.shape("turtle")

def rechteck():
    raphael.begin_fill()
    raphael.forward(20)
    raphael.left(90)
    raphael.forward(100)
    raphael.left(90)
    raphael.forward(20)
    raphael.left(90)
    raphael.forward(100)
    raphael.left(90)
    raphael.end_fill()

for i in range(25):
    raphael.color(10 * i, 0, 255 - 10 * i)
    rechteck()
    raphael.forward(20)
