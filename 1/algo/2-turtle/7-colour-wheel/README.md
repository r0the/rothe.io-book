# :extra: Farbkreis
---

Im Farbkreis werden die gesättigten Farben in einem Kreis angeordnet, wobei ähnliche Farben nebeneinander liegen. Komplementärfarben liegen sich oft gegenüber.

::: columns 2
![HSV-Farbkreis](./turtle-colour-wheel.png)
***
![Ittens Farbkreis ©](./colour-wheel-itten.svg)
:::

## HSV-Farbmodell

Hier verwenden wir den Farbkreis, welcher auf dem HSV-Farbmodell beruht. Dabei wird die Farbe durch den **Farbwinkel** (engl. *hue*) angegeben. Ausserdem werden **Sättigung** (engl. *saturation*) und **Helligkeit** (engl. *value*) angegeben. Hier betrachten wir aber nur den Farbwinkel.

Die Umrechnung eines Winkels $h$ des Farbkreises in einen RGB-Farbwert geschieht gemäss folgender Tabelle:

| Winkel            |                Rot |               Grün |               Blau |
| ----------------- | ------------------:| ------------------:| ------------------:|
| $0° ≤ h < 60°$    |              $255$ |         $4.25 ⋅ h$ |                $0$ |
| $60° ≤ h < 120°$  | $4.25 ⋅ (120 - h)$ |              $255$ |                $0$ |
| $120° ≤ h < 180°$ |                $0$ |              $255$ | $4.25 ⋅ (h - 180)$ |
| $180° ≤ h < 240°$ |                $0$ | $4.25 ⋅ (240 - h)$ |              $255$ |
| $240° ≤ h < 300°$ | $4.25 ⋅ (h - 300)$ |                $0$ |              $255$ |
| $300° ≤ h < 360°$ |              $255$ |                $0$ | $4.25 ⋅ (360 - h)$ |


## Python

Das folgende Python-Unterprogramm berechnet eine RGB-Farbe aus einem Winkel.

``` python
def winkelfarbe(h):
    c = 255
    f = 4.25
    if 0 <= h < 60:
        return c, int(f * h), 0
    elif 60 <= h < 120:
        return int(f * (120 - h)), c, 0
    elif 120 <= h < 180:
        return 0, c, int(f * (h - 120))
    elif 180 <= h < 240:
        return 0, int(f * (240 - h)), c
    elif 240 <= h < 300:
        return int(f * (h - 240)), 0, c
    elif 300 <= h < 360:
        return c, 0, int(f * (360 - h))
    else:
        return 0, 0, 0
```

::: exercise Aufgabe Farbkreis

Verwende für diese Aufgabe das obenstehende Unterprogramm `winkelfarbe`.

1. Erstelle ein Python-Programm, welches den HSV-Farbkreis zeichnet.
2. Erstelle ein Python-Programm, welches einen Regenbogen zeichnet.
:::
