# Erstes Programm
---

## Turtle-Grafik

Bei der **Turtle-Grafik** wird ein Roboter (die *turtle*) programmiert, welcher einen Zeichenstift über ein Papier trägt und so Bilder zeichnen kann. Mit einfachen Befehlen wie «vorwärts», «rückwärts», «nach links drehen» oder «nach rechts drehen» wird der Roboter bewegt und zeichnet so aufs Papier. Gezeichnete Formen können auch ausgefüllt werden. Der Stift kann angehoben und gesenkt werden, so dass sich der Roboter auch ohne zu zeichnen bewegen kann.

## Module

::: warning
#### Hintergrund: Module
Programmiersprachen bieten meist nur sehr grundlegende Befehle wie die Ausgabe von Text oder einfache mathematische Operationen. Für komplexere Aufgaben wie das Zeichnen einer Linie gibt es aber glücklicherweise vordefinierte **Unterprogramme**. Diese werden in sogenannten **Bibliotheken** oder **Modulen** zusammengefasst.
:::

Um in einem Python-Programm vordefinierte Unterprogramme verwenden zu können, muss das entsprechende Modul mit der folgenden Anweisung **importiert** werden:

``` python
import turtle
```

## Erste Turtle

Zuerst muss eine Turtle erstellt werden. Sie kriegt den Namen `raphael`:

``` python
raphael = turtle.Turtle()
```

Die obenstehende Programmzeile hat folgende Bedeutung: «Gib dem Modul `turtle` den Befehl eine `Turtle` zu erstellen. Gib der Turtle den Namen `raphael`.»

::: warning
#### Wichtig: Namen
`raphael`, `turtle` und `Turtle` sind alles **Namen**. Das sind Bezeichnungen für Dinge wir Module, Unterprogramme oder eben Turtles. Für Namen gelten folgende Regeln:

- Gross- und Kleinschreibung ist wichtig, `Turtle` und `turtle` sind unterschiedliche Namen.
- Namen müssen mit einem Buchstaben beginnen und dürfen nur aus Buchstaben, Ziffern und dem Unterstrich `_` bestehen.
:::

Nun können der Turtle `raphael` Befehle erteilt werden, beispielsweise:

``` python
raphael.forward(100)
```

Das bedeutet, dass sich `raphael` um 100 Pixel nach vorne bewegen soll. Ein Befehl an eine Turtle wird immer mit dem `.` an den Namen der Turtle angehängt.

Zunächst lernen wir folgende Turtle-Befehle kennen:

~~~ python
.shape(<mark>name</mark>)
~~~
ändert das Aussehen der Turtle. Für `name` können folgende Werte eingesetzt werden: `"arrow"`, `"turtle"`, `"circle"`, `"square"`, `"triangle"`, `"classic"`.

~~~ python
.forward(<mark>n</mark>)
~~~
bewegt die Turtle um `n` Pixel nach vorne.

~~~ python
.left(<mark>a</mark>)
~~~
dreht die Turtle um `a` Grad nach links.

~~~ python
.right(<mark>a</mark>)
~~~
dreht die Turtle um `a` Grad nach rechts.

Damit können wir das folgende Programm schreiben:

::: columns 2
``` python ./turtle_corner.py
```
***
![gezeichneter 90-Grad-Winkel](./turtle-corner.png)
:::
