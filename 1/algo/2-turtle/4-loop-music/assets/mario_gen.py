import base64

SOLUTION = b"ABCBCDEDFDEDFGHGABCBCIJIKIJIKGLGAIJIKIJIKM"

SOURCE = """
import base64
import io
import pygame

pygame.mixer.init()

played = ""

def play(name):
    global played
    if not name in MIDI:
        print("Takte " + name + " sind nicht vorhanden.")
        return
    played += name
    print("Spiele Takte " + name)
    mid64 = MIDI[name]
    pygame.mixer.music.load(io.BytesIO(base64.b64decode(mid64)))
    pygame.mixer.music.play()
    while pygame.mixer.music.get_busy():
        pygame.time.wait(10)

def check():
    solution = base64.b64decode(SOLUTION).decode("ascii")
    p = len(played) * 2
    start = solution.find(played) * 2 + 1
    if start > 0:
        print("Du hast", p, "Takte ab Takt", start, "richtig gespielt.")
    else:
        print("Die gespielten Takte kommen in dieser Reihenfolge nicht im Stück vor.")
"""

RANGE = "abcdefghijklm"
with open("mario.py", mode="w") as source:
    source.write(SOURCE)
    source.write("SOLUTION = \"")
    source.write(base64.b64encode(SOLUTION).decode("ascii"))
    source.write("\"\nMIDI = {\n")
    for a in RANGE:
        with open("music/mario-" + a + ".mp3", mode="rb") as file:
            mid = file.read()
            source.write("  \"" + a.upper() + "\": \"")
            source.write(base64.b64encode(mid).decode("ascii"))
            source.write("\",\n")
    source.write("}\n")
