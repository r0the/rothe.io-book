from mario import *

def part1():
    play("A")

def part2():
    for i in range(2):
        play("B")
        play("C")

def part3():
    for i in range(2):
        play("D")
        play("E")
        play("D")
        play("F")

def part4():
    play("G")
    play("H")
    play("G")
    play("A")

def part5():
    for i in range(1, 3):
        play("I")
        play("J")
        play("I")
        play("K")

def part6():
    play("G")
    play("L")
    play("G")
    play("A")

part1()
part2()
part3()
part4()
part2()
part5()
part6()
part5()

play("M")

check()
