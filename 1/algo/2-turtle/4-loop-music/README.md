# Schleifen (Musik)
---

Du hast ein Notenblatt als Beispiel für einen Algorithmus kennengelernt. Auf dem Notenblatt werden **Wiederholungszeichen** verwendet, um eine **Wiederholung** zu kennzeichnen.

Auf dieser Seite lernst du, wie in der Programmiersprache Python Befehle wiederholt werden können, indem du das Abspielen eines Musikstück programmierst.

## Vorbereitung

1. Lade das folgende Python-Modul herunter und speichere es in dem Ordner, wo du deine eigenen Python-Programme speicherst:

* [:download: mario.py](./mario.py)

2. Erstelle im Mu Editor ein neues Python-Programm, speichere es als **mario_loesung.py** im gleichen Ordner wie das heruntergeladene _mario.py_.

3. Kopiere den folgenden Python-Code in die neue Datei:

   ``` python ./mario_1.py
   ```

Nun sollte die erste Zeile des Notenblatts [Super Mario Bros: Main Theme][1] gespielt werden. Der Befehl `mario.play("A")` spielt die ersten zwei Takte des Notenblatts. Analog können immer zwei Takte mit einem Befehl abgespielt werden:

![](./mario-line-1.svg)

::: exercise Aufgabe Mario 1

Erweitere das Programm so, dass die Wiederholung auf den nächsten zwei Zeilen abgespielt wird

Vergleiche die abgespielten Takte deines Programms mit der folgenden Webseite:

* [:link: Super Mario Bros: Main Theme][1]
***
``` python ./mario_1_solution.py
```
:::

## Wiederholung

![Wiederholungszeichen](./music-repeat.svg)

Wie auf dem Notenblatt kann auch in Python angegeben werden, dass Befehle wiederholt werden sollen. Um die erste Zeile des Notenblatts mit Wiederholung zu spielen, schreibt man in Python:

``` python ./mario_2.py
```

Der Befehl `for i in range(n)` bedeutet, dass die folgenden eingerückten Zeilen `n` mal wiederholt werden. Die Einrückung sollte immer vier Leerzeichen betragen. Der nächste Befehl, der nicht mehr wiederholt werden soll, wird nicht mehr eingerückt. In Programmiersprachen wird eine solche Wiederholung von Befehlen als **Schleife** bezeichnet.

::: exercise Aufgabe Mario 2

Ändere dein Programm so, dass die zwei Wiederholungen auf den ersten drei Zeilen des Notenblatts in Python als Schleife abgebildet werden.
***
``` python ./mario_2_solution.py
```
:::


::: exercise Aufgabe Mario 3

Erweitere dein Programm so, dass das ganze Mario-Stück abgespielt wird.
:::

[1]: https://musescore.com/user/2072681/scores/2601926
