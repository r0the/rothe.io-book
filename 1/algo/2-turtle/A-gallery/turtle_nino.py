import turtle

nino = turtle.Turtle()
nino.speed(0)


def hexagon():
    for i in range(6):
        nino.forward(100)
        nino.right(60)


def figur(breite, farbe):
    nino.pensize(breite)
    nino.pencolor(farbe)
    for i in range(10):
        hexagon()
        nino.right(36)


figur(40, "red")
figur(30, "green")
figur(20, "yellow")
figur(10, "purple")
figur(5, "black")
figur(1, "white")
nino.hideturtle()
