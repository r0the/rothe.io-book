# Kreise und Bogen

~~~ python
.circle(<mark>radius</mark>, <mark>winkel</mark>)
~~~
zeichnet einen Kreisbogen mit dem angegebenen Radius. Wird der Winkel weggelassen oder auf `360` gesetzt, wird ein ganzer Kreis gefahren. Ist der Winkel `180`, so wird ein Halbkreis gefahren.

Damit lassen sich beispielsweise die folgenden Figuren zeichnen:

::: columns 3
![](./turtle-circle-filled.png)
***
![](./turtle-heart.png)
***
![](./turtle-flower.png)
:::

~~~ python
.pensize(n)
~~~
legt die Dicke des Zeichenstifts auf `n` Pixel fest.

Damit lässt sich beispielsweise das Instagram-Logo zu zeichnen:

![](./turtle-instagram.png)
