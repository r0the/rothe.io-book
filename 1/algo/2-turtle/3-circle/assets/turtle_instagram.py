import turtle

raphael = turtle.Turtle()

raphael.pensize(20)
raphael.circle(50)

raphael.penup()
raphael.forward(100)
raphael.left(90)
raphael.pendown()

raphael.forward(100)
raphael.circle(50, 90)
raphael.forward(100)
raphael.circle(50, 90)
raphael.forward(100)
raphael.circle(50, 90)
raphael.forward(100)
raphael.circle(50, 90)

raphael.penup()
raphael.forward(105)
raphael.left(90)
raphael.forward(35)

raphael.dot(25)
