# Zusammenfassung
---

## Einfache Befehle

::: cards 2
#### [Modul importieren][1]

Um vordefinierte Python-Befehle aus einem Modul zu verwenden, muss das Modul importiert werden.

``` python
import turtle
```

***
#### [Unterprogramm aufrufen][2]

Um ein vordefiniertes Unterprogramm aufzurufen wird der Name des Unterprogramms gefolgt von Klammern hingeschrieben. In den Klammern können **Parameter** stehen.
```
stufe(50)
raphael.begin_fill()
```

:::

## Kontrollstrukturen

Mit **Kontrollstrukturen** wird festgelegt, in welcher Reihenfolge die einzelnen Befehle eines Programms nacheinander ausgeführt werden.

::: cards 2
#### [Schleife][3]

Mit einer Schleife können Befehle eine bestimmte Anzahl Mal wiederholt werden.

``` python
for i in range(5):
    raphael.forward(100)
    raphael.left(90)
```

***
#### [Unterprogramm definieren][2]

Ein Unterprogramm fasst mehrere Befehle zusammen, welche später mehrmals aufgerufen werden können. Unterschiedliche Werte für verschiedene Aufrufe des gleichen Unterprogramms können als **Parameter** übergeben werden.

``` python
def stufe(laenge):
    raphael.forward(laenge)
    raphael.left(90)
    raphael.forward(laenge)
    raphael.right(90)
```
:::

## Turtle-Befehle

~~~ python
name = turtle.Turtle()
~~~
erzeugt eine neue Turtle mit dem Namen `name`.

### Bewegung

~~~ python
.forward(n)
~~~
bewegt die Turtle um `n` Pixel nach vorne.

~~~ python
.backward(n)
~~~
bewegt die Turtle um `n` Pixel nach hinten.

~~~ python
.left(a)
~~~
dreht die Turtle um `a` Grad nach links.

~~~ python
.right(a)
~~~
dreht die Turtle um `a` Grad nach rechts.

``` python
import turtle

raphael = turtle.Turtle()
raphael.forward(100)
raphael.left(90)
raphael.forward(100)
```

### Stift

~~~ python
.penup()
~~~
hebt den Stift an. Wenn sich die Turtle nun bewegt, zeichnet sie nicht mehr.

~~~ python
.pendown()
~~~
senkt den Stift. Wenn sich die Turtle nun bewegt, zeichnet sie wieder.

~~~ python
.pencolor(farbe)
~~~
legt die Zeichenfarbe für den Stift fest. Die Farbe muss in Anführungszeichen geschrieben werden. [Hier][4] ist eine Liste von Farben.

~~~ python
.pensize(dicke)
~~~
legt die Dicke des Zeichenstifst in Pixel fest.

``` python
import turtle

raphael = turtle.Turtle()
raphael.penup()
raphael.pencolor("red")
raphael.pensize(10)
raphael.forward(100)
raphael.pendown()
raphael.forward(100)
```

### Füllen

~~~ python
.fillcolor(farbe)
~~~
legt die Farbe für das Füllen von Figuren fest. Die Farbe muss in Anführungszeichen geschrieben werden. [Hier][4] ist eine Liste von Farben.

~~~ python
.begin_fill()
~~~
beginnt damit, eine gefüllte Figur zu zeichnen.

~~~ python
.end_fill()
~~~
beendet die Figur und füllt sie aus.

``` python
import turtle

raphael = turtle.Turtle()
raphael.fillcolor("red")
raphael.begin_fill()
raphael.forward(100)
raphael.left(120)
raphael.forward(100)
raphael.left(120)
raphael.forward(100)
raphael.end_fill()
```

### Aussehen und Geschwindigkeit

~~~ python
.shape(aussehen)
~~~
ändert das Aussehen der Turtle. Für `aussehen` können folgende Werte eingesetzt werden: `"arrow"`, `"turtle"`, `"circle"`, `"square"`, `"triangle"`, `"classic"`.

~~~ python
.hideturtle()
~~~
versteckt die Turtle. Kann verwendet werden, um die Turtle nach dem Zeichnen aus ihrem Kunstwerk zu verbannen.

~~~ python
.showturtle()
~~~
zeigt die Turtle wieder an.

~~~ python
.speed(s)
~~~
legt die Geschwindigkeit der Turtle fest. Für `s` können folgende Werte eingesetzt werden: `"fastest"`, `"fast"`, `"normale"`, `"slow"`, `"slowest"`.

``` python
import turtle

raphael = turtle.Turtle()
raphael.shape("turtle")
raphael.speed("fastest")
# Hier zeichnen
raphael.hideturtle()
```

[1]: ?page=9-1-import
[2]: ?page=2-4-subroutine-turtle
[3]: ?page=2-3-loop-music

[4]: ?page=2-1-colour
