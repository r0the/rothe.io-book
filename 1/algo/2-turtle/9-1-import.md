# Modul importieren
---

## Was ist ein Modul?

Ein **Modul** ist eine Python-Programmdatei. Um Unterprogramme und andere Definitionen aus einem anderen Modul zu verwenden, muss das Modul **importiert** werden.

Module können aus folgenden Quellen stammen:

- **Eigene Module** können jederzeit durch das Erstellen einer Python-Datei angelegt werden.
- **Standardmodule** sind bei jeder Installation von Python mit dabei. Ein Beispiel ist das `turtle`-Modul.
- **Zusätzliche Module** können jederzeit installiert werden.

## Import-Befehl

Der Import-Befehl lädt ein Modul zur Verwendung im aktuellen Python-Programm.

``` python
import turtle
```

Wenn dieser Befehl ausgeführt wird, geschieht folgendes:

1. Python sucht die Datei _turtle.py_ im gleichen Ordner wie das aktuelle Python-Programm.
2. Python sucht die Datei _turtle.py_ in den installierten Modulen von Python.
3. Wenn die Datei gefunden wurde, führt Python die Datei als Python-Programm aus.
4. Wenn die Datei nicht gefunden wurde, bricht Python das Programm mit dem Fehler _ModuleNotFoundError_ ab.

::: warning Achtung
Wenn man ein eigenes Python-Programm unter den Namen _turtle.py_ speichert, findet Python das Standardmodul _turtle_ nicht mehr. Die Namen von Standardmodulen sollten also nie als Dateiname für eigene Python-Programme verwendet werden.
:::

## Standardmodule

In der folgenden Tabelle sind einige wichtige Standardmodule aufgeführt:

| Modul    | Inhalt                                        |
|:-------- |:--------------------------------------------- |
| `math`   | Mathematische Funktionen für Gleitkommazahlen |
| `random` | Erzeugen von Pseudo-Zufallszahlen             |
| `turtle` | Turtlegrafik                                  |
| `os`     | Zugriff auf Dateien und Ordner                |


::: info Import-Varianten

Es gibt viele alternative Varianten der Import-Befehls. Interessierte können diese hier nachlesen.
***
### Import eines Moduls mit Alias

Es besteht auch die Möglichkeit, den Namen der Variable zu wählen, in welcher das Modul zu Verfügung steht. Dazu wird der Anweisung `as` und der gewünschte Name angehängt:

``` python
import math as m
m.sqrt(2)
```

In diesem Fall wird das Modul als Objekt in der Variable `m` verfügbar gemacht.

### Import eines Namens aus einem Modul

Wenn nur ein Name aus einem Modul verwendet wird, bietet sich diese Anweisung an:

``` python
from math import sqrt
sqrt(2)
```

Nach dieser Anweisung steht nur die `sqrt`-Funktion aus dem `math`-Modul, zu Verfügung. Die Funktion wird direkt in der Variable `sqrt` gespeichert.

### Import eines Namens aus einem Modul mit Alias

Auch hier kann mit `as` ein Name für die Variable gewählt werden:

``` python
from math import sqrt as square_root
square_root(2)
```

### Import aller Namen aus einem Modul

Python bietet auch die Möglichkeit, sämtliche Namen aus einem Modul zu importieren und diese als Variablen zu Verfügung zu stellen. Diese Anweisung sollte man jedoch **auf keinen Fall verwenden**, da man keine Kontrolle darüber hat, welche Namen im eigenen Programm definiert werden.

``` python
from math import *
sqrt(2)
```

:::
