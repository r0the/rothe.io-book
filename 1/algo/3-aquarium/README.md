# Pygame Zero: Aquarium
---

In diesem Tutorial lernst du grundlegende Elemente der Programmiersprache Python kennen. Du lernst ausserdem, das Python-Modul «Pygame Zero» kennen, mit welchem einfach interaktive grafische Programme erstellt werden können.

Das Ziel dieses Tutorials ist es, die Simulation eines Aquariums zu programmieren.

Für dieses Tutorial musst du **Thonny** starten.
