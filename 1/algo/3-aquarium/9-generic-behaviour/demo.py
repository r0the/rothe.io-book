import pgzrun

WIDTH = 800
HEIGHT = 600
background_colour = 0, 120, 200
red_fish = Actor("fish_red_right")
red_fish.x = WIDTH / 2
red_fish.y = HEIGHT / 2

blue_fish = Actor("fish_blue_right")
blue_fish.vx = 2
blue_fish.vy = 1

green_fish = Actor("fish_green_right")
green_fish.vx = 1
green_fish.vy = 2

def bewege_automatisch(fish):
    fish.x = fish.x + fish.vx
    fish.y = fish.y + fish.vy
    if fish.right >= WIDTH:
        fish.vx = -fish.vx
    if fish.left < 0:
        fish.vx = -fish.vx
    if fish.bottom >= HEIGHT:
        fish.vy = -fish.vy
    if fish.top < 0:
        fish.vy = -fish.vy


def update():
    if keyboard.a and red_fish.left > 0:
        red_fish.x = red_fish.x - 2
    if keyboard.d and red_fish.right < WIDTH:
        red_fish.x = red_fish.x + 2
    if keyboard.w and red_fish.top > 0:
        red_fish.y = red_fish.y - 2
    if keyboard.s and red_fish.bottom < HEIGHT:
        red_fish.y = red_fish.y + 2

    bewege_automatisch(blue_fish)
    bewege_automatisch(green_fish)


def draw():
    screen.fill(background_colour)
    red_fish.draw()
    blue_fish.draw()
    green_fish.draw()
    
pgzrun.go()

