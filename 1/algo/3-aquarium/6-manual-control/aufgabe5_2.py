import pgzrun

WIDTH = 800
HEIGHT = 600
background_colour = 0, 120, 200
red_fish = Actor("fish_red_right")
red_fish.x = WIDTH / 2
red_fish.y = HEIGHT / 2


def update():
    if keyboard.a and red_fish.left > 0:
        red_fish.x = red_fish.x - 2
    if keyboard.d and red_fish.right < WIDTH:
        red_fish.x = red_fish.x + 2
    if keyboard.w and red_fish.top > 0:
        red_fish.y = red_fish.y - 2
    if keyboard.s and red_fish.bottom < HEIGHT:
        red_fish.y = red_fish.y + 2


def draw():
    screen.fill(background_colour)
    red_fish.draw()

pgzrun.go()
