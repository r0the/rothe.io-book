# Fische
---

Als nächstes wollen wir einen Fisch in das Aquarium einfügen. Um ein Objekt in Pygame Zero darzustellen, sind drei Schritte nötig:

1. Grafiken für Pygame Zero zugänglich machen
2. Objekt erstellen
3. Objekt zeichnen

## Speicherort für Grafiken

Damit Pygame Zero auf eine Grafikdatei zugreifen kann, muss sie im Unterordner **images** des Speicherorts des Programms befinden. Im folgenden gehen wird davon aus, dass das Programm im «normalen» Ordner **mu_code** gespeichert worden ist.

## Aktoren erstellen

In Pygame Zero heissen bewegliche Objekte **Aktoren**. Mit `Actor(bild)` wird ein Aktor erstellt. Der Name der Bilddate muss dabei **in Anführungszeichen** und ohne Dateiendung angegeben werden:

``` python
red_fish = Actor("fish_red_right")
```

Das erzeugte Aktor-Objekt wird in mit einer Zuweisung in einer Variable gespeichert, damit wir es später verwenden können.

## Aktoren zeichnen

Jedes Aktor-Objekt hat ein Unterprogramm `.draw()`. Dies darf nicht mit unserem eigenen Unterprogramm `draw()` verwechselt werden! Um das Unterprogramm eines Aktors aufzurufen, verwenden wir das Punkt `.` als Zugriffsoperator:

``` python
red_fish.draw()
```

Diese Anweisung, welche den Fisch in das Fenster zeichnet, muss am richtigen Ort in unserem Programm platziert werden. Dieser Ort ist im Unterpgoramm `draw()` **nach** dem Füllen des Hintergrunds.

::: exercise Aufgabe 2.1 – Ein Fisch

1. Speichere die Fisch-Grafik mit einem Rechtsklick und anschliessendem Auswählen von _Bild speichern unter_:

   ![](./images/fish_red_right.png)

   ![](../mu/images/mu-images.png) Öffne in Mu den Ordner für Bilder und kopiere die Fisch-Grafik in diesen Ordner.

2. Erweitere das Skript **aquarium.py** so, dass ein Aktor erstellt und in einer Variable gespeichert wird.
3. Erweitere das Skript, dass der Aktor in das Fenster gezeichnet wird.
***
``` python ./aufgabe2_1.py
```
:::

::: exercise Aufgabe 2.2 – Zwei Fische

Erstelle einen zweiten Aktor mit einem anderen Aussehen:

![](./images/fish_blue_right.png)
:::

::: exercise Zusatzaufgabe 2.3 – Eigene Grafik

Verwende eine eigene Grafik für einen Aktor auf der [nächsten Seite][1] findest du eine Anleitung, wie du aus einem Foto eine geeignete Grafik erstellst.

[1]: ?page=/content/ict/fotos/2-2-turtle
