# <center>Algorithmen und Programme<center>

![](images/cover.jpg)

## Zusammenfassung

**Algorithmen** sind Anleitungen, die beschreiben, wie ein Problem gelöst werden kann. Ein Algorithmus besteht aus **klar definierten Einzelschritten** und **Entscheidungen**, welche dazu führen können, dass Einzelschritte nicht oder mehrmals ausgeführt werden.
