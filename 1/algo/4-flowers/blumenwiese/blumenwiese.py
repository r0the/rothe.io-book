import pgzrun

TITLE = "Blumenwiese"
WIDTH = 1000
HEIGHT = 733

wiese = Actor("wiese")
veilchen = Actor("veilchen")

def draw():
    wiese.draw()

    veilchen.x = 400
    veilchen.y = 600
    veilchen.draw()

pgzrun.go()
