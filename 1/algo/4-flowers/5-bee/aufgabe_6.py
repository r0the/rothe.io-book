import pgzrun
import random

TITLE = "Blumenwiese"
WIDTH = 1000
HEIGHT = 733

wiese = Actor("wiese")
veilchen = Actor("veilchen")
loewenzahn = Actor("loewenzahn")
orchidee = Actor("orchidee")
biene = Actor("biene_rechts")
biene.x = WIDTH / 2
biene.y = 100
geschwindigkeit = 2


def zeichne_blume(blume, x, y):
    blume.x = x
    blume.y = y
    blume.draw()


def draw():
    wiese.draw()
    biene.draw()


def update(zeitdifferenz):
    if keyboard.right:
        biene.image = "biene_rechts"
        biene.x = biene.x + geschwindigkeit
    if keyboard.left:
        biene.image = "biene_links"
        biene.x = biene.x - geschwindigkeit
    if keyboard.up:
        biene.y = biene.y - geschwindigkeit
    if keyboard.down:
        biene.y = biene.y + geschwindigkeit

pgzrun.go()
