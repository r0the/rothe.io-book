# Tutorial «Blumen»
---

Für dieses Tutorial kannst du eine vorbereiteten ZIP-Ordner herunterladen:

::: exercise Aufgabe 1 – Vorbereitung
1. Lade den folgenden ZIP-Ordner herunter:

   * [:zip: blumenwiese.zip](./blumenwiese.zip)

2. Klicke die ZIP-Datei mit der **rechten** Maustaste an und wähle den Menüpunkt _Alle extrahieren…_:

   ![](./zip-extract-1.png)

3. Wähle mit _Durchsuchen_ den gewünschten Ordner und klicke auf _Extrahieren_:

   ![](./zip-extract-2.png)

4. Starte den Mu-Editor.

5. Klicke auf _Load_ und öffne die Datei _blumenwiese.py_.
:::
