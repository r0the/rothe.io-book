# Pair-Programming[^1]
---

## Worum geht es?

Programmieren ist eine Tätigkeit die vor allem im Kopf passiert und auch anstrengend und fehleranfällig sein kann. Schnell hat man etwas nicht bedacht oder verliert viel Zeit bei einem kleinen unwichtigen Detail.

Pair-Programming ist eine Arbeitstechnik, bei der zwei ProgrammiererInnen zusammenarbeiten, um gemeinsam bessere Ergebnisse zu liefern als alleine. Eine Person erstellt das Programm und die andere behält das Gesamtprojekt im Blick. Gemeinsam spricht man fortlaufend darüber das was man tut und denkt.

## Wie funktioniert es?

### Rollen beim Pair-Programming

Jeder übernimmt eine Rolle – entweder bist du der **Driver** oder der **Navigator**. Das ist etwa so wie beim Rallye fahren, wo jemand fährt und der andere die Karte liest und ansagt, welche Kurve als nächstes kommt. Der Driver entwickelt das Programm und bedient den Computer. Der Navigator überlegt was als nächstes zu tun ist, denkt über Problemstellungen nach, kontrolliert das entstehende Programm und macht Vorschläge. Die beiden Rollen solltet ihr regelmässig tauschen.

### Regeln beim Pair-Programming

Damit Pair-Programming funktioniert müssen einige wichtige Regeln beachtet werden.

1. **Miteinander sprechen:** Sprecht über alles was ihr gerade tut und denkt. Stille ist ein schlechtes Zeichen.
2. **Rollen einhalten:** Auch wenn du gerade besser weisst, wie etwas geht – nicht einfach ins Keyboard/Maus greifen.
3. **Bleibt gelassen:** Auch wenn es gerade mal nicht so gut klappt – streitet euch nicht! Kein Beleidigen und Fluchen.
4. **Driver lenkt:** Erkläre immer was du gerade tust. Begründe warum du etwas tust und mit welchem Ziel.
5. **Beim Thema bleiben:** Sprecht über das Projekt und schweift nicht ab. Macht keine anderen Dinge nebenbei!
6. **Navigator denkt:** Kommandiere den Driver nicht herum und sag ihm nicht ständig was er genau zu tun hat.

## Hinweis

Pair Programming ist nicht etwa eine Erfindung von Pädagoginnen und Pädagogen oder gar nur ein anderer Begriff für klassiche Partnerarbeit! Diese Arbeitstechnik wird tatsächlich in der Software-Entwicklung eingesetzt, um weniger Fehler zu machen und besseren Code zu schreiben.

## Ein Projekt mit Pair-Programming

Überlegt euch zusammen ein kreatives Programmier-Projekt, welches ihr gerne umsetzen möchtet. Entscheidet, wer mit welcher Rolle startet und wechselt euch im Laufe der Entwicklung immer wieder ab.

Trefft alle Entscheidungen gemeinsam – welche Farbe soll eine Figur haben, welcher Text soll eingeblendet werden ...

[^1]: Quelle: [iLearnIT.ch](http://ilearnit.ch/de/broschueren.html)
