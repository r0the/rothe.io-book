import turtle

raphael = turtle.Turtle()
raphael.shape("turtle")


def unterprogramm(farbe):
    raphael.fillcolor(farbe)
    raphael.begin_fill()
    for i in range(3):
        raphael.forward(100)
        raphael.left(120)
    raphael.end_fill()


unterprogramm("red")
raphael.forward(100)
unterprogramm("green")
raphael.forward(100)
unterprogramm("blue")
