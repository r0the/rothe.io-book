import turtle

def baum(a):
    turtle.left(90)
    turtle.forward(a)
    turtle.dot(a)
    turtle.back(a)
    turtle.right(90)

baum(200)
