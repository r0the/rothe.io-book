# Postenarbeit Algorithmus
---

::: goal Lernziel
Die Schüler*innen setzen sich mit verschiedenen Aspekten von Algorimen auseinander:
- Definition
- Elemente: Einzelschritt, Repetition, Entscheidung
:::

## Posten Origami

Die Schüler*innen setzen sich mit der Eindeutigkeit von Anweisungen auseinander.

* [:pdf: Posten Origami (PDF)](./posten-origami.pdf)
* [:odt: Posten Origami (ODT)](./posten-origami.odt)

## Posten Musik

Die Schüler*innen setzen sich mit Wiederholung und Zusammenfassung von Anweisungen auseinander.

* [:pdf: Posten Musik Variante Mario Brothers (PDF)](./posten-musik-mario.pdf)
* [:odt: Posten Musik Variante Mario Brothers (ODT)](./posten-musik-mario.odt)
* [:pdf: Posten Musik Variante Jingle Bells (PDF)](./posten-musik-jingle.pdf)
* [:odt: Posten Musik Variante Jingle Bells (ODT)](./posten-musik-jingle.odt)

## Posten Flussdiagramm

Die Schüler*innen lernen Flussdiagramme kennen.

* [:pdf: Posten Flussdiagramm Variante Wochentag (PDF)](./posten-wochentag.pdf)
* [:odt: Posten Flussdiagramm Variante Wochentag (ODT)](./posten-wochentag.odt)
* [:pdf: Posten Flussdiagramm Variante Sortieren (PDF)](./posten-sortieren.pdf)
* [:odt: Posten Flussdiagramm Variante Sortieren (ODT)](./posten-sortieren.odt)

## Posten Roboter

Die Schüler*innen lernen Turtle-Anweisungen kennen.

* [:pdf: Posten Roboter (PDF)](./posten-roboter.pdf)
* [:odt: Posten Roboter (ODT)](./posten-roboter.odt)
