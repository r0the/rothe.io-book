# Material für Lehrer*innen
---

* [Postenarbeit Algorithmus](?page=algorithm/)

## Möglicher Ablauf

### 1. Doppellektion – Posten Algorithmus

* [:pdf: Präsentation Einführung (PDF)](1-algorithmen.pdf)
* [:pptx: Präsentation Einführung (PPTX)](1-algorithmen.pptx)

Ablauf
- Einführung mit Präsentation
- Durchführung [Postenarbeit Algorithmus](?page=algorithm/)


### Weitere Ressourcen

* [:pdf: Arbeitsblatt Flussdiagramm (PDF)](arbeitsblatt-flussdiagramm.pdf)
* [:odt: Arbeitsblatt Flussdiagramm (ODT)](arbeitsblatt-flussdiagramm.odt)


* [:pdf: Arbeitsblatt Python-Syntax (PDF)](arbeitsblatt-syntax.pdf)
* [:odt: Arbeitsblatt Python-Syntax (ODT)](arbeitsblatt-syntax.odt)

## Begriffe zuordnen

Repetitions-Sequenz: Alle Schüler*innen erhalten zufällig je eine Karte. Immer drei Karten passen zusammen: ein Begriff, eine Definition und Code-Beispiel. Kann auch verwendet werden, um Gruppen zu bilden.

* [:pdf: Karten Begriffe (PDF)](karten-begriffe-1.pdf)
* [:odt: Karten Begriffe (ODT)](karten-begriffe-1.odt)
