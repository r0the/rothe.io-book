# Planspiel
---

* [:pdf: Planspiel](./planspiel-v3.pdf)

#### Spieldauer
2-3 Stunden
▪
#### Spieler*innen

- 12 bis ca. 50 Personen im Alter von 14+ Jahren, die deutschsprachige Texte lesen und verstehen können und die vorher schon einen Einstieg ins Thema «Big Data» gemacht haben
- darunter eine ein- bis zweiköpfige Spielleitung
- und eine zwei- bis dreiköpfige Jury.

#### Benötigte Räume

- je Team – das Spiel braucht 3-7 Teams mit je 3-7 Personen – ein eigener Raum
- an zentraler Stelle (kann ein verbindender Korridor sein) ein Tisch für die Spielleitung
- ein grosser Raum für die Einführung und die Abschlusspräsentation mit Sitzplätzen für die gesamte Gruppe, einem Präsentationsbereich und einem Jury-Tisch; darin sollte es eine Pinnwand, ein Whiteboard oder eine Flipchart für die Präsentationen geben
- ggf. einen weiteren grossen Raum für die Preisverleihung; kann in Party übergehen

#### Benötigte Materialien

- Stifte und Papier für jedes Team
- je Team ein Satz Datenkarten (vorher ausdrucken und ausschneiden!)
- je Team ein Satz Aufgabenblätter (vorher vorbereiten und ausdrucken!)
- ggf. Post-its zur Verteilung der Teams (siehe unten)
- für jedes Jurymitglied 1 ausgedruckter Bewertungsbogen
- (symbolische) Preise: Preis für die Gewinner*innen (je nach Anzahl der Teams ggf. auch 2. und 3. Preise), Trostpreise für die
übrigen Teams
- Snacks und Softdrinks für den Ausklang
- Kostümkiste (bei Spielleitung), mit alten Anzügen und Krawatten, Perücken, Sonnenbrillen etc.

#### Zusätzliche Materialien (optional)
- wenn genügend Rechner vorhanden sind, können die Gruppen auch mit Power-Point- oder ähnlichen Präsentationen arbeiten; dafür sollte im Präsentationsraum ein Beamer bereitstehen
- falls Kameras vorhanden sind, kann mit Fotos oder Kurzvideos gearbeitet werden; auch dafür sollte im Präsentationsraum ein Beamer bereitstehen, für Videos zusätzlich Lautsprecher

Die Gruppe sollte auf die Rechner/Kameras hingewiesen werden, z.B. auf dem Aufgabenblatt zu Aufgabe 4 (siehe unten). Für diese Aufgabe sollte dann auch etwas mehr Zeit eingeplant werden.

#### Autor*innen

Ein Spiel des jfc Medienzentrums e.V. (www.jfc.info), entwickelt von Sascha Düx (sascha@jfc.info) mit Ideen und Feedback von Henrike Boy, Dennis Brauner und Judith Mayer.

## Die 4 Spielphasen

### I. Spielbeginn (ca. 15 Minuten):
Die Spielleitung erklärt der Gruppe die Ausgangssituation und die Spielregeln. Die Teams werden aufgeteilt.

## II. Gruppenphase (ca. 75 Minuten):
Die Teams arbeiten in ihren jeweiligen Räumen. Die Spielleitung bringt zu bestimmten Zeiten Aufgabenblätter.

### III. Präsentationsphase (je nach Anzahl der Teams ca. 30-60 Minuten):

Die Teams präsentieren ihre Ideen; die Jury bewertet und berät sich kurz.

### IV. Preisverleihung (je nach Anzahl der Teams ca. 15-30 Minuten mit offenem Ende):

Jury bewertet die Teams und vergibt die Preise; anschliessend lockeres Beisammensein mit Snacks/Essen und Möglichkeit, mit der Jury und den anderen Teams zu diskutieren.

## Vorbereitung
1. Vor dem Spiel sollten sich die Teilnehmer_innen bereits mit dem Thema «Big Data» beschäftigt haben. Dieses Spiel ist keine Einstiegsmethode! Siehe dazu auch die Broschüre «Big Data – eine Arbeitshilfe für die Jugendarbeit» des jfc Medienzentrum, www.jfc.info

2. Die Spielleitung plant das Spiel: Wie viele Teilnehmer_innen sind zu erwarten, wie viele Teams soll es geben? Sollen die Teams sich finden, werden sie «gesetzt», oder per Zufall verteilt? Welche Räume werden genutzt? etc.

3. Besonders sorgfältig muss die Jury ausgewählt und vorbereitet werden. Die Jury-Mitglieder sollten sich schon mit Big Data und/oder Datenschutz beschäftigt haben; ideal ist eine Kombination von 2-3 Menschen mit unterschiedlichen, zur Sache passenden Kompetenzen; z.B. ein Elternteil, das in einer Online-Firma arbeitet, und ein/e sehr datenschutzbewusster Kolleg/in. Die Jury muss das Spielprinzip und die Bewertungskriterien verstehen; sie sollte sich vorab mit den Datenpool-Karten und dem beiliegenden Auszug aus der Erklärung der Menschenrechte beschäftigen.

4. Die Spielleitung stellt die Materialien zusammen; inkl. Ausdrucken und Ausschneiden der Datenpool-Karten sowie Vorbereiten und Ausdrucken der Aufgabenblätter. Jede Karte und jedes Blatt muss pro Team einmal vorhanden sein!

5. Die Spielleitung bereitet die Räume vor, deponiert ggf. schon Stifte und Blätter sowie Datenkarten und das erste Aufgabenblatt in den Räumen der Teams, bereitet den Präsentationsraum vor, klebt ggf. Zettel unter die Stühle, auf denen die Gruppe in Spielphase I sitzt (siehe unten).
