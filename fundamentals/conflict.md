# Konflikt
---

Ein Konflikt liegt vor, wenn Interessen, Ziele oder Werte verschiedener Beteiligter miteinander unvereinbar sind oder erscheinen.

## Anwendungen

#### Alltag
- Schulstunde: Kommunikationsbedürfnis vs. Ruhebedürfnis
- Geschwindigkeit vs. Qualität

#### Informatik
- YouTube und Urheberrecht: gratis Musik hören vs. Geld verdienen
- Überwachung: Privatsphäre vs. Sicherheit
- mehrere Personen arbeiten am gleichen Dokument
- mehrere Programme benutzen den gleichen Prozessor
