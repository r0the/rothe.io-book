# Protokollierung
---

Protokollierung ist das chronologische Aufzeichnen von Vorgängen. Jeder Protokolleintrag besteht mindestens aus einem Zeitpunkt, den beteiligten Personen und dem Vorgang.

Die Protokollierung ist nicht zu verwechseln mit dem Begriff «[:fundamental: Protokoll](?page=protocol)», mit welchem hier die Regelung eines künftigen Ablaufs gemeint ist.

Das Protokollieren von Aktivitäten durch ein Computerprogramm wird als *Logging* bezeichnet. Die so erzeugten *Logs* können unterschiedlich genutzt werden:

- Zur Suche von Fehlern in der Programmierung der Software (*Debugging*).
- Zum Nachvollziehen von Zugriffen auf geschützte Informationen (*Auditing*).
- Zum Sammeln von Daten über die Benutzer des Programms (*Tracking*).

## Anwendungen

#### Alltag
- Fahrtschreiber oder Flugschreiber («Black Box») in Fahrzeugen bzw. Flugzeugen
- Eintritte zu kritischen Bereichen, z.B. Museen, Militäranlagen
- Berechnung von Fahrkosten per Smartphone-App im öffentlichen Verkehr

#### Informatik
- Zugriffslog einer Webseite
- Auditlog (Anmeldungen) eines Betriebssystems
- Log eines WLAN-Zugriffspunkts
- Browser-History
