# Typen
---

Daten wird ein Typ zugeordnet, um festzulegen, welche Operationen mit den Daten möglich sind und wie sich die Daten verhalten.

## Anwendungen
#### Informatik

- [Typen in der Tabellenkalkulation](?context=/content/ict/index&page=/content/ict/data/1-1-data-types)
- Typen in Programmiersprachen
- Dateitypen
