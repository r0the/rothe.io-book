# Protokoll
---

Ein Protokoll ist die Regelung eines zukünftigen Ablaufs (Voraus-Protokoll). Dies ist nicht zu verwechseln mit der zweiten Bedeutung des Begriffs, dem Resultat einer [:fundamental: Protokollierung](?page=logging).

Diplomatische Protokolle bei Staatsbesuchen und Hofprotokolle von Königshäusern legen beispielsweise Rangfolgen, Abläufe, Kleidervorschriften, Sitzordnungen und Verhalten fest.[^1]

## Anwendungen

#### Alltag
- Kleidervorschriften in Unternehmen und an Schulen
- Knigge
- Stundenplan

#### Informatik
- Netzwerkprotokolle (z.B. IP, DNS, HTTP)
- Geräteprotokolle (z.B. USB, VGA, HDMI, SATA)

[^1]: Quelle: [Wikipedia](https://de.wikipedia.org/wiki/Protokoll)
