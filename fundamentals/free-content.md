# Freie Inhalte
---

Bei Freien Inhalten ist die kostenlose Nutzung und Weiterverbreitung urheberrechtlich erlaubt.[^1] Dieser Zustand entsteht durch den Ablauf von gesetzlichen Schutzfristen oder indem ein Werk unter eine freie Lizenz gestellt wird.

Freie Inhalte werden in folgenden Gebieten genutzt:

- Medien, z. B. Texte, Bilder, Musik, Filme, Animationen und Modelle
- Software, siehe Open Source
- Technologie. Mittels Open Hardware und Open Source werden Privatpersonen in die Lage versetzt, selbst Produkte herzustellen oder zu erweitern.
- Datenbanken, siehe Open Data
- Wissenschaft und Lehre, siehe Open Access
- Politik, siehe Open Government
- Lern- und Lehrmaterialien, Open Educational Resources
- Kunst

## Anwendungen

- [Wikipedia][1] ist eine freie Enzyklopädie
- Das freie Betriebssystem Linux bildet die Basis für alle Android-Systeme.
- Die meisten von den Regierungsbehörden der USA erstellten Inhalte sind frei.[^2]

[1]: https://de.wikipedia.org

[^1]: Quelle: [Wikipedia](https://de.wikipedia.org/wiki/Freie_Inhalte)
[^2]: Quelle: [usa.gov](https://www.usa.gov/government-works)
