# Quantisierung
---

## Anwendungen

#### Informatik
- Digitalisieren eines Audiosignals
- Aufnehmen einer Fotografie oder eines Videos
- Rastern einer Vektorgrafik
- Digitalisieren eines Sensorwerts
