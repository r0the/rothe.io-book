# Public-Key
---

Ein Public-Key- oder asymmetrisches Verschlüsselungsverfahren ist ein Verfahren aus der Kryptografie, bei welchem ein Paar aus öffentlichem und privatem Schlüssel verwendet wird, um Daten zu verschlüsseln oder zu signieren.

Dieser Ansatz löst das grosse Problem der Schlüsselverteilung von symmetrischen Verschlüsselungsverfahren.
