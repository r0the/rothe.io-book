# Adresse
---

Eine Adresse ist eine Zielangabe. Sie dient dazu, ein Ziel innerhalb eines Kontexts eindeutig zu beschreiben.

::: box info
Gymnasium Kirchenfeld<br>
Fachschaft Informatik<br>
Kirchenfeldstrasse 25<br>
3005 Bern<br>
Schweiz
:::

Bei der Verwendung einer Adresse muss das Adressierungsschema bekannt sein. Bei einer Wohnadresse muss vorgängig festgelegt worden sein, wo Bern und die Kirchenfeldstrasse sind.

Adressschemen können hierarchisch aufgebaut sein. Bei einer Wohnadresse ist innerhalb einer Strasse die Strassennummer eindeutig. Innerhalb einer Ortschaft ist in der Regel der Strassenname eindeutig, innerhalb eines Landes der Name der Ortschaft und so weiter. So wird innerhalb einer Ortschaft deren Name in der Ortsangabe weggelassen. Somit bilden Wohnadressen einen [:fundamental: Namensraum][1].


## Anwendungen

#### Alltag
- Wohnadresse
- Postanschrift
- Telefonnummer


#### Informatik
- [URLs](?context=/content/ict/index&page=/content/ict/search/1-5-url)
- IP-Adresse
- Datenbankschlüssel
- Speicheradresse

[1]: ?page=namespace
