# Schichten
---

Eine Sache kann auf verschiedenen Schichten oder Ebene betrachtet werden. Auf einer höheren Schichten werden Details der darunterliegenden Ebene ausgeblendet. Dadurch kann die untersuchte Sache sowohl im Detail als auch ganzheitlich verstanden werden.

Die [Skalierbarkeit](?page=scalability) von Informatiksystemen wird durch die Unterteilung in Schichten erhöht.

| Abstraktionsebene | Begriffe             |
|:----------------- |:-------------------- |
| Anatomie          | Organe, Gewebe       |
| Molekularbiologie | DNA, Proteine        |
| Chemie            | Moleküle, Reaktionen |
| Atomphysik        | Atome, Elektronen    |

## Anwendungen

#### Informatik
- In der Kommunikation wird ein Modell verschiedener Netzwerkschichten verwendet.
- Die Funktionsweise von Computern kann auf verschiedenen Ebenen betrachtet werden.
- Die Repräsentation von Daten ist häufig mehrschichtig.
- Komplexe Software wird üblicherweise in mehrere Schichten aufgeteilt.
