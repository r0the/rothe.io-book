# Code
---

Ein Code ist eine Vorschrift, welche Daten von einer Darstellung in eine andere umwandelt. Dabei darf keine Information verloren gehen, eine Rückumwandlung muss möglich sein.

::: columns
![QR-Code](/content/data/images/qr-code.svg "QR-Code")
***
![Brailleschrift](images/braille.jpg "Blindenschrift ist ein Code")
:::

Codes werden verwendet, um Daten in eine [:fundamental: binäre Darstellung](?page=binary) umzuwandeln.

Für die Kommunikation werden oft Codes mit [:fundamental: Redundanz](?page=redundancy) eingesetzt, damit Übertragungsfehler erkannt und korrigiert werden können.

## Anwendungen

#### Alltag

- Morsezeichen
- Brailleschrift
- [:link: genetischer Code][3]
- [:link: IATA-Flughafencode][4]
- [:link: Internationale Standardbuchnummer][5]
- [:link: Klassifikation von Krankheiten nach ICD-11][6]
- [:link: Internationale Ländercodes][7]
- [:link: QR-Code][8]

[3]: https://de.wikipedia.org/wiki/Genetischer_Code
[4]: https://de.wikipedia.org/wiki/IATA-Flughafencode
[5]: https://de.wikipedia.org/wiki/Internationale_Standardbuchnummer
[6]: https://de.wikipedia.org/wiki/Internationale_statistische_Klassifikation_der_Krankheiten_und_verwandter_Gesundheitsprobleme
[7]: https://de.wikipedia.org/wiki/ISO-3166-1-Kodierliste
[8]: https://de.wikipedia.org/wiki/QR-Code

#### Informatik

- Darstellung von Datum und Zeit als Zahl in der Tabellenkalkulation
- [Umwandung von Signalen in Eingaben durch ein Tastaturlayout](?book=book.writing.index&page=.book.writing.1-1-keyboard-layout)
- **Unicode** für die binäre Darstellung von Zeichen und Symbolen
- **Programmcode** für die Darstellung von Computerbefehlen
- **Verschlüsselung** für die sichere Übertragung von Informationen
