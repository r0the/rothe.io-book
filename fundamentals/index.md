# :fundamental: Fundamentale Ideen
---

Mein Informatikunterricht orientiert sich an Fundamentalen Ideen.

> «Eine Fundamentale Idee ist ein Sachverhalt, der
>
> - in verschiedenen Bereichen vielfältig anwendbar oder erkennbar ist (Horizontalkriterium),
> - auf jedem intellektuellen Niveau aufgezeigt und vermittelt werden kann (Vertikalkriterium),
> - in der historischen Entwicklung deutlich wahrnehmbar ist und längerfristig relevant bleibt (Zeitkriterium),
> - einen Bezug zur Sprache und zum Denken des Alltags und der Lebenswelt besitzt (Sinnkriterium) und
> - sich auf verschiedenen kognitiven Repräsentationsstufen (enaktiv, ikonisch, symbolisch) darstellen lässt (Repräsentationskriterium).»
>
> *Informatikunterricht planen und durchführen, Hartmann et al., Springer, 2006*

Folgend ist eine wachsende Liste Fundamentaler Ideen:

- [Abstraktion](?page=abstraction)
- [Adresse](?page=address)
- [Algorithmus](?page=algorithm)
- [Binäre Darstellung](?page=binary)
- [Boolsche Algebra](?page=boolean-algebra)
- [Code](?page=code)
- [Freie Inhalte](?page=free-content)
- [Konflikt](?page=conflict)
- [Namensraum](?page=namespace)
- [Protokoll](?page=protocol)
- [Protokollierung](?page=logging)
- [Public-Key](?page=public-key)
- [Quantisierung](?page=quantization)
- [Redundanz](?page=redundancy)
- [Schichten](?page=layers)
- [Schnittstelle](?page=interface)
- [Simulation](?page=simulation)
- [Skalierbarkeit](?page=scalability)
- [Stellvertreter](?page=proxy)
- [Typen](?page=type)
- [Visualisierung](?page=visualization)
- [Wiederholung](?page=repetition)
- [Zwischenspeicher](?page=buffer)
