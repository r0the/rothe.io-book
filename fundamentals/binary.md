# Binäre Darstellung
---

Alle Daten lassen sich als Zeichenfolge mit zwei Grundsymbolen schreiben. Als Grundsymbole werden üblicherweise die Ziffern Null und Eins gewählt.

::: columns
![Null und Eins](images/binary.jpg "Grundsymbole Null und Eins")
***
![Lochkarten in einem Jacquard-Webstuhl](images/jacquard-loom-punchcards.jpg "Lochkarten in einem Jacquard-Webstuhl")
:::

Die erste binäre Darstellung stammt von Joseph-Marie Jacquard, welcher Lochkarten verwendet hat, um Muster für mechanische Webstühle zu programmieren.

Zwei Zustände lassen sich physikalisch einfach durch das Vorhandensein oder die Abwesenheit einer physikalischen Eigenschaft darstellen. Deshalb werden Daten in der modernen Digitaltechnik binär gespeichert und übermittelt.

Die Datenmenge, welche mit einer einzigen Null oder Eins dargestellt werden kann, heisst *Bit*. Mit jedem zusätzlichen Bit verdoppelt sich die mögliche Anzahl verschiedenen Daten, die dargestellt werden können. Ein *Byte* ist die Datenmenge, welche mit acht Bits dargestellt werden kann.

Daten werden durch einen [:fundamental: Code](?page=code) in eine binäre Darstellung umgewandelt.

Die Funktionsweise binärer Computer wird mit der [:fundamental: Booleschen Algebra](?page=boolean-algebra), einer mathematischen Struktur, beschrieben. Diese mathematischen Beschreibung ist eine notwendige Grundlage für die Entwicklung von Mikrochips.
