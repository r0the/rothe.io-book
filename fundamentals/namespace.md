# Namensraum
---

## Anwendungen

#### Alltag
- internationale Vorwahl bei Telefonnummern
- Postadressen
- Familiennamen

#### Informatik
- Dateipfad (Ordner und Dateiname)
- Domain Name System (DNS)
- IP-Adressen
- Lokale und Globale Variablen
- Pakete in Python
