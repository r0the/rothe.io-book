# Informatik
---

> [Informatik ist die] Wissenschaft von der systematischen Darstellung, Speicherung, Verarbeitung und Übertragung von Informationen, besonders der automatischen Verarbeitung mithilfe von Digitalrechnern.[^1]

- Theoretische Informatik
- Computersysteme
- Angewandte Informatik
- Softwareentwicklung

[^1]: Duden Informatik A – Z: Fachlexikon für Studium, Ausbildung und Beruf, 4. Aufl., Mannheim 2006
