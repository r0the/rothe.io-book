# Visualisierung
---

Visualisierung heisst abstrakte Daten und Zusammenhänge in eine grafische und somit visuell erfassbare Form zu bringen. Dadurch können Aspekte sichtbar gemacht werden, welche beim Betrachten der Ausgangsdaten nicht unmittelbar deutlich sind. Dabei werden vernachlässigbare Details des Ausgangsdaten weggelassen.[^1]

Beim Visualisieren müssen oft grosse Datenmengen schnell verarbeitet und in eine andere Form gebracht werden. Deshalb sind Computer für diese Aufgabe sehr geeignet.

## Anwendungen
#### Alltag

- Abstrakte Daten werden mit Hilfe von Tabellenkalkulationsprogrammen als Diagramme dargestellt.

#### Informatik

- Algorithmen werden als Programmablaufpläne («Flussdiagramme») visualisiert.
- Geometrische Beschreibungen (Vektorgrafiken, 3D-Modelle) werden zur Darstellung auf einem Bildschirm gerendert.
- Fraktale Mengen wie die [Mandelbrot-Menge][1] konnten 1978 mit Hilfe von Computern erstmals visualisiert werden.

[1]: https://de.wikipedia.org/wiki/Mandelbrot-Menge

[^1]: Quelle: [Wikipedia](https://de.wikipedia.org/wiki/Visualisierung)
