# Skalierbarkeit
---

Skalierbarkeit ist die Fähigkeit eines Systems, sich erfolgreich an veränderte Bedingungen anzupassen. Dabei wird davon ausgegangen, dass sich die Bedingungen um Grössenordnungen ändern.

Das Mooresche Gesetz besagt, dass sich die Leistungsfähigkeit von Mikrochips in regelmässigen Abständen verdoppelt.

Die Skalierbarkeit von Informatiksystemen wird durch die Unterteilung in [:fundamental: Schichten](?page=layers) erhöht.

## Anwendungen

#### Alltag

- Supermarkt, der am Black Friday die 100-fache Menge an Kunden bedienen muss
- Erde, die eine Population von 10 Milliarden statt 100 Millionen Menschen nachhaltig versorgen muss
- Skalierbarkeit eines Geschäftsmodells in der Betriebswirtschaftslehre

#### Informatik

- Webseite, die 1 Million statt 10 Anfragen pro Minute bearbeiten muss
- Datenbank, welche 100 Milliarden statt 1 Million Datensätze speichern muss
- Datenverbindung, welche 10 GB statt 10 kB Daten pro Sekunde übertragen muss
