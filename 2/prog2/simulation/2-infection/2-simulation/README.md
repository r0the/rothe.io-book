# Simulation
---


## Bewegungsmodell

Die **Personen** werden durch farbige Kreise dargestellt. Im Gegensatz zu den Schneeflocken bewegen sich die Personen in alle Richtungen. Dazu muss für jede Person die horizontale und vertikale Geschwindigkeit gespeichert werden:

``` python
x = [] # x-Koordinate
y = [] # y-Koordinate
vx = [] # horizontale Geschwindigkeit
vy = [] # vertikale Geschwindigkeit
```

Die Bewegung geschieht, indem jeweils die Geschwindigkeit zur Position addiert wird:

``` python
x[i] = x[i] + vx[i]
y[i] = y[i] + vy[i]
```

::: info Physikalische Erklärung
Wir gehen von der Newtonschen Bewegungsgleichung aus, die besagt, dass die Geschwindigkeit $v_x$ der zurückgelegte Weg $\Delta x$ pro Zeiteinheit $t$ ist:

$$ v_x = \frac{\Delta x}{t} $$

Wir nehmen an, dass pro Simulationsschritt eine Zeiteinheit verstreicht, also das $t = 1$. Somit ist der zurückgelegte Weg $\Delta x$ gleich der Geschwindigkeit $v_x$

$$ v_x = \Delta x$$

Um die neue Position $x'$ zu berechnen, addieren wir den zurückgelegen Weg während des Simulationsschritts zur aktuellen Position:

$$ x' = x + \Delta x = x + v_x $$

Das gilt analog natürlich auch in vertikaler Richtung.
:::

## Abprallen am Rand

Um das Abprallen am Rand zu realisieren, müssen wir überprüfen, ob eine Person über einen Rand hinausläuft. Dies geschieht mit den folgenden Bedingungen:

``` python
if x[i] < 0:
    # ...
if x[i] > WIDTH:
    # ...
if y[i] < 0:
    # ...
if y[i] > HEIGHT:
    # ...
```

Ein Abprallen am linken oder rechten Rand bedeutet, dass die horizontale Geschwindigkeit der Person umgekehrt wird:

``` python
vx[i] = -vx[i]
```

Und ein Abprallen am oberen oder unteren Rand bedeutet, dass die vertikale Geschwindigkeit der Person umgekehrt wird:

``` python
vy[i] = -vy[i]
```

## Zeitmessung

Die vertrichene Zeit in Tagen wird in der Variable `tag` gespeichert. In jedem Simulationsschritt wird der Wert der Wert `sim_geschwindigkeit` zu `tag` addiert.

**Achtung:** Da `tag` eine globale Variable ist, welche im Unterprogramm `update()`
einen neuen Wert erhält, muss sie als `global` definiert werden.

**Tipp**: Die Animation des Schneefalls mit den Listen zur Speicherung der Positions- und Grössenangaben kann als Vorlage für die Simulation dienen.

## Kontakterkennung

Ein Kontakt findet statt, wenn zwei Personen einen kleineren Abstand als `radius_ansteckung` haben.

``` python
def überprüfe_kontakt(i, k):
    a_quadrat = (x[i] - x[k]) ** 2 + (y[i] - y[k]) ** 2
    return a_quadrat < radius_kontakt * radius_kontakt
```

Wir vergleichen das Quadrat der Radien, da das Wurzelziehen eine langsame Operation auf Computern ist. Wenn wir pro Simulationsschritt pro Kontaktüberprüfung zwei Wurzeln berechnen müssen, kann das die Simulation verlangsamen.

::: info Mathematische Erklärung
Zwei Personen haben Kontakt, wenn der Abstand kleiner als der Kontaktradius $r$ ist:

$$ a < r $$

Diese Ungleichung können wir quadrieren und erhalten:

$$ a^2 < r^2 $$

Nach dem Satz des Pythagoras ist das Quadrat des Abstandes $a^2$ der zwei Personen $i$ und $k$:

$$ a^2 = (x_k - x_i)^2 + (y_k - y_i)^2$$



:::

::: exercise Aufgabe – Grundgerüst

Kopieren Sie das folgende Grundgerüst der Simulation nach Thonny und testen sie es:

``` python solutions/simulation-1.py
```
:::


## Infektionsmodell

::: exercise Aufgabe – Vorbereitung der Umsetzung des Modells
Überlegen Sie sich zu zweit folgendes:

- Wie könnte man die Zustände der einzelnen Personen speichern und grafisch darstellen?
- Wie weiss das Programm, welche Person bereits wie lange infiziert bzw. krank ist?
- Wie beginnt die Epidemie?

Besprechen Sie Ihre Überlegungen mit dem Lehrer, bevor Sie mit dem Programmieren beginnen.
:::

::: exercise Aufgabe – Umsetzung des Modells
Programmieren Sie die Simulation.
:::

::: extra Aufgabe - Erweiterungen
Überlegen Sie sich, wie folgende Erweiterungen umgesetzt werden können:

- **Quarantäne:** Infizierte Personen werden mit einer bestimmten Wahrscheinlichkeit erkannt und in Quarantäne geschickt.
- **Ansteckungswahrscheinlichkeit:** Bei einem Kontakt gibt es eine Wahrscheinlichkeit der Ansteckung.
- **Mutation:** Nach einer bestimmten Anzahl Tage kann das Virus mutieren. Die Mutation hat eine grössere Ansteckungswahrscheinlichkeit.
:::

``` python solutions/simulation-2.py
```
