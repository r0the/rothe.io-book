import pgzrun
import random

TITLE = "Infektionssimulation"
WIDTH = 1200
HEIGHT = 600

anzahl = 100
radius_person = 4
radius_kontakt = 4

farbe_hintergrund = (180, 180, 180)
farbe_text = (255, 255, 0)
farbe_person = (0, 0, 0)

tag = 0
sim_geschwindigkeit = 0.05

x = []
y = []
vx = []
vy = []

# Initialisiere Personen
for i in range(0, anzahl):
    x.append(random.randrange(0, WIDTH))
    y.append(random.randrange(0, HEIGHT))
    vx.append(random.randrange(-2, 2))
    vy.append(random.randrange(-2, 2))


# Zeichnet eine Person (Kreis)
def zeichne_person(x, y, farbe):
    screen.draw.filled_circle((x, y), radius_person, farbe)


# Gibt einen Text auf den Bildschirm aus
def zeichne_text(x, y, text):
    screen.draw.text(text, left=x, top=y, color=farbe_text)


# Überprüft, ob die Personen an der Listenposition i und j Kontakt haben
def überprüfe_kontakt(i, k):
    a_quadrat = (x[i] - x[k]) ** 2 + (y[i] - y[k]) ** 2
    return a_quadrat < radius_kontakt * radius_kontakt


def kontakt(i, k):
    print("Personen " + str(i) + " und " + str(k) + " haben Kontakt.")


def draw():
    screen.fill(farbe_hintergrund)
    for i in range(0, anzahl):
        zeichne_person(x[i], y[i], farbe_person)
    zeichne_text(10, 10, "Tag: " + str(int(tag)))


def update():
    global tag
    tag = tag + sim_geschwindigkeit
    for i in range(0, anzahl):
        # Bewege Person
        x[i] = x[i] + vx[i]
        y[i] = y[i] + vy[i]
        # Pralle am linken Rand ab
        if x[i] < 0:
            vx[i] = -vx[i]
        # Pralle am rechen Rand ab
        if x[i] > WIDTH:
            vx[i] = -vx[i]
        # Pralle am oberen Rand ab
        if y[i] < 0:
            vy[i] = -vy[i]
        # Pralle am unteren Rand ab
        if y[i] > HEIGHT:
            vy[i] = -vy[i]
        # Überprüfe, ob zwei Personen Kontakt haben
        for k in range(i + 1, anzahl):
            if überprüfe_kontakt(i, k):
                kontakt(i, k)


pgzrun.go()
