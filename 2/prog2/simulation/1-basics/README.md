# Grundlagen[^1]
---
Die **Simulation** hat sich neben der **theoretischen** und der **experimentellen Forschung** als drittes Standbein des wissenschaftlichen Erkenntnisgewinns etabliert. Die Informatik fungiert daher in einer Vielzahl von Fachgebieten als Hilfswissenschaft.

## Die Wissenschaftliche Methode

Schon immer haben Menschen versucht, sich die Welt zu erklären – aus reiner Neugier, oder um ihr Verständnis natürlicher Phänomene zum eigenen Vorteil einzusetzen. Konkret bedeutet das, man entwickelt eine Theorie, also eine mögliche Erklärung für ein Phänomen, mit dem man sich in seiner Lebenswelt konfrontiert sieht. Anschliessend versucht man, die Theorie anhand von weiterer Evidenz – also konkreten Beobachtungen – mit der Wirklichkeit abzugleichen. Seit dem Mittelalter hat sich aus diesem grundlegenden Mechanismus das System der Wissenschaft entwickelt. Dabei ist der Kern, dass gewisse Ansprüche an wissenschaftliche Theorien und auch an wissenschaftliche Evidenz gestellt werden.


## Simulationen entwickeln und auswerten

Jede Simulation durchläuft drei Phasen:

- Modellieren
- Simulieren
- Validieren

### Modellieren

Bevor man eine Simulation durchführen kann, muss man ein funktionales Computermodell entwickeln, also den relevanten Wirklichkeitsausschnitt modellieren. Das heisst, die Komplexität der Realität wird auf die wesentlichen Faktoren reduziert. Die Realität wird also nicht vollständig dargestellt. Diese Vollständigkeit ist aber gar nicht beabsichtigt, im Gegenteil: Es sollen lediglich die wesentlichen Einflussfaktoren identifiziert und dargestellt werden, die für den realen Prozess bedeutsam sind.

Der Prozess der Modellierung lässt sich grob in zwei Schritte unterteilen:

- Abstrahieren/Reduzieren: Der genaue Geltungsbereich bzw. Modellkontext muss definiert werden und die wesentlichen Einflussfaktoren bestimmt. Das Ziel ist ein möglichst einfaches Modell, das aber trotzdem die relevantesten Faktoren berücksichtigt.
- Konkretisieren/Umsetzen: Der Erklärungsansatz muss in ein funktionales Modell überführt werden, es gilt also, alle relevanten Eigenschaften und Funktionsweisen des reduzierten Abbilds als Programmcode – und damit maximal explizit – zu formulieren. Damit die Simulation bzw. das virtuelle Experiment dann konkret durchgespielt werden kann, müssen häufig auch relevante Aspekte der Umwelt bzw. des experimentellen Settings abgebildet, also modelliert werden.

### Simulieren

Simulieren bedeutet, ein funktionales Modell in Bewegung zu versetzen und sein Verhalten in bestimmten Situationen zu beobachten. Je nach Komplexität des Modells kann dieser Vorgang langwierig und rechenintensiv sein – auch, weil ein einziger Durchgang selten genügt.

Die Simulation oder Simulierung ist eine Vorgehensweise zur Analyse von Abläufen in der Realität, die für die theoretische oder formelmässige Behandlung zu komplex sind, vor allem dann, wenn es sich um dynamisches Verhalten handelt. Konkret heisst das: Für die Simulation werden Experimente an einem Modell durchgeführt, um Erkenntnisse über die Realität zu gewinnen.

Wie bei realen Experimenten versucht man in Simulationen meist, die Auswirkungen bestimmter Eigenschaften oder Rahmenbedingungen auf das Gesamtsystem zu untersuchen – dazu werden die **Parameter** in verschiedenen Durchläufen variiert.

Neben der gezielten Variation von Parametern gibt es noch einen weiteren Grund dafür, dass in Simulationen oft sehr viele Durchgänge nötig sind: stochastische Parameter. Häufig ist es sinnvoll, bestimmte Eigenschaften des Anfangszustands einer Simulation zufällig festzulegen – oder ein Modell befolgt in jeder Runde mit einer bestimmten Wahrscheinlichkeit die eine oder andere Regel. Enthält eine Simulation solche Zufallselemente (stochastischen Parameter), ist ein einzelner Durchgang nur bedingt aussagekräftig, da das Ergebnis ja zumindest zu einem gewissen Grad durch Zufall zustande kam. Es braucht also mehrere Wiederholungen, um das «typische» Verhalten des Modells in dieser Situation (mit ansonsten unveränderten Parametern) abzuschätzen.

### Validieren

Nach dem Beobachten der Simulation erfolgt die Auswertung der gesammelten Daten. Besonderes Augenmerk gilt den Unterschieden zwischen den verschiedenen Durchgängen.

Beim Validieren einer Simulation geht es darum, das Verhalten des Modells (ggf. in Bezug auf bestimmte Parameter) auszuwerten und mit dem Verhalten des realen Systems zu vergleichen.

#### Ergebnisse visualisieren

Da es meist um dynamische Systeme und grosse Mengen von Daten geht, ist oft schon die Beurteilung des Modellverhaltens in einem einzigen Durchgang eine Herausforderung – noch schwieriger wird es, wenn verschiedene Durchgänge zusammengefasst oder verglichen werden sollen. Für die Beurteilung und Auswertung von Simulationen werden daher verschiedene Visualisierungstechniken eingesetzt.

In Simulationen wird oft ein erheblicher Aufwand betrieben, um die wesentlichen Eigenschaften des Systems im Verlauf der Simulation visuell darzustellen – obwohl die Implementierung einer solchen Prozessvisualisierung meist viel mehr Code braucht als die eigentliche Umsetzung des funktionalen Modells. So werden beispielsweise relevante Eigenschaften von Agenten (Position, Zustand, wichtige Werte) dargestellt, damit man ihre Veränderung im Verlauf der Simulation direkt beobachten kann. Das gibt erste Hinweise darauf, ob das Modellverhalten plausibel scheint oder ob ggf. Fehlern bei der Umsetzung gemacht wurden.

Wenn die Ergebnisse der Simulation(en) angemessen ausgewertet und (visuell) aufbereitet sind, kommt der letzte und in wissenschaftlicher Hinsicht entscheidende Schritt: Der Vergleich mit der Realität.

#### Abgleich mit der Realität

Im einfachsten Fall genügt der Abgleich mit bereits existierender Evidenz – oder abwarten, z.B. bei Wetterprognosen. Schon schwieriger wird es, wenn entsprechende Experimente erst noch entwickelt und durchgeführt werden müssen – oder, wenn lange Zeiträume und/oder schwierige Messungen involviert sind, wie bspw. bei Klimaprognosen. Und dann gibt es noch viele Fälle, in denen der Anwendungsbereich einen direkten Vergleich mit experimentellen Daten verbietet – z.B. Simulationen des Urknalls oder gesamtgesellschaftlicher Dynamiken. Dazu kommt die immer präsente Frage, ob das Modell tatsächlich die relevanten Aspekte des Realitätsausschnitts abbildet, denn die notwendige Vereinfachung bedingt auch, dass man eine 100%-ige Übereinstimmung der Ergebnisse nicht erwarten kann. Gerade diese entscheidende Validierung des Modells an der Realität ist also oft nur indirekt, bruchstückhaft, oder unter bestimmten Vorbehalten möglich. Aber andererseits war ja von vornherein klar, dass es nicht um eine komplette Abbildung der Realität gehen kann, sondern nur darum, dass die Theorie/das Modell im Verlaufe immer weiterer Iterationen durch den Prozess der Wissenschaftlichen Methode zunehmend besser darin wird, die wichtigsten Aspekte des realen Systems abzubilden.

[^1]: Quelle: [ofin.ch](https://oinf.ch/kurs/simulationen/), CC BY-NC-SA 4.0
[^2]: Quelle: [ofin.ch](https://oinf.ch/kurs/simulationen/modellieren/), CC BY-NC-SA 4.0
[^3]: Quelle: [ofin.ch](https://oinf.ch/kurs/simulationen/simulieren/), CC BY-NC-SA 4.0
[^4]: Quelle: [ofin.ch](https://oinf.ch/kurs/simulationen/validieren/), CC BY-NC-SA 4.0
