# Arbeiten mit Listen
---

Eine Liste ist ein «zusammengesetzter» Datentyp und besteht aus mehreren Elementen. In einer Liste können Zahlen, Strings (also Zeichenfolgen) oder Boole'sche Werte (`True` resp. `False`) gespeichert werden. Es sind auch gemischte Listen und Listen von Listen möglich.

## Eine Liste definieren

Python erkennt Listen anhand der eckigen Klammern, vorgegebene Werte werden durch Kommas getrennt. Die Liste darf auch leer sein.

``` python
liste = [1, 17.5, "Guten Tag", True]          # gemischte Liste

arbeitstage = ["Mo", "Di", "Mi", "Do", "Fr"]  # Liste mit 5 Elementen
monate = ["Januar"]                           # Liste mit einem Elemente
jahre = []                                    # leere Liste
```

## Eine Liste ausgeben

Es ist möglich, eine Liste mit Hilfe der `print()`-Funktion komplett auszugeben.

``` python
arbeitstage = ["Mo", "Di", "Mi", "Do", "Fr"]
print(arbeitstage)
```

Die Ausgabe sieht so aus:
```
['Mo', 'Di', 'Mi', 'Do', 'Fr']
```

Soll die Liste an einen Text angehängt werden, ist es wie bei Zahlen nötig, die Liste in einen String umzuwandeln:

``` python
arbeitstage = ["Mo", "Di", "Mi", "Do", "Fr"]
print("Die ersten fünf Tage der Woche heissen: " + str(arbeitstage))
```

## Auf ein bestimmtes Element zugreifen

Mit der Klammerschreibweise kann direkt auf ein bestimmtes Element der Liste zugegriffen werden. Dazu schreibt man den **Index**, also die Position des gewünschten Elements in die eckigen Klammern direkt hinter den Namen der Liste.

::: info Wichtig
Beim Arbeiten mit Listen muss stets beachtet werden, dass das erste Element der Liste den Index `0` hat!
:::

``` python
arbeitstage = ["Mo", "Di", "Mi", "Do", "Fr"]

print(arbeitstage[0])               # gibt "Mo" aus
arbeitstage[0] = "So"               # überschreibt den bisherigen Wert "Mo"
print(arbeitstage[0])               # gibt nun "So" aus
```

Es ist sogar möglich, die Elemente von hinten zu zählen, dabei gibt man als Index *negative Zahlen* an:

``` python
arbeitstage = ["Mo", "Di", "Mi", "Do", "Fr"]

print(arbeitstage[-1])              # gibt "Fr" aus
print(arbeitstage[-5])              # gibt "Mo" aus
```

## Die Länge einer Liste bestimmen

Mit Hilfe der Funktion `len()` kann die Länge (also die Anzahl Elemente) einer Liste bestimmt werden:

``` python
arbeitstage = ["Mo", "Di", "Mi", "Do", "Fr"]
laenge = len(arbeitstage)
print("Die Liste enthält " + str(laenge) + " Elemente.")
```

::: info Wichtig
Da der Index des ersten Elementes `0` ist, ist der Index des letztes Elementes der Liste `len(liste) - 1`!
:::

## Eine Liste abarbeiten

Häufig will man mit jedem Element einer Liste etwas Bestimmtes tun. Die bereits bekannte `for`-Schleife eignet sich hervorragend zum «Abarbeiten» einer Liste. Im nachfolgenden Beispiel wird jedes Element auf einer eigenen Zeile ausgegeben.

``` python
arbeitstage = ["Mo", "Di", "Mi", "Do", "Fr"]

for index in range(0, len(arbeitstage)):
    print(arbeitstage[index])
```

Um den Index in einem sinnvollen Bereich zu halten, erzeugen wir mit der `range()`-Funktion einen Zahlenbereich von 0 bis zur Länge der Liste. Praktisch ist, dass die Zahl, die der Länge entspricht, selber nicht mehr zum Bereich gehört – wie es auch kein Element der Liste mit Index = Länge der Liste gibt.

In der Liste selber greifen wir mit der von oben bekannten Klammerschreibweise auf das entsprechende Element zu.

Häufig verwendet man anstelle von `index` schlicht die Variable `i`. Die Variable `i` kommt also in Schleifen besonders häufig vor:

``` python
arbeitstage = ["Mo", "Di", "Mi", "Do", "Fr"]

for i in range(0, len(arbeitstage)):
    print(arbeitstage[i])
```

::: exercise Aufgaben
1. Erstelle eine Liste mit sämtlichen Wochentagen und gibt diese auf drei Arten aus:
   - als Liste,
   - als Liste in einem Satz und
   - als einzelne Elemente (jeweils eines pro Zeile).
2. Erstelle eine Liste mit einigen Noten und berechne dann den Durchschnitt (ohne dass du die Anzahl Noten direkt als Zahl in die Formel schreibst!) und gib ihn aus.
3. Schreibe eine Funktion `durchschnitt(noten)`, die eine Liste von Noten erwartet und daraus den Durchschnitt berechnet. Das Resultat soll mit `return` zurückgegeben werden. Rufe die Funktion auf und gib das Resultat aus.
***
1.
2. Du brauchst eine Variable, in der du die Noten aufsummierst. Anschliessend kannst du die Summe durch die Anzahl Elemente der Liste (sprich deren Länge) dividieren.
3. Das meiste hast du bereits programmiert. Überlege, welche Zeilen in die Funktion übernommen werden sollen.
***
1.
``` python
wochentage = ["Mo", "Di", "Mi", "Do", "Fr", "Sa", "So"]

print(wochentage)

print("Die Wochentage heissen: " + str(wochentage))

for i in range(0, len(wochentage)):
    print(wochentage[i])
```
2.
``` python
noten = [5.5, 4, 5, 5.5, 4.5]
notensumme = 0

for i in range(0, len(noten)):
    notensumme = notensumme + noten[i]

durchschnitt = notensumme / len(noten)
print("Der Notendurchschnitt beträgt " + str(durchschnitt))
```
3.
``` python
def durchschnitt(noten):
    notensumme = 0

    for i in range(0, len(noten)):
        notensumme = notensumme + noten[i]

    durchschnitt = notensumme / len(noten)
    return durchschnitt


notenliste = [5.5, 4, 5, 5.5, 4.5]
schnitt = durchschnitt(notenliste)
print("Der Notendurchschnitt beträgt " + str(schnitt))
```
:::
