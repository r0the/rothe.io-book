# Caesar programmieren
---

## Rechnen mit Zeichen

Mathematische Operationen können nur mit Zahlen (und leider nicht mit Buchstaben) ausgeführt werden. Folglich müssen die Buchstaben vor der Berechnung in Zahlen umgewandelt und nach der Rechnung zurückkonvertiert werden.

Eine gute Möglichkeit, die zu tun, ist, die Position eines Buchstabens in einer bestimmten Zeichenkette zu bestimmen:

``` python
ALPHABET = "ABCDE"
code = ALPHABET.find("C")        # code ist 2
if code != -1:
    code = (code + 3) % len(ALPHABET) # code ist 0
    buchstabe = ALPHABET[code]        # verschlüsselter Buchstabe: D
```

Zudem können Zeichenketten oder auch einzelne Buchstaben wie folgt in Grossbuchstaben umgewandelt werden:

``` python
text = "Sehr geheim!"
gross = text.upper()    # Resultat: SEHR GEHEIM!
```

::: box exercise
#### :exercise: Aufgabe: Caesar
Programmiere die Funktionen zum Verschlüsseln und zum Entschlüsseln mit der Caesar-Methode.

1. Es sollen erst nur die Grossbuchstaben A bis Z verschlüsselt werden.
2. Der Text soll vor dem Verschlüsseln in Grossbuchstaben umgewandelt werden.
3. Erweitere das Alphabet so, dass auch Umlaute verschlüsselt werden.
***
``` python solutions/caesar1.py
```
:::

::: box exercise
#### :extra: Zusatzaufaufgabe
Entwickle mit appJar eine Benutzeroberfläche für die Caesar-Verschlüsselung.
:::

## Caesar knacken
::: box exercise
#### :exercise: Aufgabe: Caesar knacken
Überlege dir, wie man ohne Kenntnis des Schlüssels eine verschlüsselte Nachricht knacken könnte. Tausche dazu mit deinem/r Pultnachbarn/in einen kurzen verschlüsselten Text aus und versuche, ihn zu entschlüsseln.

Schreibe auf, wie du vorgehst und überlege dir, ob das man programmieren kann resp. welche Python-Kenntnisse du dazu noch brauchst.
:::
