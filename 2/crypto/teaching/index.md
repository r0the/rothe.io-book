# Material für Lehrpersonen
---

von Tom Jampen

### 1. Doppellektion: Einstieg mit antiker symmetrischer Verschlüsselung

#### Lernziele

- Du verstehst das Prinzip symmetrischer Verschlüsselungsverfahren und wendest das Wissen in Beispielen mit antiken Verfahren an.
- Du greifst antike symmetrische Verfahren an (Häufigkeitsanalysen, Brute Force, ...).


#### 1.1 Verschlüsselungsverfahren kennenlernen und verschlüsselte Texte knacken
Die SuS arbeiten in Gruppen und lernen je ein Verfahren zur Verschlüsselung genauer kennen (antike Verfahren).
Sie erhalten dazu alle je einen (den anderen Gruppen unbekannten) Text, der mit diesem Verfahren verschlüsselt worden ist. In den ersten ~15min kennen sie das Verfahren nicht. Sie versuchen, den Text mit eigenen Ideen zu entschlüsseln.
Nach spätestens 20min wird jeder Gruppe gesagt, mit welchem Algorithmus ihr Text verschlüsselt ist. Sie recherchieren, studieren das Verfahren und versuchen so, den verschlüsselten Text zu entschlüsseln. Falls sie die Nachricht dennoch nicht entschlüsseln können, kann ihnen der Schlüssel ausgehändigt werden.
Verfahren:

- Skytale
- Polybios
- Caesar
- ROT-13
- Atbash
- Monoalphabetische Substitution
- Vigenère
- ADFGX

Unterlagen für die Lehrperson (docx)
Verschlüsselte Texte (docx)

**Hinweis:** Ist eine Gruppe früher fertig, darf diese Gruppe weitere verschlüsselte Texte anschauen und versuchen zu entschlüsseln.


#### 1.2 Verfahren vorstellen

Die Gruppen stellen im Verlaufe der zweiten Lektion den untersuchten Algorithmus kurz (5min) vor und berichten, ob resp. wie sie den Text entschlüsseln konnten.

Ziel:

- Kennenlernen von einfachen symmetrischen Verfahren
- Austausch, Wissensicherung
- Überlegungen zum Knacken von Algorithmen


#### 1.3 Hausaufgabe
Als Aufgabe auf die nächste Doppellektion sollen die SuS versuchen, einen Text zu entschlüsseln, der mittels Vernam One-Time Pad verschlüsselt ist.

Auftrag One-Time Pad (docx)
Auftrag One-Time Pad mit Lösung (docx)

### 2. Doppellektion: Grundlagen der Kryptologie und moderne symmetrische Verfahren

#### Lernziele

- Du kennst das Prinzip von Kerckhoffs und kannst nachvollziehen, wieso es sinnvoll ist, die Funktionsweise von Algorithmen nicht geheimzuhalten (Security through Obscurity).
- Du weisst, dass moderne symmetrische Verfahren mit Bitfolgen anstelle von Buchstaben arbeiten und mathematisch viel komplexer sind.

#### 2.1 Grundlagen und Kerckhoffs' Prinzip

Die SuS stellen allenfalls weitere (zu Hause) entschlüsselte Texte vor.

Kurze Präsentation mit Überblick über Kryptologie (Kryptographie, Kryptoanalyse) und den wichtigsten Aspekten der Kryptographie:

- Wichtige Begriffe
- Niemand soll wissen, was ich kommuniziere (Vertraulichkeit, d.h. Daten müssen verschlüsselt sein)
- Daten dürfen unterwegs/später nicht manipuliert worden sein/werden können (Integrität)
- Niemand darf in meinem Namen Informationen senden (Digitale Signatur) oder sich als mich ausgeben (Authentifizierung)
- erster Teil von Bruce Schneiers Analogie mit dem Design von Safes zeigen

Abstimmung (z.B. nach Prof. Mazur): Frage: Ist die Offenlegung des Verschlüsselungsalgorithmus für seine Sicherheit förderlich?

- Antwort 1: Ja, eine Offenlegung erhöht die Sicherheit.
- Antwort 2: Es spielt keine Rolle.
- Antwort 3: Nein, eine Offenlegung senkt die Sicherheit.

Lösung nicht bekanntgeben. Stattdessen 5-10min Diskussion unter den SuS mit denjenigen Nachbarn, die anders gestimmt haben (Peer Instruction).

Anschliessend erneute Abstimmung zur gleichen Frage und Kontrolle, ob sich die Anzahl korrekte Antworten (Antwort 1) vergrössert hat.

Erkenntnissicherung:
- Teil von Bruce Schneiers Analogie mit dem Design von Safes zeigen
- Prinzip von Kerckhoffs: "Die Sicherheit eines Kryptosystems darf nicht von der Geheimhaltung des Algorithmus abhängen. Die Sicherheit gründet sich nur auf die Geheimhaltung des Schlüssels."

Präsentation Kryptologie Grundlagen

#### 2.2 Moderne symmetrische Verschlüsselung

Die SuS lösen einzeln das Arbeitsblatt "Vom One-Time Pad zur modernen Blockchiffre". Dabei wird - ausgehend vom One-Time Pad (diesmal mit 0 und 1 und XOR) das Prinzip von Blockchiffren und zwei Betriebsarten (ECB und CBC) erläutert und erarbeitet.

Vom One-Time Pad zur modernen Blockchiffre (docx)
Vom One-Time Pad zur modernen Blockchiffre mit Lösung (docx)

#### 2.3 Schlüsselraum und konkrete moderne Verfahren

Abschlusspräsentation mit kurzer Vostellung aktuellerer Verfahren (z.B. AES) sowie Einführung des Begriffs "Schlüsselraum", Überlegungen zum Schlüsselraum der betrachteten Verfahren sowie Berechnung der benötigten Zeit bei Brute-Force-Angriff.

Evtl Bezug zu behandelten Algorithmen:

- ADFGX kann zwar von Hand nur geknackt werden, wenn das Verfahren offen liegt. Mehrere Mitteilungen mit demselben Schlüssel ermöglichen es, dem Verfahren auf die Spur zu kommen.
- Polybios, Caesar, Atbash und sogar monoalphabetische Substitution hingegen können problemlos geknackt werden, auch wenn Verfahren nicht offen liegt (Häufigkeitsanalysen).
- OTP wiederum kann unmöglich geknackt werden, auch wenn das Verfahren komplett bekannt ist. Im Gegenteil: Es können damit sämtliche möglichen Texte gleicher Länge bei geschickter Schlüsselwahl entschlüsselt werden (Beispiele zeigen und somit Aufgabe von letzter Woche besprechen).

Je nach Zeit: Mit CrypTool zu zweit die Visualisierung von AES anschauen und diskutieren.

Erkenntnisse:
- Aktuelle Verfahren sind massiv komplexer
- Der Schlüsselraum aktueller Verfahren ist riesig

Präsentation Schlüsselraum und moderne Verfahren

### 3. Doppellektion: Schüsselaustausch und Grundlagen asymmetrischer Verschlüsselung

#### Lernziele

- Du erkennst das Problem des Schlüsselaustauschs bei symmetrischen Verfahren.
- Du kennst Man-in-the-Middle-Angriffe und verstehen, dass auch öffentliche Schlüssel auf ihre Echtheit überprüft werden müssen.
- Du begreifst asymmetrische Verfahren mit ihren Schlüsselpaaren als Hilfsmittel für einen sicheren Austausch (z.B. Vorhängeschloss/Schlüssel-Analogie).
- Du verstehst Zertifikate als Bestätigung der Echtheit von öffentlichen Schlüsseln durch eine vertrauenswürdige Stelle mit Hilfe einer digitalen Signatur (z.B. bei HTTPS).


#### 3.1 Alice, Bob und Eve
Einstieg mit kurzer Rekapitulation des bisher gelernten (aktuelle symmetrische Verfahren sind sicher, nutzen aber den gleichen Schlüssel zur Ver- und Entschlüsselung). Die "berühmten" Krypto-Charakteren Alice, Bob und Eve werden vorgestellt. Zudem wird das heutige Problem bekanntgegeben: Wie soll der Schlüssel für die sichere Kommunikation über einen unsicheren Kanal ausgetauscht werden, wenn Alice und Bob sich nicht persönlich treffen können?

Präsentation Schlüsselaustausch (inkl. Folien für die nächsten Kapitel)

#### 3.2 Schlüsselaustausch
Die SuS arbeiten in 4-er Gruppen (jede/r hat eine Funktion: Alice=A, Bob=B, Eve=Eavesdroppen und Troy=Trusted-Third-Party). Die Funktion der einzelnen wird natürlich nicht verraten. Ziel mittels Zündholzschachtel und einigen Vorhängeschlössern ein Verfahren entwickeln, mit dem der Inhalt der Box sicher übertragen werden kann, ohne dass die Schlüssel zu den Vorhängeschlössern ausgetauscht werden müssen.

Material:
- mind. 2 Set Vorhängeschlösser (je gleichschliessend), z.B.: https://www.galaxus.ch/de/s4/product/burg-waechter-vorhaengeschloss-4er-set-quadro-4stk-vorhaengeschloss-8437739?tagIds=139&supplier=2804880
- grosse Streichholzschachteln, z.B.: https://elkverlag.ch/streichholzschachteln-gross.html

Mögliche Lösungen für Versand von Alice zu Bob:

- Alice verschliesst Box mit ihrem Schloss und schickt sie Bob. Dieses fügt noch sein Schloss hinzu und schickt die Box zurück. Alice nimmt ihr Schloss weg und schickt Box wieder zu Bob.
- Bob deponiert offene Schlösser bei Troy (=Post). Alice bringt Box zur Post, verlangt das Schloss von Bob und verschliesst damit die Box. Nur Bob kann die Box mit seinem Schlüssel öffnen.

#### 3.3 Schlüsselaustausch nach Diffie-Hellman (Analogie mit Farben)
Die SuS spielen auf einer Webseite (z.B. https://www.inf-schule.de/kommunikation/kryptologie/modernechiffriersysteme/exkurs_diffie) den Schlüsselaustausch gemäss Diffie-Hellman mit der Farben-Analogie durch und verstehen, wie man sich auf eine Farbe (=eigentlich eine Zahl) einigen kann, ohne diese über einen öffentlichen Kanal zu übertragen.

#### 3.4 Asymmetrische Verschlüsselung

Präsentation:

- Was haben diese Verfahren gemeinsam?
  - Es wird pro Person ein Schlüsselpaar (mit privatem und öffentlichem Schlüssel) verwendet
  - Vorhängeschlosss = öffentlicher Schlüssel, Schlüssel dazu = privater Schlüssel

- Prinzip der asymmetrischen Verschlüsselung
- Analogie mit einem Schloss mit 2 möglichen Schlüsselpositionen ("zu" - "offen"). Der private Schlüssel kann nur im Uhrzeigersinn drehen (von "zu" nach "offen"), der öffentliche nur im Gegenuhrzeigersinn (von "offen" nach "zu").
- Einsatz asymmetrischen Verschlüsselung (Session-Key für symmetrische Verschlüsselung schützen)

### 4. Doppellektion: Integrität, Authentifizierung - Hashfunktionen

#### Lernziele
- Du weisst, dass Passwörter nicht im Klartext gespeichert werden.
- Du verstehst Hashverfahren als Möglichkeit zur sicheren Speicherung von Passwörtern und zur Intregritätsprüfung.
- Du kennst Angriffsmöglichkeiten (Dictionnary, Brute Force) auf Passworthashes.
- Du kannst die Stärke von Passwörtern einschätzen und weisst, dass die Länge die massgebende Grösse für ein sicheres Passwort ist.

#### 4.1 Hashfunktionen
Die SuS lesen den Text von Pascal Schuppli über Hashfunktionen und Kollisionen (Beispiel mit Kindern und farbigen Stühlen) und lösen Aufgaben dazu.

Präsentation Hashfunktionen und Passwörter (inkl. Folien für die nächsten Kapitel)
Sequenz von Pascal Schuppli

#### 4.2 Passwörter
Passwörter werden nicht im Klartext sondern in gehashter Form gespeichert. Die SuS überlegen sich in Zweiergruppen, wie man ein Passwort herausfinden könnte, wenn man den Hashwert kennt (Brute Force- oder Dictionnary-Attack). Anschliessend lösen sie den Auftrag zu Passwörtern (Passwörter einschätzen, Anzahl Möglichkeiten ausrechnen je nach Länge und verwendeter Zeichen, existierende Hashes knacken).

#### 4.3 Einsatz von Hashfunktionen

Abschlusspräsentation zur Erkenntnissicherung:

- Hashfunktionen sind Einwegfunktionen und lassen sich nicht umkehren
- Kryptographische Hashfunktionen müssen Zusatzbedingungen erfüllen
  - keine Kollisionen auffindbar in vernünftiger Zeit
  - 1 Bit am Input ändern -> ca 50% Änderung der Output-Bits
- Inhalte können gehasht werden, um Manipulation zu entdecken


### 5. Doppellektion: Digitale Signatur und Zertifikate

#### Lernziele

- Du verstehst, dass das Prinzip der asymmetrischen Verschlüsselung auch für digitale Signaturen eingesetzt werden kann.
- Du verstehst die Rolle von Zertifizierungsstellen und kannst nachvollziehen, was Webseitenzertifikate bedeuten.


#### 5.1 Digitale Signaturen
Analogie mit dem Schloss mit 2 Positionen auf ein Schloss mit 3 Positionen erweitern ("zu" - "offen" - "zu"). Auftrag "Digitale Signaturen" bearbeiten (Was sind digitale Signaturen, Analogie erweitern, arbeiten mit digitalen Signaturen in CrypTool, Angriff überlegen).

#### 5.2 Zertifikate
Auftrag "Zertifikate": Lösung für Man-in-the-Middle-Angriff, Funktionsweise, Video BSI, Aufgaben.

Erkenntnissicherung z.B. mit Hilfe eines Kahoot-Quiz.

### 6. Doppellektion: Angriffe auf Systeme und Personen

#### Lernziele

- Du bist dir bewusst, dass für erfolgreiche Angriffe auf Systeme häufig Fehlverhalten von Personen verantwortlich ist.
- Du weisst, dass Personen daher ein beliebtes Angriffsziel sind (z.B. Social Engineering, Phishing).
- Du weisst, dass Systeme Schwachstellen aufweisen und somit Angriffsflächen bieten.
- Du verstehst, dass regelmässige Softwareupdates, aktuelle Antivirensoftware und eine aktive Firewall für die Systemsicherheit zentral sind.
- Du verstehst die Wichtigkeit, dieselben Passwörter nicht für mehrere Dienste/Systeme einzusetzen und kennst Möglichkeiten der Passwortverwaltung (z.B. Passwortmanager).
- Du verstehst, dass das Passwort des E-Mail-Accounts besonders wichtig ist (z.B. wegen der Passwortrücksetzung via E-Mail).

#### 6.1

- Phishing


#### 6.2 Angriffs-Demos

Vorführen einiger Angriffe
- Ausführen von beliebigem Code mittel Rubber-Ducky-USB-Stick (gibt sich als Tastatur aus und beginnt zu tippen)
- Fake-SMS schicken über e-call
