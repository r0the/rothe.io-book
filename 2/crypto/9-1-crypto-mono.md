# Monoalphabetische Methoden
---

## Unsichtbare Tätowierung

Die folgende Geschichte ist durch den griechischen Historiker Herodot überliefert:

Im Jahr 499 v. Chr. wollte Histiaios mit Hilfe seines Neffen einen Aufstand gegen den persischen König Darius anzetteln. Da er Berater des Königs war, musste er behutsam vorgehen. Er rasierte seinem zuverlässigsten Sklaven den Kopf und tätowierte eine Nachricht darauf. Nachdem die Haare nachgewachsen waren, schickte er den Sklaven zu seinem Neffen, welcher ihm den Kopf wieder rasieren sollte. Er tat dies und setzte die tätowierte Aufforderung zur Revolte in die Tat um.

Das Verstecken oder Verbergen von Informationen und Nachrichten wird Steganografie genannt. Das Ziel ist, dass ein Aussenstehender die Existenz einer Nachricht gar nicht wahrnimmt. Im Gegensatz dazu geht die Kryptologie davon aus, dass die Existenz der Nachricht bekannt ist, sie jedoch nicht entziffert werden kann.

## Caesar-Verschlüsselung

Schema der Caesar-Verschlüsselung. Quelle: Wikimedia Commons

Der römische Feldherr Gaius Julius Caesar hat seine militärischen Nachrichten verschlüsselt. Der römische Schriftsteller Sueton hat folgendes überliefert:

> … is qua occultius perferenda erant, per notas scripsit, id est sic structo litterarum ordine, ut nullum verbum effici posset: quae si qui investigare et persequi velit, quartam elementorum litteram, id est D pro A et perinde reliquas commutet.

> … wenn etwas Geheimes zu überbringen war, schrieb er in Zeichen, das heißt, er ordnete die Buchstaben so, dass kein Wort gelesen werden konnte: Um diese zu lesen, tauscht man den vierten Buchstaben, also D für A aus und ebenso mit den restlichen.

Caesar hat also jeden Buchstaben seiner Nachrichten durch den Buchstaben ersetzt, welcher im Alphabet drei Stellen weiter hinten steht. Der Buchstabe D, welcher für A eingesetzt wird, wird Schlüssel genannt. Er muss bekannt sein, um die Nachricht wieder entschlüsseln zu können.

![](images/caesar-chiffre.svg "Schema der Caesar-Verschlüsselung")

- Klartext:   ABCDEFGHIJKLMNOPQRSTUVWXYZ
- Geheimtext: DEFGHIJKLMNOPQRSTUVWXYZABC

Heute wird jede Verschlüsselung, die auf einer Verschiebung des Alphabets beruht, eine Casear-Verschlüsselung genannt.

::: box exercise

#### :exercise: Aufgabe 1: Caesar-Verschlüsselung

Entschlüssle die folgenden Caesar-Nachrichten:

- DOHD LDFWD HVW
- YHQL YLGL YLFL

:::

## ROT13

ROT13 ist eine Caesar-Verschlüsselung, bei welcher die Buchstaben um 13 Stelle verschoben werden. Der Buchstabe A wird also auf N abgebildet wird. Das spezielle an dieser Verschlüsselung ist, das man durch eine erneute Verschlüsselung wieder den Klartext erhält.

## Monoalphabetische Substitution

Allgemein wird ein solches Verschlüsselungsverfahren, bei welchem jedes Zeichen durch ein festgelegtes anderes Zeichen ersetzt wird, eine monoalphabetische Substitution genannt.

Bei dieser Substitution hängt die Anzahl möglicher Verschlüsselungen von der Grösse des zugrundeliegenden Zeichensatzes ab. Wenn $n$ Zeichen verwendet werden, gibt es $n!$ unterschiedliche Verschlüsselungen.

::: box exercise

#### :exercise: Aufgabe 2

Versucht in Zweiergruppen, die folgenden drei Geheimtexte zu entziffern. Es sind eine Transpositions- und zwei Substitutionsverschlüsselungen verwendet worden:

- RVAR IREFPUYÜFFRYGR ANPUEVPUG
- DFNCUSNH AOEHEEIT RHSLSLC
- HIILCLHQW CX HQWVFKOXHVVHOQ VHLQ
:::

## Grundbegriffe

Zusammenhang zwischen Klartext, Geheimtext und Schlüssel
Quelle: Wikimedia Commons

- **Klartext** ($p$) engl. plaintext, unverschlüsselte Nachricht
- **Ge­heim­text** ($c$) engl. ciphertext, verschlüsselter Text
- **Schlüssel** ($k$) engl. key, Information, welche benötigt wird, um den Klartext zu ver- bzw. den Geheimtext zu entschlüsseln
- **Verschlüsselungsverfahren:** Algorithmus, welche einen Klartext unter Verwendung eines Schlüssels in einen Geheimtext überführt oder umgekehrt.

![](images/symmetric-cryptosystem.svg "Zusammenhang zwischen Klartext, Geheimtext und Schlüssel")

### Schlüsselraum

Die Sicherheit eines Verschlüsselungsverfahrens hängt stark von der Anzahl möglicher Schlüssel, also von der Grösse des Schlüsselraums ab.

Die Grösse des Schlüsselraums wird in Bit (d.h. als binärer Logarithmus) angegeben. Bei zwei möglichen Schlüsseln spricht man von 1 Bit, bei 1024 möglichen Schlüsseln von 10 Bit.

::: box exercise

#### :exercise: Aufgabe 3: Schlüsselräume

Gib die Grösse des Schlüsselraumes für die folgenden Verschlüsselungsverfahren an:

- Skytale
- Rail Fence
- Caesar
- ROT-13
- beliebige monoalphabetische Substitution

:::

## Häufigkeitsanalyse

![](images/al-kindi.jpg "al-Kindi")

Im neunten Jahrhundert hat der arabische Gelehrte Abū Ya'qūb ibn Ishāq al-Kindī (oder kurz al-Kindi) durch eine Analyse des Korans entdeckt, dass die einzelnen Zeichen der arabischen Schrift in bestimmten Häufigkeiten auftreten. In seiner Abhandlung über die Entzifferung kryptographischer Botschaften schrieb er folgendes:

> Eine Möglichkeit, eine verschlüsselte Botschaft zu entziffern, vorausgesetzt, wir kennen ihre Sprache, besteht darin, einen anderen Klartext in derselben Sprache zu finden, der lang genug ist, um ein oder zwei Blätter zu füllen, und dann zu zählen, wie oft jeder Buchstabe vorkommt. Wir nennen den häufigsten Buchstaben den «ersten», den zweithäufigsten den «zweiten», den folgenden den «dritten» und so weiter, bis wir alle Buchstaben in der Klartextprobe durchgezählt haben. Dann betrachten wir den Geheimtext, den wir entschlüsseln wollen, und ordnen auch seine Symbole. Wir finden das häufigste Symbol und geben ihm die Gestalt des «ersten» Buchstabens der Klartextprobe, das zweithäufigste Symbol wird zum «zweiten» Buchstaben, das dritthäufigste zum «dritten» Buchstaben und so weiter, bis wir alle Symbole des Kryptogramms, das wir entschlüsseln wollen, auf diese Weise zugeordnet haben.

Durch die Häufigkeitsanalyse werden Geheimtexte, die durch eine monoalphabetische Substitution verschlüsselt worden sind, grundsätzlich entzifferbar. Das heisst, sie können ohne Kenntnis des Schlüssels decodiert werden, indem eine Häufigkeitsanalyse vorgenommen wird. Für die Entzifferung einer Caear-Chiffre genügt es, den häufigsten Buchstaben zu suchen und anschliessend den Schlüssel zu wählen, der E auf diesen Buchstaben abbildet.
