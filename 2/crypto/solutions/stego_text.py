ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÜ .?"

ART = [ "Ein", "Der", "Kein", "Jeder" ]
SUB = [
    "Sommer", "Winter", "Computer", "Vogel", "Fisch", "Gewitter", "Berg", "Gedanke"
]

VERB = [ "macht", "geht", "kommt", "steht", "fliegt", "schwimmt", "lebt", "ist" ]
ADV = [ "oft", "selten", "manchmal", "nie"]

REL = [ "mit", "wegen"]
ADJ = [ "grünen", "roten", "gelben", "blauen" ]
OBJ = [ "Tuplen.", "Nüssen.", "Ideen.", "Socken." ]


def stego_encrypt(text):
    text = text.upper()
    encrypted = ""
    pos = 0
    for c in text:
        code = ALPHABET.find(c)
        if code != -1:
            if pos == 0:
                encrypted = encrypted + ART[code % 4] + " "
                encrypted = encrypted + SUB[code // 4] + " "
            elif pos == 1:
                encrypted = encrypted + VERB[code // 4] + " "
                encrypted = encrypted + ADV[code % 4] + " "
            elif pos == 2:
                encrypted = encrypted + REL[code // 16] + " "
                encrypted = encrypted + ADJ[(code % 16) % 4] + " "
                encrypted = encrypted + OBJ[(code % 16) // 4] + " "
            pos = (pos + 1) % 3
    return encrypted
            

def stego_decrypt(encrypted):
    text = ""
    pos = 0
    art = -1
    sub = -1
    verb = -1
    adv = -1
    rel = -1
    adj = -1
    obj = -1
    for word in encrypted.split(" "):
        if len(word) > 0:
            if pos == 0:
                art = ART.index(word)
            elif pos == 1:
                sub = SUB.index(word)
                text = text + ALPHABET[sub * 4 + art]
            elif pos == 2:
                verb = VERB.index(word)
            elif pos == 3:
                adv = ADV.index(word)
                text = text + ALPHABET[verb * 4 + adv]
            elif pos == 4:
                rel = REL.index(word)
            elif pos == 5:
                adj = ADJ.index(word)
            elif pos == 6:
                obj = OBJ.index(word)
                text = text + ALPHABET[rel * 16 + obj * 4 + adj]
            pos = (pos + 1) % 7
    return text

enc = stego_encrypt("Ein geheimes Passwort")
print(enc)
dec = stego_decrypt(enc)
print(dec)
