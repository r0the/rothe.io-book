from PIL import Image

RED = 0
GREEN = 1
BLUE = 2

LSB_MASK = 0x00000001
INVERTED_LSB_MASK = 0xfffffffe

BEGIN = '$$$'
END = '###'

FILENAME_LOADED = 'berge-loaded.png'


def reveal(filename, channel):
    image = Image.open(filename)
    pixels = image.load()
    width, height = image.size
    message = ''
    char_index = 0
    
    for y in range(0, height):
        for x in range(0, width):
            # get color of current pixel
            color = pixels[x, y]
            
            # prepare new character if index is 0
            if char_index == 0:
                char = 0
            
            # test whether LSB is 1
            if color[channel] & LSB_MASK == LSB_MASK:
                # shift LSB into correct position before adding
                bit = LSB_MASK << 7-char_index
                char += bit
                
            char_index += 1
            
            # test if current character is completed
            if char_index == 8:
                char_index = 0
                message += chr(char)
                
                if is_eom_reached(message):
                    return trim(message)
    
    return 'Unexpected end of file: ' + message


def is_eom_reached(message):
    # test for test presence of the END pattern
    return message[len(message) - len(END):len(message)] == END


def trim(message):
    # remove BEGIN and END patterns
    return message[len(BEGIN):-len(END)]


def main():
    message = reveal(FILENAME_LOADED, RED)
    print('Secret message:\n' + message)
    
    return


main()