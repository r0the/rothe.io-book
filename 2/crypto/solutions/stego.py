from PIL import Image

RED = 0
GREEN = 1
BLUE = 2

LSB_MASK = 0x00000001
INVERTED_LSB_MASK = 0xfffffffe

BEGIN = '$$$'
END = '###'