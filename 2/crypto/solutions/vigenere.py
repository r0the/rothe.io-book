ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def vigenere_encrypt(text, key):
    text = text.upper()
    key = key.upper()
    encrypted = ""
    for c in text:
        code = ALPHABET.find(c)
        if code == -1:
            encrypted = encrypted + c
        else:
            code = (code + key) % len(ALPHABET)
            encrypted = encrypted + ALPHABET[code]

print(encrypted)
