from PIL import Image
from stego import *


def analyse_lsb(filename, channel):
    image = Image.open(filename)
    pixels = image.load()
    width, height = image.size
    
    for y in range(0, height):
        for x in range(0, width):
            (r, g, b) = pixels[x, y]
            
            if channel == RED:
                lsb = r & LSB_MASK
                pixels[x, y] = (lsb * 255, 0, 0)
            elif channel == GREEN:
                lsb = g & LSB_MASK
                pixels[x, y] = (0, lsb * 255, 0)
            elif channel == BLUE:
                lsb = b & LSB_MASK
                pixels[x, y] = (0, 0, lsb * 255)
    
    return image


def main():
    red = analyse_lsb('berge-loaded.png', RED)
    red.save('berge-red.png')
    red.show()
    
    green = analyse_lsb('berge-loaded.png', GREEN)
    green.save('berge-green.png')
    
    blue = analyse_lsb('berge-loaded.png', BLUE)
    blue.save('berge-blue.png')
    
    return


main()