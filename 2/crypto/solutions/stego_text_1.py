ALPHABET = "GEHIM"

SUB = ["Sommer", "Winter", "Computer", "Vogel", "Fisch"]

def stego_encrypt(text):
    text = text.upper()
    encrypted = ""
    pos = 0
    for c in text:
        i = ALPHABET.find(c)
        if i != -1:
            encrypted = encrypted + SUB[i] + " "
    return encrypted

def stego_decrypt(encrypted):
    text = ""
    for word in encrypted.split(" "):
        if len(word) > 0:
            i = SUB.index(word)
            text = text + ALPHABET[i]
    return text

enc = stego_encrypt("GEHEIM")
print(enc)
print(stego_decrypt(enc))