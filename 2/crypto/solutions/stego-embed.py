from PIL import Image

RED = 0
GREEN = 1
BLUE = 2

LSB_MASK = 0x00000001
INVERTED_LSB_MASK = 0xfffffffe

BEGIN = '$$$'
END = '###'

FILENAME = 'berge.png'
FILENAME_LOADED = 'berge-loaded.png'
MESSAGE = 'Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.'


def embed(filename, channel, message):
    image = Image.open(filename)
    pixels = image.load()
    width, height = image.size
    
    message = BEGIN + message + END
    x = 0
    y = 0
    
    for char in message:
        for bit in format(ord(char), '08b'):
            # get color of current pixel
            color = pixels[x, y]
            
            # convert color tuple to list
            color = list(color)                
            
            if bit == '1':
                # change LSB to 1
                color[channel] |= LSB_MASK
            else:
                # change LSB to 0
                color[channel] &= INVERTED_LSB_MASK
            
            # convert color back to tuple
            color = tuple(color)
            
            # set new color for current pixel
            pixels[x, y] = color
            
            # move to next pixel in the same row
            x += 1
        
            # if last pixel in row is used, switch to next row
            if x >= width:
                x = 0
                y += 1
                
                if y >= height:
                    # oops, message too long
                    print('Message too long, truncated')
                    return image
    
    return image


def main():
    parcel = embed(FILENAME, RED, MESSAGE)
    parcel.save(FILENAME_LOADED)
    
    return


main()