ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

text = "Geheimer Text"
key = 3

text = text.upper()
encrypted = ""
for c in text:
    code = ALPHABET.find(c)
    if code == -1:
        encrypted = encrypted + c
    else:
        code = (code + key) % len(ALPHABET)
        encrypted = encrypted + ALPHABET[code]

print(encrypted)
