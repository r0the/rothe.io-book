# Kryptologie
---

Kryptologie (von kryptós, griechisch für «versteckt, verborgen, geheim») ist eine Wissenschaft, welche sich mit dem Verschlüsseln und Entschlüsseln von Informationen beschäftigt. Sie wird unterteilt in die zwei Teilgebiete

- **Kryptografie**, welche sich mit dem Verschlüsseln von Informationen beschäftigt, und
- **Kryptanalyse**, dem Studium von Methoden zum «Knacken» von verschlüsselten Informationen.

Heute werden kryptografische Techniken auch in anderen Gebieten wie bei digitalen Signaturen oder elektronischem Geld angewendet.

* [:link: NZZ: Bruce Schneier zur Crypto-Affäre](https://www.nzz.ch/schweiz/der-it-spezialist-bruce-schneier-zur-crypto-affaere-ld.1540118)
