# Steganografie
---


## Linguistische Steganographie

In Buch I und II von Johannes Trithemius’ Steganographia (1499/1500) werden die einzelnen Buchstaben des geheimzuhaltenden Textes, zuerst in nicht substituierter Form, dann mittels monoalphabetischer Substitution, in einem vorgegebenen Rhythmus, oft unter Einschluss von Leeren, zu neuen Wörtern gestreckt, und diese neuen Wörter syntaktisch und grammatikalisch korrekt zu einem thematisch stimmigen Text verbunden. In Buch I und II von Trithemius' Polygraphia (1508/1515) müssen die die Buchstaben des Klartexts ersetzenden Wörter vom Chiffrierer nicht länger selbst erfunden werden, sondern werden seitenlang und tabellarisch als syntaktisch und grammatisch aneinanderfügbare linguistische Fertigbauteile vorgegeben: in P I folgen auf 24 Substantive im Nominativ 24 entsprechende Adjektive, dann 24 Partizipien, dann 24 Akkusativobjekte, dann 24 Prädikate, dann 24 Dativobjekte usw. wobei die Worttabellen je einmal pro Buchstabe, von links nach rechts zu benutzen sind. So ergibt z. B. die Chiffrierung von lieber unmittelbar den Text „Illustrator sapientissimus gubernans celestia concedat requirentibus“.

![](images/trithemius-polygraphiae.jpg)


* [**Spamnic:** Nachricht als Spam-Mail tarnen][1]

```python solutions/stego_text.py
```

[1]: http://www.spammimic.com/

## Bilder

### Daten einbauen

```python solutions/stego-embed.py
```

### Daten auslesen

```python solutions/stego-reveal.py
```
