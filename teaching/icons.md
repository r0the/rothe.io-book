# Icons
---

[Material Design Icons][1] als SVG-Dateien:

## Marken und Logos

![](images/icons/apple.svg) ![](images/icons/google.svg) ![](images/icons/instagram.svg) ![](images/icons/microsoft-windows.svg) ![](images/icons/twitter.svg) ![](images/icons/whatsapp.svg)

## Diverse

![](images/icons/alpha-v-circle-outline.svg) ![](images/icons/book-open-variant.svg) ![](images/icons/cellphone-link.svg) ![](images/icons/cog.svg) ![](images/icons/keyboard-outline.svg) ![](images/icons/lan.svg) ![](images/icons/music.svg) ![](images/icons/wifi.svg)

## Tastatur

![](images/icons/apple-keyboard-caps.svg) ![](images/icons/apple-keyboard-command.svg) ![](images/icons/apple-keyboard-control.svg) ![](images/icons/apple-keyboard-option.svg) ![](images/icons/apple-keyboard-shift.svg) ![](images/icons/keyboard-return.svg) ![](images/icons/keyboard-tab.svg)


[1]: https://materialdesignicons.com
