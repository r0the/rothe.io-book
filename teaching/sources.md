# Lehrmittel
---

## Allgemein

* [:link: OINF: Informatik am Gymnasium Kanton Aargau](https://oinf.ch/)
* [:link: OFIN: OF Informatik Kanton Bern](https://ofin.ch/)
* [:link: Swisseduc Informatik](https://swisseduc.ch/informatik/)
* [:link: Lehrerfortbildung Baden-Württemberg: Fachportal Informatik](https://lehrerfortbildung-bw.de/u_matnatech/informatik/gym/)
* [:link: ABZ ETHZ](http://www.abz.inf.ethz.ch/maturitatsschulen/unterrichtsmaterialien/)
* [:link: LearnIT.ch: Pädagogische Hochschule Schwyz :creative-commons:](http://ilearnit.ch/de/about.html)
* [:link: inf-schule :creative-commons:](http://www.inf-schule.de/)
* [:link: BOB3 :creative-commons:](https://www.bob3.org/de/sekundarstufe1)
* [:link: SRF mySchool](https://www.srf.ch/sendungen/myschool/themen/medien-und-informatik)
* [:link: Medienistik.de :creative-commons:](https://medienistik.wordpress.com/freie-materialien/)

## Allgemein auf Englisch

* [:link: Digital Schoolhouse](http://www.digitalschoolhouse.org.uk/playful-computing-activities)
* [:link: Computer Science Unplugged :creative-commons:](https://www.csunplugged.org/)
* [:link: Computer Science Field Guide :creative-commons:](http://www.csfieldguide.org.nz/en)

## ICT und Medien

* [:link: Office Hintergrund](https://office.suud.ch)
* [:link: Medienprofis-Test Pro Juventute](https://medienprofis-test.projuventute.ch/#)
* [:link: Computergrafik am Franz-Ludwig-Gymnasium :creative-commons:](http://cogra.flgrafik.de/)

## KI
* [:link: Computer Vision Basics in Microsoft Excel](https://github.com/amzn/computer-vision-basics-in-microsoft-excel)

## Algorithmen und Programmieren

* [:link: microbit.eeducation.at](https://microbit.eeducation.at/wiki/Hauptseite)
* [:link: freeCodeCamp](https://www.freecodecamp.org/)
* [:link: codeacadamy](https://www.codecademy.com/)
* [:link: Awesome micro:bit - A curated list of resources for the BBC micro:bit](https://github.com/carlosperate/awesome-microbit)
* [:link: Float Toy](http://evanw.github.io/float-toy/)

## Big Data

* [:link: Lernparcours jfc Medienzentrum](http://bigdata.jfc.info/)
* [:link: Museum für Kommunikation: Lehrmittel Big Data](https://www.mfk.ch/bigdata/)

## Videos

* [:youtube: Samichlaus is Watching You](https://youtu.be/RhKDs_Wgxh4)
* [:youtube: Ada und die Programmiererinnen](https://www.srf.ch/play/tv/srf-myschool/video/ada-und-die-programmiererinnen?id=5a0c67a4-02b9-4dea-831b-7ea7be22e5b1)
* [:youtube: Amazing mind reader reveals his ‘gift’](https://youtu.be/F7pYHN9iC9I)
