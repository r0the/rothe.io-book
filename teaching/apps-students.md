# Apps für Schüler*innen
---

::: cards 2
[![](images/thonny.png)][2]
#### [Thonny][2]
Python-Entwicklungsumgebung für Einsteiger. Ist einfach zu installieren und zu bedienen. Verschiedene Modi für Anfänger, Standard und Fortgeschrittene. Benutzeroberfläche wirkt eher altmodisch. Visualisierung von Variablen zu Laufzeit, Debugger. Meine erste Wahl.

:mdi-apple: :mdi-microsoft-windows: :mdi-linux:

***
[![](images/mu.png)][1]
#### [Mu Editor][1]
Python-Entwicklungsumgebung für Anfänger. Ist einfach zu installieren und zu bedienen. Ideal für Einsteiger. Unterstützt verschiedene Modi für Pygame Zero, micro:bit und andere. Ich vermisse die Funktion «Speichern als». Der Standard-Ordner ist unter Windows schwierig zu finden, ein Wechseln ist nicht möglich.

:mdi-apple: :mdi-microsoft-windows: :mdi-linux:

***
[![](images/pythonista.png)][3]
#### [Pythonista][3]
Python-Entwicklungsumgebung für iOS.

:mdi-cellphone: :mdi-apple:

***
![](images/planner.png)
#### Microsoft Planner
Bildet Taskboards aus der agilen Projektmethodik ab. Gut geeignet, um Schüler*innen ein Projekt (z.B. die Maturaarbeit) planen und durchführen zu lassen. Kann in Teams integriert werden.

***
[![](images/orange.png)][4]
#### [Orange][4]
Datenvisualisierung

:mdi-apple: :mdi-microsoft-windows: :mdi-linux:
:::


* [:link: **PapDesigner**: Erstellen von Programmablaufplänen](http://friedrich-folkmann.de/papdesigner/Hauptseite.html)
* [:link: **Scratch**: Visuelle Programmiersprache](https://scratch.mit.edu/)
* [:link: **Digital**: Simulation von logischen Schaltungen](https://github.com/hneemann/Digital)

[1]: https://codewith.mu/
[2]: https://thonny.org/
[3]: http://omz-software.com/pythonista/
[4]: http://orange.biolab.si/
