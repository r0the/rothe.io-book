# Spielkarten
---

::: columns 6
![](images/playing-cards/herz_a.svg)
***
![](images/playing-cards/pik_a.svg)
***
![](images/playing-cards/karo_a.svg)
***
![](images/playing-cards/kreuz_a.svg)
***
![](images/playing-cards/joker_r.svg)
***
![](images/playing-cards/joker_s.svg)
:::

::: columns 6
![](images/playing-cards/herz_k.svg)
***
![](images/playing-cards/herz_d.svg)
***
![](images/playing-cards/herz_b.svg)
***
![](images/playing-cards/herz_10.svg)
***
![](images/playing-cards/herz_9.svg)
***
![](images/playing-cards/herz_8.svg)
***
![](images/playing-cards/herz_7.svg)
***
![](images/playing-cards/herz_6.svg)
***
![](images/playing-cards/herz_5.svg)
***
![](images/playing-cards/herz_4.svg)
***
![](images/playing-cards/herz_3.svg)
***
![](images/playing-cards/herz_2.svg)
:::

::: columns 6
![](images/playing-cards/pik_k.svg)
***
![](images/playing-cards/pik_d.svg)
***
![](images/playing-cards/pik_b.svg)
***
![](images/playing-cards/pik_10.svg)
***
![](images/playing-cards/pik_9.svg)
***
![](images/playing-cards/pik_8.svg)
***
![](images/playing-cards/pik_7.svg)
***
![](images/playing-cards/pik_6.svg)
***
![](images/playing-cards/pik_5.svg)
***
![](images/playing-cards/pik_4.svg)
***
![](images/playing-cards/pik_3.svg)
***
![](images/playing-cards/pik_2.svg)
:::

::: columns 6
![](images/playing-cards/karo_k.svg)
***
![](images/playing-cards/karo_d.svg)
***
![](images/playing-cards/karo_b.svg)
***
![](images/playing-cards/karo_10.svg)
***
![](images/playing-cards/karo_9.svg)
***
![](images/playing-cards/karo_8.svg)
***
![](images/playing-cards/karo_7.svg)
***
![](images/playing-cards/karo_6.svg)
***
![](images/playing-cards/karo_5.svg)
***
![](images/playing-cards/karo_4.svg)
***
![](images/playing-cards/karo_3.svg)
***
![](images/playing-cards/karo_2.svg)
:::

::: columns 6
![](images/playing-cards/kreuz_k.svg)
***
![](images/playing-cards/kreuz_d.svg)
***
![](images/playing-cards/kreuz_b.svg)
***
![](images/playing-cards/kreuz_10.svg)
***
![](images/playing-cards/kreuz_9.svg)
***
![](images/playing-cards/kreuz_8.svg)
***
![](images/playing-cards/kreuz_7.svg)
***
![](images/playing-cards/kreuz_6.svg)
***
![](images/playing-cards/kreuz_5.svg)
***
![](images/playing-cards/kreuz_4.svg)
***
![](images/playing-cards/kreuz_3.svg)
***
![](images/playing-cards/kreuz_2.svg)
:::

Quelle: [Public Domain Vectors](https://publicdomainvectors.org/)
