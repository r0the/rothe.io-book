# :reference: Unterrichtsmaterial
---

Auf diesen Seiten entsteht eine Sammlung von Unterrichtsmaterial für den Informatikunterricht.

* [:pdf: Jöran Muuß-Merholz: Freie Unterrichtsmaterialien, Beltz, 2018][1]

[1]: resources/Joeran-Muuss-Merholz-Freie-Unterrichtsmaterialien-Beltz-2018.pdf

https://www.wings.ch/content/produktvorstellung-computertechnik-und-programmierung

https://p5js.org/get-started/
