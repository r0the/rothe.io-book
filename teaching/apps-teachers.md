# App für Lehrer*innen
---
::: cards 2
[![](images/obs.png)][1]
#### [OBS Studio][1]
Mit dem OBS Studio (Open Broadcaster Software) können verschiedene Video- und Audioquellen auf einfache Weise kombiniert werden, um einen Live-Stream oder ein Video zu erzeugen.

:mdi-apple: :mdi-microsoft-windows: :mdi-linux:

:::

[1]: https://obsproject.com/
