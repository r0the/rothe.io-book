# Illustrationen
---

Alle Illustrationen stammen von Seraina Hohl und sind urheberrechtlich geschützt. Sie können unter den Bedingungen der [Creative Commons Attribution-NonCommercial-ShareAlike 4.0-Lizenz][1] verwendet werden.

::: cards 3
![](images/illustrations/communicate.svg)
[:download: Affinity Designer](images/illustrations/communicate.afdesign)

***
![](images/illustrations/dna.svg)
[:download: Affinity Designer](images/illustrations/dna.afdesign)

***
![](images/illustrations/malware.svg)
[:download: Affinity Designer](images/illustrations/malware.afdesign)

***
![](images/illustrations/parking.svg)
[:download: Affinity Designer](images/illustrations/parking.afdesign)

:::

::: columns 3
![](images/illustrations/code.svg)
[:download: Affinity Designer](images/illustrations/code.afdesign)

***
![](images/illustrations/internet.svg)
[:download: Affinity Designer](images/illustrations/internet.afdesign)

***
![](images/illustrations/metadata.svg)
[:download: Affinity Designer](images/illustrations/metadata.afdesign)

***
![](images/illustrations/monte-carlo.svg)
[:download: Affinity Designer](images/illustrations/monte-carlo.afdesign)

***
![](images/illustrations/phishing.svg)
[:download: Affinity Designer](images/illustrations/phishing.afdesign)

***
![](images/illustrations/privacy.svg)
[:download: Affinity Designer](images/illustrations/privacy.afdesign)

***
![](images/illustrations/protocol.svg)
[:download: Affinity Designer](images/illustrations/protocol.afdesign)

***
![](images/illustrations/simulation.svg)
[:download: Affinity Designer](images/illustrations/simulation.afdesign)

***
![](images/illustrations/vector-raster.svg)
[:download: Affinity Designer](images/illustrations/vector-raster.afdesign)

***
![](images/illustrations/for-loop.svg)
[:download: Affinity Designer](images/illustrations/for-loop.afdesign)

***
![](images/illustrations/key-exchange.svg)
[:download: Affinity Designer](images/illustrations/key-exchange.afdesign)
:::

[1]: https://creativecommons.org/licenses/by-nc-sa/4.0/
