const jsonFetchBase = 'https://intern.gymkirchenfeld.ch/api/';

function doFetch (params) {
  const accessToken = window.localStorage.getItem('accessToken');
  const options = {
    method: params.method || 'GET',
    headers: new window.Headers()
  };
  if (params.data) {
    options.body = JSON.stringify(params.data);
    options.headers.append('Content-Type', 'application/json');
  }
  options.headers.append('Authorization', 'Bearer ' + accessToken);
  return fetch(params.uri, options).then(resp => resp.json());
}

GK = {
  list: function (resource, id, subResource) {
    let uri = jsonFetchBase + resource;
    if (id) uri += '/' + id + '/' + subResource;
    return doFetch({ method: 'GET', uri: uri });
  },
  get: function (resource, id, subResource, subId) {
    let uri = jsonFetchBase + resource + '/' + id;
    if (subResource) uri += '/' + subResource + '/' + subId;
    return doFetch({ method: 'GET', uri: uri });
  },
  post: function (resource, id, subResource, data) {
    let uri = jsonFetchBase + resource + '/' + id;
    if (subResource) uri += '/' + subResource;
    return doFetch({ method: 'POST', uri: uri, data: data });
  },
  put: function (resource, id, subResource, id, data) {
    let uri = jsonFetchBase + resource + '/' + id;
    if (subResource) uri += '/' + subResource;
    return doFetch({ method: 'PUT', uri: uri, data: data});
  },
  del: function (resource, id) {
    let uri = jsonFetchBase + resource + '/' + id;
    return doFetch({ method: 'DELETE', uri: uri });
  },
  postSimple: function (resource, data) {
    let uri = jsonFetchBase + resource;
    return doFetch({ method: 'POST', uri: uri, data: data });
  },
}

GK.login = function (account, password) {
    return GK.postSimple('auth', { account: account, password: password }).then(response => {
      if (response.token) {
        window.localStorage.setItem('accessToken', response.token);
        return true;
      } else {
        window.localStorage.removeItem('accessToken');
        return false;
      }
    });
}

GK.portrait = function(id) {
}

GK.loggedIn = function () {
  return typeof window.localStorage.getItem('accessToken') === 'string';
}

GK.logout = function () {
  window.localStorage.removeItem('accessToken');
}
