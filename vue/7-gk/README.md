# Intern-API
---

## Verwendung

Um auf das Intern-API zuzugreifen, sollte die folgende JavaScript-Datei verwendet werden:

``` javascript ./gk-api.js
```

Die folgenden Funktionen sind asynchron, d.h. das Resultat muss immer in einem `.then()`-Block verarbeitet werden:

``` javascript
GK.list('holiday').then(result => {
  console.log(result);
});
```

## Allgemeine Informationen

### Ferien

``` javascript
GK.list('holiday')
```
liefert die Ferien gruppiert nach Schuljahr zurück.

### Fächer

``` javascript
GK.list('subject')
```
liefert eine Liste alle Fächer gruppiert nach Fachtyp zurück.

## Login

Um auf die geschützten Daten zugreifen zu können, muss man sich zuerst authentisieren.

``` javascript
GK.login(account, password)
```

## Geschütztes API

### Profil

``` javascript
GK.list('profile')
```
liefert das Profil des aktuellen Benutzers zurück.

### Absenzenübersicht

``` javascript
GK.list('person', personId, 'absenceInfo')
```
liefert die Absenzenübersicht für die Person mit der angegebenen `personId` zurück. Wenn für `personId` der Wert `-1` übergeben wird, wird die Absenzenübersicht für den aktuellen Benutzer zurückgeliefert.

### Entschuldigungen

``` javascript
GK.list('person', personId, 'justifications')
```
liefert eine Liste aller Entschuldigungen der  Person mit der angegebenen `personId` zurück.

``` javascript
GK.get('person', personId, 'justifications', id)
```
liefert die Entschuldigung mit der ID `id` der  Person mit der angegebenen `personId` zurück.

``` javascript
GK.post('person', personId, 'justifications', data)
```
erstellt eine neue Entschuldigung für die Person mit der angegebenen `personId`.

``` javascript
GK.put('person', personId, 'justifications', data)
```
speichert die Entschuldigung mit der ID `id` der  Person mit der angegebenen `personId`.
