new Vue({
  el: '#app',
  data: {
    account: '',
    password: ''
  },
  methods: {
    login: function () {
      GK.login(this.account, this.password).then(response => {
        if (response.token) {
          console.log('LOGIN OK');
        }
      }).catch(ex => {
        console.log(ex);
        console.log('Fehler');
      })
    },
    profile: function () {
      GK.list('persons', 3616, 'absenceInfo').then(response => {
        console.log(response);
      });
    }
  }
});
