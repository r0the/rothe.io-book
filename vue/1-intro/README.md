# Einstieg
---

## Grundstruktur

Eine einfache Vue-App besteht aus drei Dateien:

- HTML-Datei mit dem Vue-Template
- Vue-Bibliothek _vue.js_
- Eigene JavaScript-Datei mit der Vue-App

Die Vue-Bibliothek kann hier heruntergeladen werden:

* [:download: vue.js](./vue.js)

Wir verwenden das folgende HTML-Grundgerüst, um eine Vue-App zu erstellen:

``` html ./sample-01.html
```

In die Datei _app.js_ kommt folgender Code:

``` javascript ./sample-01.js
```

## App

~~~ javascript
new Vue(appDef)
~~~
erzeugt eine neue Vue-App. Sämtliche Eigenschaften der App werden über die `appDef`-Struktur angegeben. Eine HTML-Seite kann mehrere Vue-Apps enthalten.

~~~ javascript
el: selektor
~~~
Die `el`-Eigenschaft legt fest, wo in der HTML-Seite die Vue-App eingefügt wird. Für `selektor` muss ein CSS-Selektor als Zeichenkette angegeben werden.

Der folgende JavaScript-Code erzeugt eine Vue-App und fügt sie im HTML-Element mit der ID `app` ein, da für `el` ein ID-Selektor angegeben wurde:

``` javascript
const app = new Vue({
  el: '#app'
});
```

## Daten und Darstellung

Daten der App werden in der Eigenschaft `data` definiert. Um unter dem Namen `message` den Wert `'Hello Vue!'` abzulegen, schreiben wir:

``` javascript ./sample-02.js
```

Im HTML-Code kann der Inhalt von `message` so angezeigt werden:

``` html ./sample-02a.html
```

Was in den doppelten geschweiften Klammern steht, ist eigentlich JavaScript-Code, der von Vue ausgeführt wird. Wir könnten also beispielsweise auch folgendes schreiben:

``` javascript ./sample-02b.html
```
