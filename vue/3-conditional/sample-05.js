const app = new Vue({
  el: '#app',
  data: {
    message: 'Hello Vue!',
    visible: true,
  },
  methods: {
    toggle: function () {
      this.visible = !this.visible;
    }
  }
});
