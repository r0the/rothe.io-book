# Bedingte Anzeige
---

Um ein HTML-Element nur unter einer bestimmten Bedingung anzuzeigen, wird das Attribut `v-if` verwendet. Damit wird die Bedingung in JavaScript definiert. Im folgenden Beispiel sollen bestimmte Elemente nur angezeigt werden, wenn der Wert von `visible` `true` bzw. `false` ist:

``` html ./sample-05.html
```

In der App zu diesem Beispiel ist `visible` als Wahrheitswert definiert. Die Methode `toggle` kehrt den Wert von `visible` jeweils um.

``` javascript ./sample-05.js
```
