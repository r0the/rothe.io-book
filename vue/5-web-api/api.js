function api (base, endpoint, params) {
  let paramString = '';
  for (const param in params) {
    if (paramString.length > 0) paramString += '&';
    paramString += param + '=' + encodeURIComponent(params[param]);
  }
  const uri = base + '/' + endpoint + '?' + paramString;
  return new Promise(function (resolve, reject) {
    window.fetch(uri).then(response => {
      if (response.ok) {
        response.json().then(json => {
          resolve(json);
        }).catch(error => {
          reject(error);
        });
      } else {
        reject(new Error('Cannot fetch ' + uri + ', status: ' + response.status));
      }
    }).catch(error => {
      reject(error);
    });
  });
}
