/* global api */
const BASE_URI = 'http://transport.opendata.ch/v1';

const app = new Vue({
  el: '#app',
  data: {
    search: '',
    stations: []
  },
  methods: {
    showDeparture: function (id) {
      console.log(id);
    },
    searchStations: function () {
      console.log('search');
      api(BASE_URI, 'locations', { query: this.search, test: 'h' }).then(result => {
        this.stations = result.stations;
      });
    }
  }
});
