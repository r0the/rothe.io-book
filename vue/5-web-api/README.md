# Web-APIs
---

## Web-API (*application programming interface*)

Ein Web-API ist ein Webserver, welcher auf HTTP-Aufrufe mit JavaScript-Objekten anstatt mit HTML-Dateien antwortet.

Da der Aufruf von Web-APIs nicht ganz trivial ist, wird hier die Funktion `api` definiert, welche den Gebrauch von APIs vereinfacht:

``` javascript ./api.js
```

~~~ javascript
api(base, request, params).then(result => { })
~~~
ruft die URL auf, welche sich aus `base` und `request` zusammensetzt. Das JavaScript-Objekt `params` wird in HTTP-Parameter umgewandelt und der URL angehängt.

Das Resultat des Webservers wird als JavaScript-Objekt in der Variable `result` zurückgeliefert. in den geschweiften Klammern steht der Code, welcher ausgeführt wird, sobald das Resultat eingetroffen ist.

Im folgenden Beispiel wird der Web-API von [aareguru][1] aufgerufen. Der Aufruf `current` liefert die aktuellen Daten zurück. Die Ortschaft wird mit dem Parameter `city` ausgewählt:

``` javascript
const BASE_URL ='https://aareguru.existenz.ch/v2018';
const params = {
  city: 'Bern';
};
api(BASE_URL, 'current', params).then(result => {
  console.log(result);
});
```

Mit diesem Aufruf von `api` wird die folgende HTTP-Anfrage erzeugt:

``` http
https://aareguru.existenz.ch/v2018/current?city=bern
```

## Frei verfügbare Web-APIs

* [:link: Aareguru][1]
* [:link: Swiss public transport API][2]


[1]: https://aareguru.existenz.ch/
[2]: https://transport.opendata.ch/docs.html
