# Interaktion
---

## Klicken

In HTML kann mit dem Attribut `@click` angegeben werden, welche Methode aufgerufen werden soll, wenn auf das Element geklickt wird.

``` html ./sample-03.html
```

In der Vue-App können unter `methods` Funktionen definiert werden, welche durch `click` aufgerufen werden:

``` javascript ./sample-03.js
```

## Wichtiges zu Methoden

Methoden werden immer im Bereich `methods` definiert. In einer Methode kann mit `this.` auf die Daten oder andere Methoden zugegriffen werden.

## Ereignisse

Neben `click` können auch andere Ereignisse abgefangen werden:

| Ereignis    | Bedeutung                               |
| ----------- | --------------------------------------- |
| `click`     | Maus klickt auf Element                 |
| `dblclick`  | Doppelklick auf Element                 |
| `mousedown` | Maustaste wird heruntergedrückt         |
| `mouseup`   | Maustaste wird losgelassen              |
| `mousemove` | Mauszeiger wird bewegt                  |
| `mouseover` | Mauszeiger wird über das Element bewegt |
| `mouseout`  | Mauszeiger wird auf dem Element bewegt  |
| `wheel`     | Mausrad wird bewegt                     |
| `keydown`   | Taste wird heruntergedrückt             |
| `keyup`     | Taste wird losgelassen                  |
| `keypress`  | Tasten wird getippt                     |
| `input`     | Text wird in das Element eingegeben     |

## Tastaturereignisse

Bei Tastaturereignissen möchte man oft nur das Drücken eine einzigen Taste abfangen. Dazu kann man das Ereignis mit einer der folgenden Werte ergänzen:

| Attribut  | Bedeutung              |
| --------- | ---------------------- |
| `.enter`  | [Enter]                |
| `.tab`    | [Tab]                  |
| `.delete` | [Del] oder [Backspace] |
| `.esc`    | [Esc]                  |
| `.space`  | [Space]                |
| `.up`     | [Up]                   |
| `.down`   | [Down]                 |
| `.left`   | [Left]                 |
| `.right`  | [Right]                |

Im folgenden Beispiel kann der Benutzer eine Nachricht in einem Eingabefeld tippen. Wenn [Enter] gedrückt wird, wird die Methode `setMessage` aufgerufen:

``` html ./sample-04.html
```

Die App in JavaScript sieht so aus:

``` javascript ./sample-04.js
```

Der Methode wird immer ein Ereignis-Objekt übergeben. Mit `event.target` kann auf das HTML-Element zugegriffen werden. Mit `event.target.value` wird das `value`-Attribut des `input`-Elements ausgelesen.
