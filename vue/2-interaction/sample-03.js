const app = new Vue({
  el: '#app',
  data: {
    message: 'Hello Vue!'
  },
  methods: {
    toLower: function () {
      this.message = this.message.toLowerCase();
    },
    toUpper: function () {
      this.message = this.message.toUpperCase();
    }
  }
});
