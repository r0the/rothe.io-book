# Buefy
---

Buefy ist ein Framework, welches Vue erweitert, um mit kleinem Aufwand schön dargestellte Web-Apps zu erstellen.

* [:link: Buefy-Dokumentation](https://buefy.org/documentation)
* [:download: buefy.min.js](./buefy.min.js)
* [:download: buefy.min.css](./buefy.min.css)

## Vorlage

``` html ./buefy.html
```

## Button

``` html
<b-button @click="clickMe">Click Me</b-button>
```

## Aussehen

Das Aussehen wird mit dem Attribut `type` festgelegt. Es kann folgende Werte annehmen:

| Wert         | Beispiel                                       |
| ------------ | ---------------------------------------------- |
| `is-primary` | <b-button type="is-primary">Primary</b-button> |
| `is-success` | <b-button type="is-success">Success</b-button> |
| `is-danger`  | <b-button type="is-danger">Danger</b-button>   |
| `is-warning` | <b-button type="is-warning">Warning</b-button> |
| `is-info`    | <b-button type="is-info">Info</b-button>       |
| `is-link`    | <b-button type="is-link">Link</b-button>       |
| `is-light`   | <b-button type="is-light">Light</b-button>     |
| `is-dark`    | <b-button type="is-dark">Dark</b-button>       |
| `is-text`    | <b-button type="is-text">Text</b-button>       |
