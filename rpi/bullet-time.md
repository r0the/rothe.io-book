# Raspberry Pi
---

Der Raspberry Pi ist ein günstiger Mini-Computer.

![](images/raspberry-pi.jpg)

Mit einem Satz von diesen Mini-Computern wollen wir eine kurze Film-Sequenz mit einem «Bullet Time»-Effekt realisieren.

## Bullet Time

«Bullet Time» wird ein Spezialeffekt aus der Filmkunst genannt, bei welchem der Eindruck einer Kamerafahrt während eingefrorener Zeit entsteht. Der Effekt wurde durch seine Verwendung in der Filmtrologie «Matrix» bekannt.

![](images/bullet-time-effect-cameras.jpg "Kameras für einen Bullet Time-Effekt")

Der Effekt wird technisch so realisiert, dass viele Kameras um das zu filmende Objekt herum aufgestellt werden. Zum richtigen Zeitpunkt werden die Kameras (fast) gleichzeitig ausgelöst. Die erhaltenen Bilder werden anschliessend zu einer Filmsequenz zusammengefügt.

## Fragestellungen

- Wie wird der Raspberry Pi in Betrieb genommen? Es muss ein Betriebssystem installiert werden.
- Wie können die Raspberries per WLAN vernetzt werden?
- Wie kann der Raspberry Pi über das Netzwerk ferngesteuert werden?
- Wie kann die Kamera gesteuert werden?
- Wie wird das korrekte Timing für die Aufnahme der Bilder programmiert?
- Wie werden die einzelnen Bilder auf einem Raspberry gesammelt?
- Wie können die Bilder zu einem Videosequenz zusammengesetzt werden?
- Wie können die einzelnen Bilder aufeinander abgestimmt werden, damit die zusammengesetzte Sequenz nicht wackelt.

## Probleme

- WLAN zu viel Latenz: LAN benützen oder mit Timer arbeiten.
- Sequenz «verwackelt»: Bilder an Markierung ausrichten.
- Bilder zusammenfügen: [ffmpeg](https://trac.ffmpeg.org/wiki/Slideshow)
