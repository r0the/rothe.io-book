# Abgabe
---

## Schriftlicher Teil

Sie geben bis spätestens am **15. November 2019** folgendes ab:

- schriftliche Arbeit in **drei gebundenen Exemplaren**
- schriftliche Arbeit als PDF
- anonymisierte Version der schriftlichen Arbeit als PDF (siehe unten)

Die PDF-Version der schriftlichen Arbeit und alle gebundenen Exemplare müssen identisch sein.

### Technologische Arbeiten

Bei technologischen Arbeiten geben Sie zusätzlich folgendes ab:

- Software-Produkt als ausführbares Programm
- materielles Produkt, falls vorhanden
- Sämtlichen Quellcode, Projektdateien und Bibliotheken, welche für das Erstellen des Produkts benötigt werden
- Anleitung, wie das Produkt aus dem Quellcode erstellt werden kann.

### Elektronische Abgabe

Die elektronische Abgabe erfolgt in einer der folgenden Varianten:

- Sie kopieren sämtliche abzugebenden Dateien in einen OneDrive-Ordner und teilen diesen mit der Betreuungsperson.
- Sie stellen sämtliche abzugebenden Dateien in einem Git-Repository (z.B. GitHub) zu Verfügung.

### Anonymisierte Version

Die anonymisierte Version wird für die automatische Plagiatserkennung verwendet. Sie muss folgende Kriterien erfüllen:

- Namen von Autor und Betreuungsperson sind überall entfernt worden.
- Sämtliche Abbildungen sind entfernt worden.
- Dateiformat ist PDF.
- Dateiname hat die Form **19-titel-ohne-leerzeichen.pdf**

## Arbeitsprozess

Die folgenden Dokumente dienen als Grundlage für die Beurteilung des Arbeitsprozesses. Sie geben sie bis spätestens am **13. Dezember 2019**. in elektronischer Form ab.

- Arbeitsjournal als PDF
- Selbstreflexion als PDF
