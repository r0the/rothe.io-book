# :reference: Maturaarbeiten
---

## Vorgehen

| Was                                             |       Wann |
| ----------------------------------------------- | ----------:|
| schriftlicher [Projektantrag](?page=1-proposal) | 01.03.0202 |
| MA-Vertrag                                      | 06.03.2020 |

## Ziele

Als Gymnasiastin und Gymnasiast schreiben als Teil der Maturität eine **Maturaarbeit**.

> «Schülerinnen und Schüler müssen allein oder in einer Gruppe eine grössere eigenständige schriftliche oder schriftlich kommentierte Arbeit erstellen und mündlich präsentieren.»
>
> *– [Artikel 10](https://www.admin.ch/opc/de/classified-compilation/19950018/index.html#a10") der eidg. Maturitäts-Anerkennungsverordnung (MAV)*

Am Gymnasium Kirchenfeld werden die Ziele der Maturaarbeit folgendermassen beschrieben:

> «Die Schülerinnen und Schüler werden auf die selbständige Durchführung eines Projektes und auf selbständiges wissenschaftliches Arbeiten vorbereitet. Sie sollen fähig sein,
> - eine Fragestellung oder eine Gestaltungsidee zu entwickeln und zu bearbeiten,
> - Informationen zu beschaffen und zu verarbeiten,
> - Ergebnisse schriftlich darzustellen und zu präsentieren,
> - sich kritisch mit dem Arbeitsprozess auseinanderzusetzen.»
>
> *– Artikel 2.1 des [Reglements Maturaarbeiten](https://intern.gymkirchenfeld.ch/public/downloadDocument?id=60001) des Gymnasiums Kirchenfeld*

## Vorschläge für Maturaarbeiten

- Entwicklung einer Steuerung für die Anzeigetafeln des Gymnasiums basierend auf Raspberry Pi.

## Abgeschlossene Arbeiten

Eine Auswahl abgeschlossener Maturaarbeiten:

* [:download: 2D Physik-Engine programmiert in Java, Robert Balas, 2011](samples/2011-robert-balas.pdf)
* [:download: Gravity: Entwerfen und Programmieren einer eigenen Spielidee, Michael Marti, 2014](samples/2014-michael-marti.pdf)
* [:download: Modellieren von physikalischen Kräften, Johan Stettler, 2014](samples/2014-johan-stettler.pdf)
* [:download: Bau eines Handheld-Computerspiels aus elektronischen Komponenten, Aron Szakacs, 2014](samples/2014-aron-szakacs.pdf)
* [:download: Der Bau eines LED-Würfels, Peter Werner, 2014](samples/2014-peter-werner.pdf)
* [:download: Suchalgorithmen im Geschwindigkeitstest, Lorenz Jordi, 2015](samples/2015-lorenz-jordi.pdf)
* [:download: Programmierung einer physikalisch basierten Render-Engine, Manuel Baumann, 2019](samples/2019-manuel-baumann.pdf)
* [:link: 3D-Positionsbestimmung mit Sensor Fusion-basierten Algorithmen, Riccaro Feingold, 2019](https://github.com/RF4587/Maturaarbeit)
