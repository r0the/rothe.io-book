# Präsentation
---

> «Die Arbeit wird im Rahmen von besonderen öffentlichen Schulanlässen in Standardsprache oder in einer Fremdsprache präsentiert.
>
> Die Präsentationen dauern maximal 30 Minuten (inklusive Beantwortung von Fragen und Diskussion).»
>
> *– Artikel 4 des [Reglements Maturaarbeiten](https://intern.gymkirchenfeld.ch/public/downloadDocument?id=60001) des Gymnasiums Kirchenfeld*

Die Präsentation richtet sich an Maturandinnen und Maturanden sowie Gymnasiallehrerinnen und -lehrer. Sie können allgemeines Wissen auf Maturitätsstufe voraussetzen, jedoch kein spezifisches Fachwissen.
