# Selbstreflexion
---

> Der Schüler/die Schülerin reflektiert nach der erfolgten Präsentation den gesamten Arbeitsprozess und fasst das Ergebnis schriftlich auf maximal zwei Seiten zuhanden der betreuenden Lehrkraft zusammen. Zu dieser Reflexion gehört auch eine Einschätzung, wie der Arbeitsprozess das Produkt, den schriftlichen Teil und die mündliche Präsentation beeinflusst hat; aber nicht eine Beurteilung des Produkts und/oder des schriftlichen Teils an sich.
>
> Unter «Arbeitsprozess» werden Termin- und Prozessplanung und deren Umsetzung, der Umgang mit Schwierigkeiten und Hindernissen sowie je nach Projekt weitere Aspekte subsumiert.
>
> *– Artikel 5 des [Reglements Maturaarbeiten](https://intern.gymkirchenfeld.ch/public/downloadDocument?id=60001) des Gymnasiums Kirchenfeld*
