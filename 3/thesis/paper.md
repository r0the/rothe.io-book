# Schriftliche Arbeit
---

## Inhalt

> «[…] technologische […] Arbeiten sind möglich und bestehen aus einem Produkt und einem schriftlichen Kommentar, der mindestens die drei folgenden Teile umfasst:
>
> - Eine Beschreibung der Produktidee sowie der für deren Realisierung benötigten – vorhandenen oder zu erarbeitenden – theoretischen und praktischen Grundlagen (Kenntnisse und Fertigkeiten)
> - Eine Prozessdokumentation
> - Eine Analyse und Bewertung des Produkts entsprechend der jeweiligen Wissenschaft»
>
> *– Artikel 2.2 des [Reglements Maturaarbeiten](https://intern.gymkirchenfeld.ch/public/downloadDocument?id=60001) des Gymnasiums Kirchenfeld*

Die Arbeit richtet sich an Maturandinnen und Maturanden mit einem fundierten Wissen in Informatik (z.B. Ergänzungsfach Informatik) sowie an Informatiklehrerinnen und -lehrer. Sie können daher allgemeines Fachwissen voraussetzen und Fachbegriffe verwenden. Spezifisches Wissen aus dem von Ihnen untersuchten Fachgebiet müssen Sie jedoch erklären.

## Sprache

Korrektes und fehlerfreies Deutsch ist eine Selbstverständlichkeit.

Wissenschaftliche Texte sollen **objektiv** sein. Ausgenommen ist die Einleitung, welche die persönliche Perspektive des Autors/der Autorin darstellt.

Man kann **Objektivität** durch das Vermeiden von Bezügen auf Personen fördern. Es folgen ein paar Beispiele:

Stelle Erkenntnisse nicht als Entdeckung eines Forschers/einer Forscherin dar, sondern als Tatsache:

::: box warning
**Falsch:** Claude Shannon stellte fest, dass sich die Einheit des Informationsgehaltes mit der Grösse des Alphabets ändert.
:::

::: box info
**Richtig:** Die Einheit des Informationsgehaltes ändert sich mit der Grösse des Alphabets (Shannon 1948).
:::

Kommuniziere nicht direkt mit der Leserin/dem Leser.

::: box warning
**Falsch:** Im nächsten Kapitel findest du die Erklärung, wie die Entropie mit dem  Informationsgehalt zusammenhängt.

**Falsch:**  nächsten Kapitel werde ich den Zusammenhang zwischen Entropie und Informationsgehalt erklären.
:::

::: box info
**Falsch:** Im nächsten Kapitel wird der Zusammenhang zwischen Entropie und Informationsgehalt erklärt.
:::

Um eigene Beiträge hervorzuheben, nimmst du auf dich mit der Formulierung «die Autorin» / «der Autor» Bezug:

::: box warning
**Falsch:** Um dieses Problem zu vermeiden, habe ich beschlossen, den Quicksort-Algorithmus zu verwenden.
:::

::: box info
**Richtig:** Die Autorin schlägt vor, den Quicksort-Algorithmus zu verwenden, um dieses Problem zu vermeiden.
:::



## Form

> «Der schriftliche Teil der Arbeit umfasst mindestens folgende Teile:
>
> 1. Titelblatt
> 2. Inhaltsverzeichnis
> 3. Einleitung
> 4. Bearbeitung der Fragestellung inkl. Fazit oder schriftlicher Kommentar zum Produkt gemäss 2.2
> 5. Literaturverzeichnis
>
> Die Kapitel 3 bis 4 umfassen total maximal 10‘000 Wörter.
>
> Form und Layout sind mit der betreuenden Lehrkraft zu vereinbaren und orientieren sich an den Regeln der jeweiligen Fächer.
>
> Das Titelblatt enthält mindestens folgende Informationen:
> - Signet
> - Schule
> - Abteilung
> - Klasse
> - Namen des Schülers/der Schülerin und der betreuenden Lehrkraft
> - Titel
> - Ort und Jahr»
>
> *– Artikel 3 des [Reglements Maturaarbeiten](https://intern.gymkirchenfeld.ch/public/downloadDocument?id=60001) des Gymnasiums Kirchenfeld*

### Zitieren und Bibliographieren

Sie verwenden den nummerischen Zitierstil (IEEE-Stil), in welchem die verwendeten Referenzen fortlaufen nummeriert werden.

### Abbildungen

Abbildungen sind sehr wichtig, da sie abstrakte Sachverhalte anschaulich darstellen können. Abbildungen sind gross genug, sie haben eine Beschriftung. Im Abbildungsverzeichnis sind die Quellen angegeben. Sie weisen nach, dass Sie die Abbildungen publizieren dürfen werden (Lizenz des Urhebers).
