# Antrag
---

Sie schreiben einen Projektantrag, welchen Sie mir als PDF-Dokument einreichen. Der Antrag sollte ca. eine halbe bis eine Seite Umfang haben und folgende Punkte abdecken:

- **Fragestellung:** Was ist die wissenschaftliche oder technologische Frage, welche Sie mit Ihrer Arbeit beantworten möchten?
- **Mittel**: Welche Mittel wollen Sie verwenden (d.h. welche Technologien, Programmiersprachen, Software usw.)?
- **Motivation**: Was ist Ihre Motivation, sich mit dieser Fragestellung auseinanderzusetzen?

* [:pdf: Beispiel Projektantrag](samples/2010-riccardo-feingold-projektantrag.pdf)
