# Bewertung
---

## Grundlagen

> «Der Schüler/die Schülerin und die betreuende Lehrkraft vereinbaren zu Beginn mindestens die Arbeitsplanung und die Bewertungskriterien sowie deren Gewichtung.
>
> […]
>
> Die Gesamtbeurteilung umfasst mindestens:
> - den schriftlichen Teil der Arbeit (Inhalt, Sprache, Form)
> - die Präsentation (Inhalt, Sprache, Form)
> - den Arbeitsprozess
> - bei […] technologischen Arbeiten zusätzlich: das Produkt
>
> Der schriftliche Teil der Arbeit macht (bei […] technologischen Arbeiten zusammen mit dem Produkt) 50–70% der Gesamtbeurteilung aus, die Präsentation 20–30%, der Arbeitsprozess 10–20%.
>
> Betreuende und zweitbeurteilende Lehrkraft einigen sich auf eine Note für den schriftlichen Teil der Arbeit oder das Produkt mit Kommentar, der Schüler / die Schülerin und die betreuende Lehrkraft einigen sich auf eine gemeinsame Note für den Arbeitsprozess, die weiteren Teile der Arbeit werden durch die betreuende Lehrkraft beurteilt. Die Endnote ist ganz- oder halbzahlig.»
>
> *– Artikel 7 des [Reglements Maturaarbeiten](https://intern.gymkirchenfeld.ch/public/downloadDocument?id=60001) des Gymnasiums Kirchenfeld*

## Gewichtung

Bei von mir betreuten Maturaarbeiten ist die Gewichtung folgendermassen festgelegt:

| Teil               | Gewichtung | Bewertung durch                 |
|:------------------ |:---------- |:------------------------------- |
| Schriftlicher Teil | 60%        | Betreuer und Zweitbeurteiler/in |
| Präsentation       | 20%        | Betreuer                        |
| Arbeitsprozess     | 20%        | Betreuer und Schüler/in         |

Für jeden Teil wird eine auf Zehntel gerundete Einzelnote gesetzt.

### Schriftlicher Teil (inkl. Produkt)

Bei technologischen Arbeiten mit einem Produkt wird der schriftliche Teil folgendermassen bewertet:

| Bereich           | Gewichtung |
|:----------------- | ----------:|
| Kommentar: Inhalt |        33% |
| Kommentar: Form   |        17% |
| Produkt: Inhalt   |        33% |
| Produkt: Form     |        17% |
| **Total**         |   **100%** |

### Schriftlicher Teil (ohne Produkt)

Bei Arbeiten ohne Produkt wird der schriftliche Teil so bewertet:

| Bereich   | Gewichtung |
|:--------- | ----------:|
| Inhalt    |        67% |
| Form      |        33% |
| **Total** |   **100%** |

## Kriterien

### Schriftlicher Teil: Inhalt

#### Erfassen des Themas

- Ist das Thema erfasst worden?
- Sind die wichtigen Aspekte des Themas behandelt worden?
- Sind die behandelten Aspekte sinnvoll gewichtet worden?
- Konzentriert sich die Arbeit auf die wesentlichen Aspekte, wird nicht abgeschweift?

#### Entwicklung der Gedanken

- Ist die Entwicklung des Themas logisch, systematisch und differenziert?
- Ist das Ziel erreicht bzw. das Problem gelöst worden?
- Ist die Struktur des Textes klar und nachvollziehbar?

#### Verarbeitung der Quellen

- Sind die Quellen in angemessenem Umfang, kritisch und mit Sorgfalt genutzt worden?
- Sind die grundlegenden wissenschaftlichen Erkenntnisse berücksichtigt worden?
- Besteht ein wissenschaftliches Fundament für die Arbeit, das auf relevanten Quellen basiert?

#### Eigenständigkeit

- Beinhaltet die Arbeit eine eigenständige Leistung?
- Ist die eigene Leistung sinnvoll mit den Erkenntnissen aus den Quellen verknüpft worden?

#### Sachliche Richtigkeit

- Sind die Sachverhalte korrekt dargestellt worden?
- Sind wissenschaftliche Sachverhalte und Messergebnisse korrekt interpretiert worden?

### Schriftlicher Teil: Form

#### Zitate

- Sind alle wörtlichen Zitate und Parapharsierungen als solche gekennzeichnet?
- Wird beim Zitieren ein formales System verwendet?

#### Verzeichnisse

- Sind Inhalts-, Literatur- und Abbildungsverzeichnis vorhanden?
- Sind die Verzeichnisse übersichtlich und korrekt (entsprechen sie dem Inhalt)?

#### Darstellung

- Ist der Text übersichtlich gegliedert?
- Sind Abbildungen und Tabellen ansprechend und zweckmässig gestaltet?

#### Sprache

- Ist eine klare, verständliche Sprache verwendet worden?
- Werden Fachbegriffe definiert oder umschrieben?
- Ist die Sprache fehlerfrei (Syntax, Grammatik)?
