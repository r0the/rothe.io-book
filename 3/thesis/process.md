# Arbeitsprozess
---

## Regelmässige Treffen

Sie treffen sich mindestens einmal monatlich mit mir. Die Treffen haben folgende Traktanden:

- Protokoll des letzten Treffens
- Fortschritte seit dem letzten Treffen
- aktuelle Probleme
- offene Fragen
- nächste Schritte

Sie protokollieren diese Treffen.

## Projektplan

Sie erstellen einen Projektplan. Sie halten fest, welche Arbeiten sie wann machen wollen.

## Sicherung

Sie sichern sämtliche Unterlagen, Texte und Quellcode täglich, so dass ich Zugriff auf die Daten habe. Sie verwenden dazu eine der folgenden Varianten:

- das Versionsverwaltungssystem der Schule (GitLab)
- die Cloud-Lösung der Schule (OneDrive)

## Arbeitsjournal

Während der Maturaarbeit führen Sie ein Arbeitsjournal, in welchem Sie Ihre Tätigkeiten, wichtige Erkenntnisse und Ergebnisse festhalten. Im Arbeitsjournal protokollieren Sie ebenfalls die Besprechungen.

Das Arbeitsjournal ist eine wichtige Grundlage für die Bewertung des Arbeitsprozesses.

## Ablauf

| Wann             | Was                            | Ziel                                  |
|:---------------- |:------------------------------ |:------------------------------------- |
| Februar          | Kick-Off-Treffen               | Projektbeschrieb und Projektplan      |
| Anfang März      | MA-Vertrag                     |                                       |
| Mitte Mai        | MA-Vereinbarung (GH)           |                                       |
| Anfang Juli      | MA-Woche                       | Produkt grösstenteils fertig          |
| Mitte November   | Abgabe MA                      | Produkt und schriftlicher Teil fertig |
| Ende November    | Besprechung schriftlicher Teil |                                       |
| Mitte Dezember   | Präsentation (MN)              | Präsentation fertig                   |
| Mitte Dezember   | Abgabe Selbstreflexion         | Selbstreflexion, Arbeitsjournal       |
| vor Winterferien | Schlussbesprechung             | Bewertung                             |
