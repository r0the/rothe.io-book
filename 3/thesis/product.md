# Produkt
---

## Technologien

Die verwendeten Technologien müssen von mir genehmigt werden. Grundsätzlich werden Open Source-Technologien bevorzugt. Folgende Programmiersprachen können verwendet werden:

- Python
- Java
- C++

## Lizenz

Es würde mich freuen, wenn Sie den Quellcode, der während Ihrer Arbeit entsteht, unter einer Open-Source-Lizenz freigeben. So können später andere Schüler/innen von Ihrer Arbeit profitieren. Ausserdem kann so Ihre Arbeit als Vorzeigebeispiel auf dieser Seite publiziert werden.
